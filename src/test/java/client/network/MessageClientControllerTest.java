package client.network;

import client.ClientApp;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MessageClientControllerTest {

    @Nested
    @DisplayName("getInstance behavior")
    class getInstanceTest {

        @Test
        void should_return_instance_when_null() {

            // Given
            MessageClientController messageClientController = null;
            new ClientApp();

            // When
            messageClientController = MessageClientController.getInstance();

            // Then
            assertThat(messageClientController).isInstanceOf(MessageClientController.class);
        }

        @Test
        void should_return_first_instance_when_called() {

            // Given
            new ClientApp();
            MessageClientController messageController1 = MessageClientController.getInstance();

            // When
            MessageClientController messageController2 = MessageClientController.getInstance();

            // Then
            assertThat(messageController1).isEqualTo(messageController2);
        }
    }
}
