package client.network;

import common.network.messages.DisconnectionRequestMessage;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;

import java.util.UUID;

class WebSocketStompSessionHandlerTest {
    @Test
    void givenSession_whenConnected_SendsMessage() {

        // Given
        StompSession mockSession = Mockito.mock(StompSession.class);
        StompHeaders mockHeader = Mockito.mock(StompHeaders.class);
        WebSocketStompSessionHandler sessionHandler = new WebSocketStompSessionHandler();

        // When
        sessionHandler.afterConnected(mockSession, mockHeader);
        sessionHandler.sendMessage(new DisconnectionRequestMessage(UUID.randomUUID()));

        // Then
        Mockito.verify(mockSession).subscribe(WebSocketStompSessionHandler.RECEIVE_MESSAGE_TOPIC, sessionHandler);
        Mockito.verify(mockSession).send(Mockito.anyString(), Mockito.any());
    }
}
