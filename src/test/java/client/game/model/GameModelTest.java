package client.game.model;

import common.model.GameLight;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class GameModelTest {

    private GameModel gameModelUnderTest;

    @Mock
    private GameLight mockGameLight;

    @BeforeEach
    void setUp() {
        initMocks(this);
        gameModelUnderTest = new GameModel();
    }

    @Test
    void testGetGameUUID() {
        // Setup
        gameModelUnderTest.setCurrentGame(mockGameLight);
        when(mockGameLight.getId()).thenReturn(UUID.fromString("be0dd0e5-d784-49a2-b7c4-caf3fe81c80a"));
        // Run the test
        final UUID result = gameModelUnderTest.getGameUUID();

        // Verify the results
        assertEquals(UUID.fromString("be0dd0e5-d784-49a2-b7c4-caf3fe81c80a"), result);
    }

    @Test
    void testIsChatAuthorized() {
        // Setup

        // Run the test
        gameModelUnderTest.setCurrentGame(null);
        final boolean result = gameModelUnderTest.isChatAuthorized();

        gameModelUnderTest.setCurrentGame(new GameLight());
        final boolean result2 = gameModelUnderTest.isChatAuthorized();

        // Verify the results
        assertFalse(result);
        assertEquals(result2, gameModelUnderTest.getCurrentGame().isOkChat());
    }

}
