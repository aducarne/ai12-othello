package server.network;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import server.ServerApp;

import static org.assertj.core.api.Assertions.assertThat;

class MessageServerControllerTest {

    @Nested
    @DisplayName("getInstance behavior")
    class getInstanceTest {

        @Test
        void should_return_instance_when_null() {

            // Given
            MessageServerController messageServerController = null;
            new ServerApp();

            // When
            messageServerController = MessageServerController.getInstance();

            // Then
            assertThat(messageServerController).isInstanceOf(MessageServerController.class);
        }

        @Test
        void should_return_first_instance_when_called() {

            // Given
            new ServerApp();
            MessageServerController messageController1 = MessageServerController.getInstance();

            // When
            MessageServerController messageController2 = MessageServerController.getInstance();

            // Then
            assertThat(messageController1).isEqualTo(messageController2);
        }
    }
}
