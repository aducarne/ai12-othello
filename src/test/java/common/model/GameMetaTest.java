package common.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

class GameMetaTest {

    @Mock
    private UserLight mockGameCreator;
    @Mock
    private UserLight mockPlayer2;

    private GameMeta gameMetaUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        gameMetaUnderTest = new GameMeta(UUID.fromString("d3cad118-1548-4d13-8733-1051d93b0613"), mockGameCreator, mockPlayer2, GameStatus.PENDING, false, false, false, 0) {
        };
    }

    @Test
    void testToString() {
        // Setup

        // Run the test
        final String result = gameMetaUnderTest.toString();

        // Verify the results
        assertEquals("GameMeta{" + "id=d3cad118-1548-4d13-8733-1051d93b0613, gameCreator=" + mockGameCreator + ", player2=" + mockPlayer2 + ", status=" + GameStatus.PENDING
                + ", isOkSpectators=" + false + ", isOkChat=" + false + ", isWhiteCreator=" + false
                + ", limitMove=" + 0 + '}', result);
    }
}
