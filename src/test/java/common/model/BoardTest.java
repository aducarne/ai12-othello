package common.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoardTest {

    private final Board board = new Board();

    @Test
    void onBoard() {
        assertTrue(board.onBoard(0, 0));
        assertTrue(board.onBoard(7, 0));
        assertTrue(board.onBoard(6, 7));

        assertFalse(board.onBoard(-1, -1));
        assertFalse(board.onBoard(0, -1));
        assertFalse(board.onBoard(-1, 0));
        assertFalse(board.onBoard(8, 0));
        assertFalse(board.onBoard(0, 8));
        assertFalse(board.onBoard(8, 8));
    }
}
