package server.network;

import common.network.messages.*;

import java.util.Optional;
import java.util.UUID;

/**
 * Class handling sending & receiving messages from clients
 */
public class MessageServerController {

    private static MessageServerController instance = new MessageServerController();
    private ComCoreServer comCore;
    private WebSocketController wsController;

    private MessageServerController() {
    }

    /**
     * Get the unique instance of this class
     *
     * @return the instance of MessageController
     */
    public static MessageServerController getInstance() {
        return Optional.of(instance).orElseGet(MessageServerController::new);
    }

    /**
     * Get the communication core of the server
     *
     * @return the communication core
     */
    public ComCoreServer getComCore() {
        return comCore;
    }

    /**
     * Set the new communication core of the controller
     *
     * @param comCore the communication core to set
     */
    public void setComCore(ComCoreServer comCore) {
        this.comCore = comCore;
    }

    /**
     * Set the new WebSocket controller to use for messaging
     *
     * @param wsController the WebSocket controller to use
     */
    public void setWsController(WebSocketController wsController) {
        this.wsController = wsController;
    }

    /**
     * Send a NewUserAddedMessage to clients
     *
     * @param message the message to send
     */
    public void sendListsToNewUser(ServerStateOnConnectionMessage message) {
        wsController.sendToUser(message, message.getReceiver().getId().toString());
    }

    /**
     * Send a NewUserAddedMessage to clients
     *
     * @param message the message to send
     */
    public void notifyNewUserAdded(NewUserAddedMessage message) {
        wsController.sendToAll(message);
    }

    /**
     * Send a MoveToForwardToClientMessage to clients
     *
     * @param message the message to send
     */
    public void sendBoardToUpdate(MoveToForwardToClientMessage message) {
        wsController.sendToRoom(message, message.getGameID().toString());
    }

    /**
     * Relay a Join Request message from server to the game owner.
     *
     * @param message the message to send
     */
    public void relayJoinRequestMessage(JoinRequestMessage message, String gameOwnerId) {
        wsController.sendToUser(message, gameOwnerId);
    }

    /**
     * Relay a Join Request Answer message from server to the user who asked to join a game.
     *
     * @param message the message to send
     */
    public void relayJoinRequestAnswerMessage(JoinRequestAnswerMessage message, String userToNotifyAnswer) {
        wsController.sendToUser(message, userToNotifyAnswer);
    }

    /**
     * Send a Launch Game message from server to all users associated with the game (players + spectators)
     *
     * @param message: the message to send
     * @param roomId:  the id of the room associated to the game
     */
    public void sendLaunchGameMessage(LaunchGameMessage message, String roomId) {
        wsController.sendToRoom(message, roomId);
    }


    /**
     * Send a chat message to the clients of a game
     *
     * @param chatMessage the message to send
     * @param gameId      the ID of the room
     */
    public void sendChatMessageToClients(ChatMessageToClientMessage chatMessage, UUID gameId) {
        wsController.sendToRoom(chatMessage, gameId.toString());
    }

    /**
     * Notify the clients that a new game is created.
     *
     * @param message the message to send
     */
    public void sendNewGameNotification(NewGameNotificationToClientMessage message) {
        wsController.sendToAll(message);
    }

    /**
     * Send a GameListMessage to clients
     *
     * @param message the message to send
     */
    public void sendGameList(GameListMessage message) {
        wsController.sendToAll(message);
    }

    /**
     * Send a UserListMessage to clients
     *
     * @param message the message to send
     */
    public void sendUserList(UserListMessage message) {
        wsController.sendToAll(message);
    }

    /**
     * Send an EndGameSignalToClientMessage to clients
     *
     * @param message the message to send
     * @param gameId  the game id
     */
    void sendEndGameMessage(EndGameSignalToClientMessage message, String gameId) {
        wsController.sendToRoom(message, gameId);
    }

    /**
     * Send a WinnerToClientMessage to clients
     *
     * @param message the message to send
     * @param gameId  the game id
     */
    void sendWinnerMessage(WinnerToClientMessage message, String gameId) {
        wsController.sendToRoom(message, gameId);
    }

    /**
     * Send a GameSaveMessage to a requesting client
     *
     * @param message: the message to send, containing GameHeavy with save info
     */
    void sendGameSaveToClient(GameSaveMessage message, UUID userId) {
        wsController.sendToUser(message, userId.toString());
    }

    /**
     * Send a SpectatorToAddClientMessage to clients
     *
     * @param message the message to send
     * @param gameId  the game id
     */
    public void sendSpectators(SpectatorToAddClientMessage message, String gameId) {
        wsController.sendToRoom(message, gameId);
    }

    void sendRemoveSpectatorFromClientsMessage(RemoveSpectatorFromClientMessage message, String gameId) {
        wsController.sendToRoom(message, gameId);
    }

    /**
     * Send a SpectatorToAddClientMessage to clients
     *
     * @param message the message to send
     * @param gameId  the id of the game room
     */
    public void sendNewSpectator(SpectatorToAddClientMessage message, String gameId) {
        wsController.sendToRoom(message, gameId);
    }
}
