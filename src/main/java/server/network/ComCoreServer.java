package server.network;

import common.interfaces.server.IComToDataServer;
import common.interfaces.server.IDataToCom;
import common.model.Message;
import common.network.ComCoreMeta;
import common.network.messages.JoinRequestAnswerMessage;
import common.network.messages.JoinRequestMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Communication core containing message controller and interfaces.
 */
@Component
public class ComCoreServer extends ComCoreMeta {

    private static final Logger LOGGER = LogManager.getLogger(ComCoreServer.class);
    private IComToDataServer comToDataImpl;
    private DataToComServerImpl dataToComServerImpl;
    private MessageServerController messageServerController;

    @Autowired
    private WebSocketController wsController;

    public ComCoreServer() {
        dataToComServerImpl = new DataToComServerImpl(this);
        messageServerController = MessageServerController.getInstance();
        messageServerController.setComCore(this);
    }

    /**
     * Get the DataToCom interface implementation
     *
     * @return the implementation
     */
    public IDataToCom getDataToComServerImpl() {
        return dataToComServerImpl;
    }

    public void setComToDataImpl(IComToDataServer comToDataImpl) {
        this.comToDataImpl = comToDataImpl;
    }

    /**
     * Get the comToData interface to access Data methods
     *
     * @return the comToData interface
     */
    public IComToDataServer getComToData() {
        return comToDataImpl;
    }

    /**
     * Get the message controller associated to the server
     *
     * @return the message controller
     */
    public MessageServerController getMessageServerController() {
        return messageServerController;
    }

    /**
     * Init the WebSocket connection process
     */
    public void initWebSocketConnection() {
        messageServerController.setWsController(wsController);
    }

    public void sendChatMessage(Message message) {
        // TODO: replace by its implementation
    }


    public void joinRequestMessageProcess(JoinRequestMessage message) {
        try {
            UUID gameCreatorUserLight = this.getComToData().getGameCreator(message.getGameID());
            messageServerController.relayJoinRequestMessage(message, gameCreatorUserLight.toString());
        } catch (Exception e) {
            LOGGER.error("joinRequestMessageProcess", e);
        }
    }

    public void joinRequestAnswerMessageProcess(JoinRequestAnswerMessage message) {
        MessageServerController.getInstance().relayJoinRequestAnswerMessage(message, message.getPlayerAskingID().toString());
    }
}
