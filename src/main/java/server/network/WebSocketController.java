package server.network;

import common.network.messages.MessageMeta;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

/**
 * Controller for WebSocket implementation
 */
@Controller
public class WebSocketController {

    private static final Logger LOGGER = LogManager.getLogger(WebSocketController.class);
    private static final String TOPIC_PREFIX = "/topic/";
    private static final String DESTINATION_USER_SUFFIX = "/user";
    private static final String DESTINATION_ROOM_SUFFIX = "/room";

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * Send a message to the client app
     *
     * @param message the message to be sent to the client
     */
    public void sendToAll(MessageMeta message) {
        LOGGER.info("Message sent to whole server : {}", message);
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + "clientMessages", message);
    }

    public void sendToUser(MessageMeta message, String userId) {
        LOGGER.info("Message sent to user with id : {}", userId);
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + userId + DESTINATION_USER_SUFFIX, message);
    }

    public void sendToRoom(MessageMeta message, String roomId) {
        LOGGER.info("Message sent to whole room with id : {}", roomId);
        simpMessagingTemplate.convertAndSend(TOPIC_PREFIX + roomId + DESTINATION_ROOM_SUFFIX, message);
    }

    @MessageMapping("/user/{id}")
    public void onMessageUser(@DestinationVariable String id, MessageMeta message) {
        LOGGER.info("New client message received : {} from user with id : {}", message, id);
        message.setComCoreMeta(MessageServerController.getInstance().getComCore());
        message.processData();
    }
}
