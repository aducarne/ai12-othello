package server.network;

import common.interfaces.server.IDataToCom;
import common.model.*;
import common.network.messages.*;

import java.util.List;
import java.util.UUID;

public class DataToComServerImpl implements IDataToCom {

    private ComCoreServer comCoreServer;

    public DataToComServerImpl(ComCoreServer comCoreServer) {
        this.comCoreServer = comCoreServer;
    }

    @Override
    public void notifyNewGame(GameLight newGameCreated) {
        NewGameNotificationToClientMessage message = new NewGameNotificationToClientMessage(newGameCreated);
        comCoreServer.getMessageServerController().sendNewGameNotification(message);
    }

    @Override
    public void sendUpdateMoveMessage(UUID gameID, Board updateBoard) {
        MoveToForwardToClientMessage message = new MoveToForwardToClientMessage(gameID, updateBoard);
        comCoreServer.getMessageServerController().sendBoardToUpdate(message);
    }


    @Override
    public void sendNewUserLists(UserLight playerToNotify, List<UserLight> playersOnline,
                                 List<GameLight> gamesOnline) {
        ServerStateOnConnectionMessage message = new ServerStateOnConnectionMessage(playerToNotify, playersOnline, gamesOnline);
        comCoreServer.getMessageServerController().sendListsToNewUser(message);
    }

    @Override
    public void sendGameList(List<GameLight> gameLights) {
        GameListMessage message = new GameListMessage(gameLights);
        MessageServerController.getInstance().sendGameList(message);
    }

    @Override
    public void launchGame(UUID gameId, Board board) {
        LaunchGameMessage message = new LaunchGameMessage(gameId, board);
        MessageServerController.getInstance().sendLaunchGameMessage(message, gameId.toString());
    }

    @Override
    public void sendEndGameSignal(UUID gameID, Board endedGame) {
        EndGameSignalToClientMessage message = new EndGameSignalToClientMessage(endedGame);
        MessageServerController.getInstance().sendEndGameMessage(message, gameID.toString());
    }

    @Override
    public void notifyGameListChanged(List<GameLight> updatedGameList) {
        GameListMessage message = new GameListMessage(updatedGameList);
        MessageServerController.getInstance().sendGameList(message);

    }

    @Override
    public void notifyUserListChanged(List<UserLight> updatedUserList) {
        UserListMessage message = new UserListMessage(updatedUserList);
        MessageServerController.getInstance().sendUserList(message);

    }

    @Override
    public void sendWinner(UUID gameID, Board update, UserZero winner) {
        WinnerToClientMessage message = new WinnerToClientMessage(update, winner);
        MessageServerController.getInstance().sendWinnerMessage(message, gameID.toString());
    }

    @Override
    public void sendGameSave(GameHeavy game, UUID receiverPlayerId) {
        GameSaveMessage message = new GameSaveMessage(game);
        MessageServerController.getInstance().sendGameSaveToClient(message, receiverPlayerId);

    }

    @Override
    public void sendSpectatorList(List<UserZero> spectatorList, UUID id) {
        SpectatorToAddClientMessage message = new SpectatorToAddClientMessage(spectatorList);
        String gameId = id.toString();
        MessageServerController.getInstance().sendSpectators(message, gameId);
    }

    @Override
    public void removeSpectatorFromClient(List<UserZero> spectatorList, UUID gameId) {
        RemoveSpectatorFromClientMessage message = new RemoveSpectatorFromClientMessage(spectatorList);
        MessageServerController.getInstance().sendRemoveSpectatorFromClientsMessage(message, gameId.toString());
    }
}
