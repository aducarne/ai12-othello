package server.data;

import client.data.Configuration;
import common.exceptions.OthelloException;
import common.interfaces.server.IDataToCom;
import common.model.GameHeavy;
import common.model.UserLight;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

@Component
public class DataCoreServer {
    private List<UserLight> listConnectedUsers;
    private List<GameHeavy> listGameHeavy;
    private ComToDataServerImpl comToDataServerImpl;
    private IDataToCom dataToCom;

    public DataCoreServer() {
        comToDataServerImpl = new ComToDataServerImpl(this);
        listConnectedUsers = new ArrayList<>();
        listGameHeavy = new ArrayList<>();
    }

    public ComToDataServerImpl getComToDataServerImpl() {
        return comToDataServerImpl;
    }

    List<UserLight> getListConnectedUsers() {
        return listConnectedUsers;
    }

    List<GameHeavy> getListGameHeavy() {
        return listGameHeavy;
    }

    IDataToCom getDataToCom() {
        return dataToCom;
    }

    public void setDataToCom(IDataToCom dataToCom) {
        this.dataToCom = dataToCom;
    }

    GameHeavy findGame(UUID gameId) throws OthelloException {
        ListIterator<GameHeavy> itGame = getListGameHeavy().listIterator();
        boolean foundGame = false;
        GameHeavy game = null;
        while (itGame.hasNext() && !foundGame) {
            game = itGame.next();
            if (game.getId().equals(gameId))
                foundGame = true;
        }
        if (!foundGame)
            throw new OthelloException(Configuration.GAME_NOT_FOUND_ON_SERVER);
        return game;
    }

    int findGameIndex(UUID gameId) throws OthelloException {
        ListIterator<GameHeavy> itGame = getListGameHeavy().listIterator();
        boolean foundGame = false;
        int index = 0;
        while (itGame.hasNext() && !foundGame) {
            if (itGame.next().getId().equals(gameId))
                foundGame = true;
            else
                index++;
        }
        if (!foundGame)
            throw new OthelloException(Configuration.GAME_NOT_FOUND_ON_SERVER);
        return index;
    }

    UserLight findUser(UUID userId) throws OthelloException {
        ListIterator<UserLight> itUser = getListConnectedUsers().listIterator();
        boolean foundUser = false;
        UserLight user = null;
        while (itUser.hasNext() && !foundUser) {
            user = itUser.next();
            if (user.getId().equals(userId))
                foundUser = true;
        }
        if (!foundUser)
            throw new OthelloException(Configuration.GAME_NOT_FOUND_ON_SERVER);
        return user;
    }

    int findUserIndex(UUID userId) throws OthelloException {
        ListIterator<UserLight> itUser = getListConnectedUsers().listIterator();
        boolean foundUser = false;
        int index = 0;
        while (itUser.hasNext() && !foundUser) {
            if (itUser.next().getId().equals(userId))
                foundUser = true;
            else
                index++;
        }
        if (!foundUser)
            throw new OthelloException("Game not found on server");
        return index;
    }
}
