package server.data;

import common.exceptions.OthelloException;
import common.interfaces.server.IComToDataServer;
import common.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ComToDataServerImpl implements IComToDataServer {

    private static final Logger LOGGER = LogManager.getLogger(ComToDataServerImpl.class);

    ////Initializing 2 lists in order to recreate the game logic
    private static List<Integer> listeDirectionX = new ArrayList<>(
            Arrays.asList(-1, 0, 1)
    );
    private static List<Integer> listeDirectionY = new ArrayList<>(
            Arrays.asList(-1, 0, 1)
    );
    private DataCoreServer dataCoreServer;

    ComToDataServerImpl(DataCoreServer dataCoreServer) {
        this.dataCoreServer = dataCoreServer;
    }

    @Override
    public void addAuthenticatedPlayer(UserLight userLight) {
        //Adding the user to the list of connected users, and notifying the other players
        dataCoreServer.getListConnectedUsers().add(userLight);
        dataCoreServer.getDataToCom().notifyGameListChanged(getListGames());
        dataCoreServer.getDataToCom().notifyUserListChanged(getListUsers());
    }

    @Override
    public void createGame(GameLight gameLight) {
        //Adding the new game to the list of available games on the server, and notifying all the players
        GameHeavy newGame = new GameHeavy(gameLight.getId(), gameLight.getGameCreator(), gameLight.getPlayer2(),
                gameLight.getStatus(), gameLight.isOkSpectators(), gameLight.isOkChat(), gameLight.isWhiteCreator(),
                gameLight.getLimitMove(), new ArrayList<Move>(), new Timestamp(System.currentTimeMillis()),
                gameLight.isOkChat() ? new Chat(new ArrayList<Message>()) : null, null,
                gameLight.isOkSpectators() ? new ArrayList<UserZero>() : null);
        dataCoreServer.getListGameHeavy().add(newGame);
        dataCoreServer.getDataToCom().notifyGameListChanged(getListGames());
    }

    @Override
    public void requestGameSave(UUID userID, UUID gameID) {
        //Returning the GameHeavy corresponding to the game to save
        try {
            GameHeavy gameToSave = dataCoreServer.findGame(gameID);
            dataCoreServer.getDataToCom().sendGameSave(gameToSave, userID);
        } catch (Exception e) {
            LOGGER.error("requestGameSave", e);
        }
    }

    @Override
    public void receiveNewChatMessage(Message newMessage, UUID gameID) throws OthelloException {
        //Finding the game concerned and adding the message to its chat
        GameHeavy currentGame = dataCoreServer.findGame(gameID);
        int gameIndex = dataCoreServer.findGameIndex(gameID);
        currentGame.getChat().getChatList().add(newMessage);
        dataCoreServer.getListGameHeavy().set(gameIndex, currentGame);
    }

    @Override
    public void disconnectUser(UUID userID) throws OthelloException {
        dataCoreServer.getListConnectedUsers().remove(dataCoreServer.findUserIndex(userID));
        dataCoreServer.getDataToCom().notifyUserListChanged(getListUsers());
    }

    @Override
    public void addSpectator(GameLight gameLight, UserLight userLight) {
        UserZero userZero = new UserZero(userLight.getId(), userLight.getPseudo());
        List<GameHeavy> gameHeavies = dataCoreServer.getListGameHeavy();
        for (GameHeavy gameHeavy : gameHeavies) {
            if (gameLight.getId().equals(gameHeavy.getId())) {
                gameHeavy.getSpectators().add(userZero);
                dataCoreServer.getDataToCom().sendSpectatorList(gameHeavy.getSpectators(), gameHeavy.getId());
            }
        }
    }

    @Override
    public void removeSpectatorFromServer(UserZero user, UUID gameUUID) {
        for (GameHeavy gameHeavy : dataCoreServer.getListGameHeavy()) {
            if (gameHeavy.getId().equals(gameUUID)) {
                List<UserZero> spectatorList = gameHeavy.getSpectators();
                try {
                    for (int i = 0; i < spectatorList.size(); i++) {
                        //Equals non fonctionnel ici car UUID ne possede pas la meme reference, meme si la valeur est identique
                        //Passage par des strings pour s'affranchir de ce problème
                        if (spectatorList.get(i).getId().toString().equalsIgnoreCase(user.getId().toString()))
                            spectatorList.remove(i);
                    }
                } catch (Exception e) {
                    LOGGER.debug(e);
                }
                dataCoreServer.getDataToCom().removeSpectatorFromClient(spectatorList, gameUUID);
            }
        }
    }

    @Override
    public void sendMove(UUID gameId, Move move, Board board) throws OthelloException {
        //Getting the game, and adding the move to its move list
        GameHeavy currentGame = dataCoreServer.findGame(gameId);
        currentGame.getMoveList().add(move);

        //If x pos of the move is -1, it's a forfeit
        if (move.getX() == -1) {
            //If the sender of the forfeit move is player 1, setting next player to -1
            if (move.getPlayer().getId().equals(board.getPlayer1().getId())) {
                board.setNextPlayer(-1);
                currentGame.setWinner(board.getPlayer2().getId());
            }
            //Same with player 2
            else {
                board.setNextPlayer(-2);
                currentGame.setWinner(board.getPlayer1().getId());
            }
        }
        //If it's not a forfeit move, we update the board with the move
        else {
            board = updateBoard(board, move.getX(), move.getY(), board.getNextPlayer());
            board = choseNextPlayer(board);
        }

        //If the next player is > 0, the game is not over, so we update the game on server
        //and send the new board to all users in game
        if (board.getNextPlayer() > 0) {
            dataCoreServer.getListGameHeavy().set(dataCoreServer.findGameIndex(gameId), currentGame);
            dataCoreServer.getDataToCom().sendUpdateMoveMessage(gameId, board);
        }
        //If it's not > 0 the game is over
        else {
            //Setting the game status to "ENDED"
            currentGame.setStatus(GameStatus.ENDED);

            UserLight winnerUpdated;
            UserLight loserUpdated;
            //If next player is -1, the winner is player 2 and loser is player 1
            if (board.getNextPlayer() == -1) {
                loserUpdated = dataCoreServer.findUser(board.getPlayer1().getId());
                winnerUpdated = dataCoreServer.findUser(board.getPlayer2().getId());
            }
            //If next player is -2, the winner is player 1 and loser is player 2
            else if (board.getNextPlayer() == -2) {
                loserUpdated = dataCoreServer.findUser(board.getPlayer2().getId());
                winnerUpdated = dataCoreServer.findUser(board.getPlayer1().getId());
            }
            //If next player is 0, we have to calculate the scores to determine the winner
            else {
                if (getWinner(board).getId().equals(board.getPlayer1().getId())) {
                    winnerUpdated = dataCoreServer.findUser(board.getPlayer1().getId());
                    loserUpdated = dataCoreServer.findUser(board.getPlayer2().getId());
                } else {
                    winnerUpdated = dataCoreServer.findUser(board.getPlayer2().getId());
                    loserUpdated = dataCoreServer.findUser(board.getPlayer1().getId());
                }
            }
            //We update the stats of the players in the list of connected players
            winnerUpdated.setPlayedGames(winnerUpdated.getPlayedGames() + 1);
            winnerUpdated.setWonGames(winnerUpdated.getWonGames() + 1);
            dataCoreServer.getListConnectedUsers().set(dataCoreServer.findUserIndex(board.getPlayer1().getId()), winnerUpdated);
            loserUpdated.setPlayedGames(loserUpdated.getPlayedGames() + 1);
            dataCoreServer.getListConnectedUsers().set(dataCoreServer.findUserIndex(board.getPlayer2().getId()), loserUpdated);
            //We send the new list of connected users to all the clients
            dataCoreServer.getDataToCom().notifyUserListChanged(dataCoreServer.getListConnectedUsers());

            //We send the updated list of games and the end game signal
            dataCoreServer.getListGameHeavy().set(dataCoreServer.findGameIndex(gameId), currentGame);
            dataCoreServer.getDataToCom().notifyGameListChanged(getListGames());
            dataCoreServer.getDataToCom().sendEndGameSignal(gameId, board);
        }
    }

    @Override
    public List<UserLight> getListUsers() {
        return dataCoreServer.getListConnectedUsers();
    }

    @Override
    public List<GameLight> getListGames() {
        List<GameLight> listGameLight = new ArrayList<>();
        for (GameHeavy gameHeavy : dataCoreServer.getListGameHeavy()) {
            listGameLight.add(gameHeavy.convertToGameLight());
        }
        return listGameLight;
    }

    @Override
    public void receiveJoinRequestAnswer(UUID playerAskingID, UUID gameId, Boolean isAccepted) throws OthelloException {
        //If the join request is accepted, we update the game and send the board to both players
        if (Boolean.TRUE.equals(isAccepted)) {
            addPlayer2(playerAskingID, gameId);
            Board board = new Board();
            GameHeavy game = dataCoreServer.findGame(gameId);
            if (game.isWhiteCreator()) {
                board.setPlayer2(game.getGameCreator());
                board.setPlayer1(game.getPlayer2());
            } else {
                board.setPlayer1(game.getGameCreator());
                board.setPlayer2(game.getPlayer2());
            }
            dataCoreServer.getDataToCom().launchGame(gameId, board);
        }
    }

    private void addPlayer2(UUID player2Id, UUID gameId) throws OthelloException {
        //Adding a player 2 to the game, and changing its status to "IN_PROGRESS"
        UserLight player2 = dataCoreServer.findUser(player2Id);
        GameHeavy currentGame = dataCoreServer.findGame(gameId);
        if (currentGame.getPlayer2() != null)
            throw new OthelloException("There is already 2 players in this game");
        currentGame.setPlayer2(player2);
        currentGame.setStatus(GameStatus.IN_PROGRESS);
        dataCoreServer.getListGameHeavy().set(dataCoreServer.findGameIndex(gameId), currentGame);
        //Then notifying all the clients of the games list change
        dataCoreServer.getDataToCom().notifyGameListChanged(getListGames());
    }

    @Override
    public UUID getGameCreator(UUID gameId) throws OthelloException {
        return dataCoreServer.findGame(gameId).getGameCreator().getId();
    }

    /**
     * Get the value of the color opposed to the one provided in parameter (1 if parameter equals 2, 2 if parameter equals 1, else 0)
     *
     * @param color: value of the color you want to get the opposite
     */
    private int getOpposedColor(int color) {
        int opposedColor = color;
        if (color > 0) {
            opposedColor = color % 2 + 1;
        }
        return opposedColor;
    }

    /**
     * Check if the move is possible
     *
     * @param posX:      x axis movement
     * @param posY:      y axis movement
     * @param moveColor: color of the pawn to move
     * @param board:     Board from which we have to count score
     */
    private boolean isMoveValid(Board board, int posX, int posY, int moveColor) {
        //If the move is on a case already occupied, it is not valid
        if (!board.onBoard(posX, posY) || board.getColor(posX, posY) != 0) {
            return false;
        }
        //Creating a counter that represents number of cases traveled in each direction
        int cpt;
        //Checking in all directions
        for (int directionX : listeDirectionX) {
            for (int directionY : listeDirectionY) {
                //Not checking in direction 0,0 because it doesn't represent a direction
                if (!(directionX == 0 && directionY == 0)) {
                    //Counter initialized to 1 because we check at least the closest case in each direction
                    cpt = 1;
                    LOGGER.debug("DEBUG DIRECTION : {} {}", directionX, directionY);
                    //We continue to travel in a direction while we are on the board,
                    //and while the case travelled is occupied by the opposed color
                    while (board.onBoard(posX + directionX * cpt, posY + directionY * cpt)
                            && board.getColor(posX + directionX * cpt,
                            posY + directionY * cpt) == getOpposedColor(moveColor)) {
                        cpt++;
                        LOGGER.debug("DEBUG : {} {}", posX + directionX * cpt, posY + directionY * cpt);
                    }
                    //Once we are not on a case occupied by the opposite color,
                    // we check if we are still on the board, and if we travelled more than just the closest case,
                    //and finally if the last case we saw is occupied by our color
                    if (board.onBoard(posX + directionX * cpt, posY + directionY * cpt) && cpt > 1
                            && board.getColor(posX + directionX * cpt, posY + directionY * cpt) == moveColor) {
                        //If this is the case, it means that the move is framing at least one tile of the opposite color,
                        //so the move is valid
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Update the board according to the move given
     *
     * @param board:     old board
     * @param posX:      position x of the move
     * @param posY:      position y of the move
     * @param moveColor: color of the move
     */
    private Board updateBoard(Board board, int posX, int posY, int moveColor) {
        //It works like isMoveValid
        int cpt;
        for (int directionX : listeDirectionX) {
            for (int directionY : listeDirectionY) {
                if (!(directionX == 0 && directionY == 0)) {
                    cpt = 1;
                    while (board.onBoard(posX + directionX * cpt, posY + directionY * cpt)
                            && board.getColor(posX + directionX * cpt,
                            posY + directionY * cpt) == getOpposedColor(moveColor)) {
                        cpt++;
                    }
                    if (board.onBoard(posX + directionX * cpt, posY + directionY * cpt) && cpt > 1
                            && board.getColor(posX + directionX * cpt, posY + directionY * cpt) == moveColor) {
                        //Unless that once we find tiles framed by our color, we change their color
                        while (cpt > 0) {
                            cpt--;
                            board.setState(posX + directionX * cpt, posY + directionY * cpt, moveColor);
                        }
                    }
                }
            }
        }
        //Returning the new board
        return board;
    }

    /**
     * Check if there is a move valid.
     *
     * @param board:     board on which we check if there is a valid move
     * @param moveColor: color to check
     */
    private boolean isThereAMoveValid(Board board, int moveColor) {
        //For each case, we test if a move of the moveColor would be valid.
        //Once we find one case for which it's true, we return true.
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (isMoveValid(board, i, j, moveColor)) {
                    return true;
                }
            }
        }
        //If no case is a move valid, we return false
        return false;
    }

    /**
     * Chose next player for a given board
     *
     * @param board: the board to check who is the next player
     */
    private Board choseNextPlayer(Board board) {
        //If the player who didn't play has a valid move, it's his turn
        if (isThereAMoveValid(board, getOpposedColor(board.getNextPlayer()))) {
            board.setNextPlayer(getOpposedColor(board.getNextPlayer()));
            //Else, if the player who just played has a valid move, it's again his turn
        } else if (isThereAMoveValid(board, board.getNextPlayer())) {
            return board;
            //Else, nobody can play, so there is no next player
        } else {
            board.setNextPlayer(0);
        }
        return board;
    }

    /**
     * Get the winner corresponding to the board
     *
     * @param board: the board from which we determine the winner
     */
    public UserZero getWinner(Board board) {
        int[] score = getScore(board);
        if (score[0] > score[1])
            return board.getPlayer1().convertToUserZero();
        else if (score[1] > score[0])
            return board.getPlayer2().convertToUserZero();
        else
            return null;
    }

    /**
     * Returns an array containing player 1's score at index 0 and player 2's at index 1
     *
     * @param board: Board from which we have to count score
     */
    private int[] getScore(Board board) {
        int[] score = new int[2];
        //Travelling across the board and counting the number of 1 (representing black tiles)
        //and 2 (representing white tiles)
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board.getStates()[i][j] == 1)
                    score[0]++;
                else if (board.getStates()[i][j] == 2)
                    score[1]++;
            }
        }
        return score;
    }
}
