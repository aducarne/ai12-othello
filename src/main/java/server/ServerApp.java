package server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import server.data.DataCoreServer;
import server.network.ComCoreServer;

import javax.annotation.PostConstruct;
import java.util.Collections;

/**
 * Class running the server app
 */
@SpringBootApplication
public class ServerApp {
    private static final Logger LOGGER = LogManager.getLogger(ServerApp.class);

    @Autowired
    private ComCoreServer comCoreServer;
    @Autowired
    private DataCoreServer dataCoreServer;

    /**
     * Run the server app
     *
     * @param args additional args
     */
    public static void main(String[] args) {
        LOGGER.debug("ServerApp");
        SpringApplication app = new SpringApplication(ServerApp.class);
        app.setDefaultProperties(Collections
                .singletonMap("server.port", "8080"));
        app.run(args);
    }

    private void initCores() {
        dataCoreServer.setDataToCom(comCoreServer.getDataToComServerImpl());
        comCoreServer.setComToDataImpl(dataCoreServer.getComToDataServerImpl());
    }

    @PostConstruct
    public void init() {
        this.initCores();
        comCoreServer.initWebSocketConnection();
    }
}
