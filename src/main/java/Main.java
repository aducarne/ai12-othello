import client.ClientApp;
import server.ServerApp;

/**
 * Main application (use following arguments to run) <br>
 * <ul>
 * <li><b>-client</b> to run ClientApp
 * <li><b>-server</b> to run ServerApp
 * </ul>
 */
public class Main {
    /**
     * Option used to launch server application
     */
    private static final String SERVER_ARG_OPTION = "-server";

    /**
     * Option used to launch client application
     */
    private static final String CLIENT_ARG_OPTION = "-client";

    /**
     * Main Application entry point
     *
     * @param args
     */
    public static void main(String[] args) {
        /**
         * If we have an argument, we look for which application to run (server or
         * client)
         */
        if (args.length > 0) {
            if (args[0].equals(SERVER_ARG_OPTION)) {
                runServerApp(args);
            } else if (args[0].equals(CLIENT_ARG_OPTION)) {
                runClientApp(args);
            }
        } else { // Default application is the client
            runClientApp(args);
        }
    }

    /**
     * Calls the main method of the server application
     *
     * @param args
     */
    private static void runServerApp(String[] args) {
        ServerApp.main(args);
    }

    /**
     * Calls the main method of the client application
     *
     * @param args
     */
    private static void runClientApp(String[] args) {
        ClientApp.main(args);
    }

}
