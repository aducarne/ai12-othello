package client.main.model;

import client.main.controller.FXHome;
import common.model.GameLight;
import common.model.UserHeavy;
import common.model.UserLight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MainApplicationModel {
    private static final Logger LOGGER = LogManager.getLogger(MainApplicationModel.class);
    private UserLight userLight;
    private UserHeavy userHeavy;
    private List<UserLight> usersList;
    private List<GameLight> gamesList;
    private List<GameLight> gamesInProgressList;


    private FXHome fxHome;

    public MainApplicationModel() {
        this.userLight = null;
        this.userHeavy = null;
        this.usersList = new ArrayList<>();
        this.gamesList = new ArrayList<>();
        this.gamesInProgressList = new ArrayList<>();
    }

    public void clean() {
        this.userLight = null;
        this.userHeavy = null;
        this.usersList.clear();
        this.gamesList.clear();
        this.gamesInProgressList.clear();
    }

    public UserLight getUserLight() {
        return userLight;
    }

    public void setUserLight(UserLight userLight) {
        this.userLight = userLight;
    }

    public UserHeavy getUserHeavy() {
        return userHeavy;
    }

    public void setUserHeavy(UserHeavy userHeavy) {
        this.userHeavy = userHeavy;
    }

    public List<UserLight> getUsersList() {
        return usersList;
    }

    public List<GameLight> getGamesList() {
        return gamesList;
    }

    public List<GameLight> getGamesInProgressList() {
        return gamesInProgressList;
    }

    public void addUserInList(UserLight user) {
        this.getUsersList().add(user);
    }

    public void addGameInList(GameLight game) {
        this.getGamesList().add(game);
    }

    public void addGameInProgressInList(GameLight game) {
        this.getGamesInProgressList().add(game);
    }

    public UserLight removeUserInList() {
        return this.getUsersList().remove(this.getUsersList().size() - 1);
    }

    public GameLight removeGameInList() {
        return this.getGamesList().remove(this.getGamesList().size() - 1);
    }

    public GameLight removeGameInProgressInList() {
        GameLight gameRemoved = this.getGamesInProgressList().remove(this.getGamesInProgressList().size() - 1);
        return gameRemoved;
    }

    public FXHome getFxHome() {
        return fxHome;
    }

    public void setFxHome(FXHome fxHome) {
        this.fxHome = fxHome;
    }

    public void notifyControllers(String notification, Object data) {
        switch (notification) {
            case "updateData":
                if (this.fxHome != null) {
                    this.fxHome.updateData();
                }
                break;

            case "responseGameJoin":
                if (this.fxHome != null) {
                    this.fxHome.showBeforeGame((String) data);
                }
                break;

            default:
                LOGGER.debug("Controller notification: {} does not exist.", notification);
        }

    }
}
