package client.main.controller;

import client.data.Configuration;
import common.model.UserLight;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.UUID;


public class FXProfileConsult implements Initializable {
    private static final Logger LOGGER = LogManager.getLogger(FXProfileConsult.class);
    private MainControllerFX mainController;
    private Stage stage;
    @FXML
    private Button viewProfileButtonEdit;

    @FXML
    private Button viewProfileButtonExport;

    @FXML
    private Text displayProfilePseudo;

    @FXML
    private Text displayProfileLastName;

    @FXML
    private Text displayProfileFirstName;

    @FXML
    private Text displayProfileAge;

    @FXML
    private Text displayProfileNbVictories;

    @FXML
    private Text displayProfileNbGames;
    private UUID userId;
    private UserLight userToDisplay = null;

    public FXProfileConsult() {
        stage = new Stage();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setUserToDisplay() {

        for (Integer i = 0; i < this.getMainController().getModel().getUsersList().size(); i++) {
            if (this.getMainController().getModel().getUsersList().get(i).getId().equals(userId)) {
                userToDisplay = this.getMainController().getModel().getUsersList().get(i);
                this.viewProfileButtonEdit.setVisible(false);
                this.viewProfileButtonExport.setVisible(false);
            }
        }

        if (userToDisplay == null) {
            userToDisplay = this.getMainController().getModel().getUserLight();
        }

        Date date = userToDisplay.getDateOfBirth();
        LocalDate birthdate = null;

        if (date != null) {
            int age;
            birthdate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            Date now = new Date();
            LocalDate today = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            age = Period.between(birthdate, today).getYears();
            displayProfileAge.setText(Integer.toString(age));
        } else {
            displayProfileAge.setText(null);
        }

        displayProfileFirstName.setText(userToDisplay.getFirstName());
        displayProfileLastName.setText(userToDisplay.getLastName());
        displayProfilePseudo.setText(userToDisplay.getPseudo());
        displayProfileNbGames.setText(Integer.toString(userToDisplay.getPlayedGames()));
        displayProfileNbVictories.setText(Integer.toString(userToDisplay.getWonGames()));

    }

    public void initialize(URL fxmlResource, ResourceBundle resource) {
        setUserToDisplay();
    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws IOException {
        String buttonSource = ((Button) e.getSource()).getId();
        UserLight userLight = this.getMainController().getModel().getUserLight();
        switch (buttonSource) {
            case "viewProfileButtonClose":
                this.stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                FXMLLoader fxmlLoader = new FXMLLoader(getClass()
                        .getResource("../fxml/home.fxml"));
                BorderPane root = fxmlLoader.load();
                FXHome controller = fxmlLoader.getController();
                controller.setMainController(this.getMainController());
                Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
                controller.launch(scene, stage);

                this.getMainController().getModel().notifyControllers("updateData", null);
                break;
            case "viewProfileButtonEdit":
                FXMLLoader loaderEdit = new FXMLLoader(getClass().getResource("../fxml/modifyProfile.fxml"));
                loaderEdit.setControllerFactory(c -> {
                    FXModifyProfile fxModifyProfile = new FXModifyProfile();
                    fxModifyProfile.setMainController(getMainController());
                    return fxModifyProfile;
                });
                Parent padreEditProfile = loaderEdit.load();
                Scene switcherEditProfile = new Scene(padreEditProfile);
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                stage.setScene(switcherEditProfile);
                break;
            case "viewProfileButtonExport":
                String userFilePath = String.format("%s%s%s.json", Configuration.SAVE_DIR_USERS, File.separator, userLight.getPseudo());
                String directoryCible;
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new java.io.File("."));
                chooser.setDialogTitle("Choix du dossier destinataire");
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
                //
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    directoryCible = chooser.getSelectedFile().getPath();
                    Files.copy(Paths.get(userFilePath), Paths.get(String.format("%s%s%s.json", directoryCible, File.separator, userLight.getPseudo())), StandardCopyOption.REPLACE_EXISTING);
                    JOptionPane.showMessageDialog(null, String.format("Profil exporté avec succès dans %s%s%s.json", directoryCible, File.separator, userLight.getPseudo()));
                } else {
                    LOGGER.info("No Selection ");
                }

                break;

            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", buttonSource);
        }
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
    }
}
