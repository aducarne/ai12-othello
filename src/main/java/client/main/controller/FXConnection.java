package client.main.controller;

import client.data.Configuration;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import common.exceptions.OthelloException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.StringUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class FXConnection {
    private static final Logger LOGGER = LogManager.getLogger(FXConnection.class);
    private MainControllerFX mainController;
    private Stage stage;
    @FXML
    private TextField loginTextFieldIdentifier;

    @FXML
    private PasswordField loginPasswordFieldPassword;

    @FXML
    private TextField loginTextFieldIPServerAddress;

    @FXML
    private TextField loginTextFieldPortNumber;

    @FXML
    private Text loginTextFieldMissing;

    @FXML
    private Text loginTextConnectionError;

    @FXML
    private TextField createAccountTextFieldName;

    @FXML
    private TextField createAccountTextFieldFirstName;

    @FXML
    private PasswordField createAccountPasswordFieldPassword;

    @FXML
    private DatePicker createAccountTextFieldBirthDate;

    @FXML
    private TextField createAccountTextFieldPseudo;

    @FXML
    private Text errorFieldsPseudo;

    @FXML
    private Text errorFieldsBirthdate;


    public FXConnection() {
        stage = new Stage();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void launch(Scene myScene, Stage stage) {
        setStage(stage);
        myScene.getStylesheets().add(Configuration.CSS_FILE_PATH);
        stage.setTitle(Configuration.APPLICATION_NAME);
        stage.setScene(myScene);
        stage.show();
    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws Exception {
        String buttonSource = ((Button) e.getSource()).getId();
        switch (buttonSource) {
            case "loginButtonCreateAccount":
                stage.setScene(createAccountScene());
                break;

            case "createAccountButtonValidate":

                errorFieldsPseudo.setVisible(false);
                errorFieldsBirthdate.setVisible(false);

                if (StringUtils.isEmpty(createAccountPasswordFieldPassword.getText())
                        || StringUtils.isEmpty(createAccountTextFieldPseudo.getText())) {
                    LOGGER.info("At least one field is missing.");
                } else if (createAccountTextFieldPseudo.getText().length() > Configuration.PSEUDO_MAX_LENGTH) {
                    errorFieldsPseudo.setVisible(true);
                    LOGGER.info("The pseudo length must be equal or lower than " + Configuration.PSEUDO_MAX_LENGTH);
                } else {
                    Date date = new Date();
                    Boolean errorDate = false;

                    if (this.createAccountTextFieldBirthDate.getValue() != null) {
                        LocalDate localDate = this.createAccountTextFieldBirthDate.getValue();
                        date = java.util.Date.from(localDate.atStartOfDay()
                                .atZone(ZoneId.systemDefault())
                                .toInstant());
                        Date now = new Date();
                        LocalDate today = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        errorDate = localDate.compareTo(today) > 0 ? true : false;
                    } else {
                        date = null;
                    }


                    try {

                        if (errorDate) {
                            LOGGER.info("The birthdate must be earlier than today");
                            errorFieldsBirthdate.setVisible(true);
                        } else {

                            getMainController().getMainCore().getMainToData().sendUserInfo(this.createAccountTextFieldPseudo.getText(),
                                    this.createAccountTextFieldName.getText(), this.createAccountTextFieldFirstName.getText(),
                                    date, 0, this.createAccountPasswordFieldPassword.getText());
                            this.stage.setScene(loginScene());
                        }

                    } catch (Exception exception) {
                        LOGGER.error("Exception caught: {}", exception);
                    }
                }
                break;

            case "createAccountButtonCancel":
                stage.setScene(loginScene());
                break;

            // login attempt
            case "loginButtonValidate":
                loginTextConnectionError.setVisible(false);
                loginTextFieldMissing.setVisible(false);
                if (StringUtils.isEmpty(loginTextFieldIdentifier.getText())
                        || StringUtils.isEmpty(loginPasswordFieldPassword.getText())) {
                    loginTextFieldMissing.setVisible(true);
                    LOGGER.info("Missing required fields.");
                } else {
                    if (StringUtils.isEmpty(loginTextFieldIPServerAddress.getText())) {
                        loginTextFieldIPServerAddress.setText(Configuration.DEFAULT_SERVER_ADDRESS);
                    }
                    if (StringUtils.isEmpty(loginTextFieldPortNumber.getText())) {
                        loginTextFieldPortNumber.setText(Configuration.DEFAULT_SERVER_PORT);
                    }
                    String hostname = loginTextFieldIPServerAddress.getText() + ":" + loginTextFieldPortNumber.getText();
                    try {
                        getMainController().getModel().setUserLight(null);
                        // identification attempt
                        getMainController().getMainCore().getMainToData().identify(loginTextFieldIdentifier.getText(),
                                loginPasswordFieldPassword.getText(), hostname);

                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/home.fxml"));
                        BorderPane root = fxmlLoader.load();
                        FXHome controller = fxmlLoader.getController();
                        controller.setMainController(getMainController());
                        Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
                        controller.launch(scene, stage);

                    } catch (Exception exception) {
                        loginTextConnectionError.setVisible(true);
                        LOGGER.error("Exception caught: {}", exception);
                    }

                }

                break;
            case "loginButtonImportAccount":
            case "createAccountButtonImportProfile":
                String nameFile;
                String filesource;
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichiers de sauvegarde (.json)", "JSON",
                        "json");
                chooser.setAcceptAllFileFilterUsed(false);
                chooser.setFileFilter(filter);
                chooser.setCurrentDirectory(new java.io.File("."));
                chooser.setDialogTitle("Selection du profil à importer");
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    filesource = chooser.getSelectedFile().getPath();
                    nameFile = new File(filesource).getName();
                    if (checkJsonFile(filesource)) {
                        Files.copy(Paths.get(filesource),
                                Paths.get(Configuration.SAVE_DIR_USERS + File.separator + nameFile),
                                StandardCopyOption.REPLACE_EXISTING);
                        JOptionPane.showMessageDialog(null, "Profil importé avec succès");
                        stage.setScene(loginScene());
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Format du fichier non pris en charge (données manquantes ou nom de fichier erroné)");
                    }

                } else {
                    LOGGER.debug("No selected profile.");
                }
                break;
            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", buttonSource);
        }
    }

    public boolean checkJsonFile(String path) throws OthelloException, IOException {
        boolean checkAttributesNotNull = false;
        boolean checkAttributesNotEmpty = false;
        boolean matchFileNamePseudo = false;
        File userFile = new File(path);
        if (!userFile.exists()) {
            throw new OthelloException(Configuration.USER_PROFILE_DOES_NOT_EXIST);
        }
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(new String(Files.readAllBytes(Paths.get(path))));
        if (!jsonElement.isJsonNull()) {
            JsonObject userObject = jsonElement.getAsJsonObject();
            JsonElement userObjectPseudo = userObject.get("pseudo");
            checkAttributesNotNull = !userObject.isJsonNull() && userObjectPseudo != null
                    && userObject.get("id") != null && !userObjectPseudo.isJsonNull()
                    && !userObject.get("id").isJsonNull();
            if (checkAttributesNotNull) {
                checkAttributesNotEmpty = !userObjectPseudo.getAsString().isEmpty()
                        && !userObject.get("id").getAsString().isEmpty();
                if (checkAttributesNotEmpty) {
                    matchFileNamePseudo = new File(path).getName().equals(userObjectPseudo.getAsString() + ".json");
                }
            }
        }

        return matchFileNamePseudo && checkAttributesNotNull && checkAttributesNotEmpty;

    }

    private Scene createAccountScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/createAccount.fxml"));
        fxmlLoader.setControllerFactory(c -> {
            return this;
        });
        BorderPane root = fxmlLoader.load();

        return setNewScene(fxmlLoader, root);
    }

    private Scene loginScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/login.fxml"));
        fxmlLoader.setControllerFactory(c -> {
            return this;
        });
        BorderPane root = fxmlLoader.load();

        return setNewScene(fxmlLoader, root);
    }

    private Scene setNewScene(FXMLLoader loader, Pane root) {
        FXConnection controller = loader.getController();
        controller.setStage(stage);

        Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
        scene.getStylesheets().add(Configuration.CSS_FILE_PATH);

        return scene;
    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
    }
}
