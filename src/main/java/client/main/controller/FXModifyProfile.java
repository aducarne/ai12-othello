package client.main.controller;

import client.data.Configuration;
import common.model.UserHeavy;
import common.model.UserLight;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;


public class FXModifyProfile implements Initializable {
    private static final Logger LOGGER = LogManager.getLogger(FXModifyProfile.class);
    private MainControllerFX mainController;
    private Stage stage;

    @FXML
    private TextField modifyProfileTextFieldName;

    @FXML
    private TextField modifyProfileTextFieldFirstName;

    @FXML
    private PasswordField modifyProfileTextFieldPassword;

    @FXML
    private DatePicker modifyProfileTextFieldBirthDate;

    @FXML
    private TextField modifyProfileTextFieldPseudo;

    @FXML
    private Text errorFieldsPseudo;

    @FXML
    private Text errorFieldsBirthdate;


    public FXModifyProfile() {
        stage = new Stage();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void initialize(URL fxmlResource, ResourceBundle resource) {

        UserLight user = this.getMainController().getModel().getUserLight();

        Date date = user.getDateOfBirth();

        if (date != null) {
            LocalDate localdate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            modifyProfileTextFieldBirthDate.setValue(localdate);
        }

        modifyProfileTextFieldName.setText(user.getLastName());
        modifyProfileTextFieldFirstName.setText(user.getFirstName());
        modifyProfileTextFieldPseudo.setText(user.getPseudo());
        modifyProfileTextFieldPassword.setText("");

    }

    private void goToProfile(MouseEvent e) throws IOException {
        FXMLLoader loaderHomeButtonMyProfile = new FXMLLoader(getClass().getResource("../fxml/viewProfile.fxml"));
        loaderHomeButtonMyProfile.setControllerFactory(c -> {
            FXProfileConsult fxProfileConsult = new FXProfileConsult();
            fxProfileConsult.setMainController(getMainController());
            return fxProfileConsult;
        });
        Parent padreHomeButtonMyProfile = loaderHomeButtonMyProfile.load();
        Scene switcherHomeButtonMyProfile = new Scene(padreHomeButtonMyProfile);
        stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setScene(switcherHomeButtonMyProfile);
    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws IOException {
        String buttonSource = ((Button) e.getSource()).getId();
        switch (buttonSource) {
            case "modifyProfileButtonCancel":
                goToProfile(e);
                break;
            case "modifyProfileButtonValidate":
                Boolean modificationOk = updateUserProfile();
                if (Boolean.TRUE.equals(modificationOk)) {
                    goToProfile(e);
                }
                break;
            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", buttonSource);
        }
    }


    private Boolean updateUserProfile() {

        errorFieldsPseudo.setVisible(false);
        errorFieldsBirthdate.setVisible(false);

        if (modifyProfileTextFieldPseudo.getText().length() > Configuration.PSEUDO_MAX_LENGTH) {
            errorFieldsPseudo.setVisible(true);
            LOGGER.info("The pseudo length must be equal or lower than " + Configuration.PSEUDO_MAX_LENGTH);
            return false;
        } else {
            Date date;
            Boolean errorDate = false;

            if (this.modifyProfileTextFieldBirthDate.getValue() != null) {
                LocalDate localDate = this.modifyProfileTextFieldBirthDate.getValue();
                Date now = new Date();
                LocalDate today = now.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                errorDate = localDate.compareTo(today) > 0;
            }


            try {

                if (Boolean.TRUE.equals(errorDate)) {
                    LOGGER.info("The birthdate must be earlier than today");
                    errorFieldsBirthdate.setVisible(true);
                    return false;
                } else {

                    UserLight userLight = this.getMainController().getModel().getUserLight();
                    UserHeavy userHeavy = this.getMainController().getModel().getUserHeavy();

                    LocalDate localDate = this.modifyProfileTextFieldBirthDate.getValue();


                    if (!this.modifyProfileTextFieldPassword.getText().isEmpty()) {
                        userHeavy.setPassword(this.modifyProfileTextFieldPassword.getText());
                    }

                    if (localDate != null) {
                        date = Date.from(localDate.atStartOfDay()
                                .atZone(ZoneId.systemDefault())
                                .toInstant());
                    } else {
                        date = null;
                    }

                    userLight.setFirstName(this.modifyProfileTextFieldFirstName.getText());
                    userLight.setLastName(this.modifyProfileTextFieldName.getText());
                    userLight.setPseudo(this.modifyProfileTextFieldPseudo.getText());
                    userLight.setDateOfBirth(date);

                    this.getMainController().getMainCore().getMainToData().updateProfile(userLight, userHeavy);
                    return true;

                }

            } catch (Exception exception) {
                LOGGER.debug("Exception caught: {}", exception);
            }
        }
        return false;

    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
    }
}
