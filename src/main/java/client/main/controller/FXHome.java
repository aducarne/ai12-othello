package client.main.controller;

import client.data.Configuration;
import common.model.GameLight;
import common.model.UserLight;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class FXHome {
    private static final Logger LOGGER = LogManager.getLogger(FXHome.class);
    private MainControllerFX mainController;
    private Stage stage;
    private UserLight item;
    @FXML
    private Button homeButtonMyProfile;
    @FXML
    private ListView<UserLight> homeListViewUsersOnline;
    private ObservableList<UserLight> homeListViewUsersOnlineData = FXCollections.observableArrayList();
    @FXML
    private ListView<GameLight> homeListViewGamesAvailable;
    private ObservableList<GameLight> homeListViewGamesAvailableData = FXCollections.observableArrayList();
    @FXML
    private ListView<GameLight> homeListViewOnGoingGames;
    private ObservableList<GameLight> homeListViewOnGoingGamesData = FXCollections.observableArrayList();
    public FXHome() {
        stage = new Stage();
        item = new UserLight();
    }

    public void setItemAttribute(UserLight user) {
        item = user;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public UserLight getUser() {
        return item;
    }

    public void initialize(String fxmlResource, Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlResource));
        BorderPane root = fxmlLoader.load();
        FXHome controller = fxmlLoader.getController();
        controller.setMainController(mainController);
        controller.setStage(stage);

        Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
        scene.getStylesheets().add(Configuration.CSS_FILE_PATH);

        stage.setTitle(Configuration.APPLICATION_NAME + (item != null ? " - " + item.getPseudo() : ""));

        stage.setScene(scene);
        stage.show();
    }

    public void launch(Scene scene, Stage stage) {
        setStage(stage);
        scene.getStylesheets().add(Configuration.CSS_FILE_PATH);
        stage.setTitle(Configuration.APPLICATION_NAME + (item != null ? " - " + item.getPseudo() : ""));
        stage.setScene(scene);
        stage.show();
    }

    public void updateData() {

        Task<Void> usersListTask = getUsersListTask();
        Task<Void> availableGamesListTask = getAvailableGamesListTask();
        Task<Void> onGoingGamesListTask = getonGoingGamesListTask();

        usersListTask.run();
        availableGamesListTask.run();
        onGoingGamesListTask.run();
    }

    public Task<Void> getUsersListTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                // add each object into data for view. Platform class is used in order to avoid
                Platform.runLater(() -> {
                    // clear all data
                    homeListViewUsersOnline.getItems().clear();
                    homeListViewUsersOnlineData.addAll(mainController.getModel().getUsersList());
                    // adds data to the listView object
                    homeListViewUsersOnline.setItems(homeListViewUsersOnlineData);
                    // uses data to create visual cells, with their events
                    homeListViewUsersOnline.setCellFactory(list -> new ListCell<UserLight>() {
                        @Override
                        protected void updateItem(UserLight item, boolean empty) {
                            super.updateItem(item, empty);

                            if (item == null || empty) {
                                setText(null);
                            } else {
                                setText(item.getPseudo());

                                Button seeProfil = new Button("Voir Profil");
                                setGraphic(seeProfil);
                                seeProfil.setId("seeProfil");

                                seeProfil.setOnMouseClicked(event -> {
                                    try {
                                        setItemAttribute(item);
                                        handleButtonClick(event);
                                    } catch (IOException e) {
                                        LOGGER.error("seeProfile", e);
                                    }
                                });
                            }

                        }
                    });
                    // Handle ListView selection changes.
                    homeListViewUsersOnline.getSelectionModel().selectedItemProperty()
                            .addListener((observable, oldValue, newValue) -> LOGGER
                                    .debug("Users connected Selection Changed (selected: {})", newValue));
                    homeListViewUsersOnline.refresh();
                });

                return null;
            }
        };
    }

    public Task<Void> getAvailableGamesListTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                Platform.runLater(() -> {
                    homeListViewGamesAvailable.getItems().clear();
                    homeListViewGamesAvailableData.addAll(mainController.getModel().getGamesList());
                    homeListViewGamesAvailable.setItems(homeListViewGamesAvailableData);
                    homeListViewGamesAvailable.setCellFactory(list -> new ListCell<GameLight>() {
                        Button joinButton = new Button("Join");

                        @Override
                        protected void updateItem(GameLight item, boolean empty) {
                            super.updateItem(item, empty);

                            if (item == null || empty) {
                                setText(null);
                            } else {
                                setText(item.getGameCreator().getPseudo());
                                setGraphic(joinButton);
                            }

                            joinButton.setOnAction(e -> {
                                setText("DEMANDE ENVOYEE //" + getText());
                                homeListViewGamesAvailable.setDisable(true);
                                getMainController().getMainCore().getMainToData().selectGame(item, stage);
                                Timer timer = new Timer();
                                TimerTask task = new TimerTask() {
                                    @Override
                                    public void run() {
                                        if (homeListViewGamesAvailable.isDisabled()) {
                                            homeListViewGamesAvailable.setDisable(false);
                                            setText(getText().replace("DEMANDE ENVOYEE //", ""));
                                            showBeforeGame("none");
                                        }
                                    }

                                };
                                timer.schedule(task, 20000l);
                            });
                        }
                    });

                    homeListViewGamesAvailable.getSelectionModel().selectedItemProperty()
                            .addListener((observable, oldValue, newValue) -> LOGGER
                                    .debug("Games available Selection Changed (selected: {})", newValue));

                    homeListViewGamesAvailable.refresh();
                });

                return null;
            }
        };
    }

    public Task<Void> getonGoingGamesListTask() {
        return new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                Platform.runLater(() -> {
                    homeListViewOnGoingGames.getItems().clear();
                    homeListViewOnGoingGamesData.addAll(getMainController().getModel().getGamesInProgressList());
                    homeListViewOnGoingGames.setItems(homeListViewOnGoingGamesData);
                    homeListViewOnGoingGames.setCellFactory(list -> {
                        return new ListCell<GameLight>() {
                            Button spectateButton = new Button("Spectate");

                            @Override
                            protected void updateItem(GameLight item, boolean empty) {
                                super.updateItem(item, empty);

                                if (item == null || empty) {
                                    setText(null);
                                } else {
                                    setText(item.getGameCreator().getPseudo() + " vs " + item.getPlayer2().getPseudo());
                                    if (item.isOkSpectators())
                                        setGraphic(spectateButton);
                                }

                                spectateButton.setOnAction((e) -> {
                                    setText("Demande Envoyée //" + getText());
                                    homeListViewOnGoingGames.setDisable(true);
                                    getMainController().getMainCore().getMainToData().requestToDataToSpecialGame(item,
                                            getUser(), stage);
                                    Timer timer = new Timer();
                                    TimerTask task = new TimerTask() {
                                        @Override
                                        public void run() {
                                            if (homeListViewOnGoingGames.isDisabled()) {
                                                homeListViewOnGoingGames.setDisable(false);
                                                setText(getText().replace("Demande Envoyée //", ""));
                                                showBeforeGame("none");
                                            }
                                        }

                                    };
                                    timer.schedule(task, 20000l);
                                });

                            }
                        };
                    });

                    homeListViewOnGoingGames.getSelectionModel().selectedItemProperty()
                            .addListener((observable, oldValue, newValue) -> {
                                System.out.println(
                                        "On Going Games Selection Changed (selected: " + newValue.toString() + ")");
                            });

                    homeListViewOnGoingGames.refresh();
                });

                return null;
            }
        };
    }

    public void showBeforeGame(String response) {
        if (response == null) {
            return;
        }
        int toastMsgTime = 2000;
        int fadeInTime = 100;
        int fadeOutTime = 100;
        if (response.equals("ok")) {
            homeListViewGamesAvailable.setDisable(false);
            Platform.runLater(() -> Toast.makeText(stage, "L'hébergeur a accepté votre demande.", toastMsgTime,
                    fadeInTime, fadeOutTime));
        } else if (response.equals("nok")) {
            Platform.runLater(() -> Toast.makeText(stage, "L'hébergeur a refusé votre demande.", toastMsgTime,
                    fadeInTime, fadeOutTime));
        } else if (response.equals("none")) {
            Platform.runLater(() -> Toast.makeText(stage, "Aucune réponse de la part de l'hébergeur.", toastMsgTime,
                    fadeInTime, fadeOutTime));
        }

    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws IOException {
        String buttonSource = ((Button) e.getSource()).getId();
        switch (buttonSource) {
            case "homeButtonMyProfile":
                FXMLLoader loaderHomeButtonMyProfile = new FXMLLoader(getClass().getResource("../fxml/viewProfile.fxml"));
                loaderHomeButtonMyProfile.setControllerFactory(c -> {
                    FXProfileConsult fxProfileConsult = new FXProfileConsult();
                    fxProfileConsult.setMainController(getMainController());
                    return fxProfileConsult;
                });
                Parent padreHomeButtonMyProfile = loaderHomeButtonMyProfile.load();
                Scene switcherHomeButtonMyProfile = new Scene(padreHomeButtonMyProfile);
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                stage.setScene(switcherHomeButtonMyProfile);

                break;
            case "homeButtonCreateGame":
                FXMLLoader loader = new FXMLLoader(getClass().getResource("../fxml/createGame.fxml"));
                loader.setControllerFactory(c -> {
                    FXCreateGame fxCreateGame = new FXCreateGame();
                    fxCreateGame.setMainController(getMainController());
                    return fxCreateGame;
                });
                Parent padre = loader.load();
                Scene switcher = new Scene(padre);
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                stage.setScene(switcher);
                break;
            case "homeDeconnection":
                getMainController().getMainCore().getMainToData().disconnect(); // clean model from the server side
                getMainController().getModel().clean(); // clean model from the view side
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/login.fxml"));
                BorderPane root = fxmlLoader.load();
                FXConnection controller = fxmlLoader.getController();
                controller.setMainController(getMainController());

                Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);

                controller.launch(scene, stage);

                break;
            case "seeProfil":
                FXMLLoader loader2 = new FXMLLoader(
                        new URL(FXHome.class.getResource("../fxml/viewProfile.fxml").toExternalForm()));
                loader2.setControllerFactory(c -> {
                    FXProfileConsult fxProfileConsult = new FXProfileConsult();
                    fxProfileConsult.setMainController(getMainController());
                    return fxProfileConsult;
                });
                Parent padre2 = loader2.load();
                FXProfileConsult fxProfile = loader2.getController();
                fxProfile.setUserId(item.getId());
                fxProfile.setUserToDisplay();

                Scene switcher2 = new Scene(padre2);
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                stage.setScene(switcher2);
                break;
            case "homeButtonSavedGames":
                FXMLLoader loader3 = new FXMLLoader(
                        new URL(FXHome.class.getResource("../fxml/savedGames.fxml").toExternalForm()));
                loader3.setControllerFactory(c -> {
                    FXSavedGames fxSaves = new FXSavedGames();
                    fxSaves.setMainController(getMainController());
                    return fxSaves;
                });
                Parent padre3 = loader3.load();
                Scene switcher3 = new Scene(padre3);
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
                stage.setScene(switcher3);

                FXSavedGames fxSaves = loader3.getController();
                fxSaves.updateData();
                break;
            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", buttonSource);
        }
    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
        this.mainController.getModel().setFxHome(this);
        item = this.mainController.getModel().getUserLight();
    }
}
