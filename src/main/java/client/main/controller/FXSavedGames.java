package client.main.controller;

import client.data.Configuration;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.text.ParseException;

public class FXSavedGames {
    private static final Logger LOGGER = LogManager.getLogger(FXSavedGames.class);
    private MainControllerFX mainController;
    private Stage stage;

    @FXML
    private ListView<String> availableGames;
    private ObservableList<String> availableGamesData = FXCollections.observableArrayList();

    public FXSavedGames() {
        stage = new Stage();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void updateData() {

        Task<Void> availableGamesListTask = new Task<Void>() {

            @Override
            protected Void call() throws Exception {

                Platform.runLater(() -> {
                    availableGames.getItems().clear();
                    availableGamesData.addAll(mainController.getMainCore().getMainToData().getSavedGames());
                    availableGames.setItems(availableGamesData);
                    availableGames.setCellFactory(list -> new ListCell<String>() {
                        Button replayGameButton = new Button("Replay");

                        @Override
                        protected void updateItem(String item, boolean empty) {
                            super.updateItem(item, empty);

                            if (item == null || empty) {
                                setText(null);
                            } else {
                                setText(item);
                                setGraphic(replayGameButton);
                            }

                            replayGameButton.setOnAction(e -> {
                                availableGames.setDisable(true);
                                try {
                                    mainController.getMainCore().getMainToData().replayGame(
                                            (Stage) ((Node) e.getSource()).getScene().getWindow(), item);
                                } catch (IOException | ParseException e1) {
                                    LOGGER.error("Could not parse Game file. {}", e);
                                }
                            });
                        }
                    });
                    // Handle ListView selection changes.
                    availableGames.getSelectionModel().selectedItemProperty()
                            .addListener((observable, oldValue, newValue) -> LOGGER.debug("Users connected Selection Changed (selected: {})", newValue));
                    availableGames.refresh();
                    LOGGER.debug(availableGames.getItems().size());
                });
                return null;
            }
        };
        availableGamesListTask.run();
    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws IOException {
        String buttonSource = ((Button) e.getSource()).getId();
        switch (buttonSource) {
            case "homeReturn":
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();

                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../fxml/home.fxml"));
                BorderPane root = fxmlLoader.load();
                FXHome controller = fxmlLoader.getController();
                controller.setMainController(getMainController());
                Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
                controller.launch(scene, stage);

                getMainController().getModel().notifyControllers("updateData", null);
                break;
            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", buttonSource);
        }
    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
    }

}
