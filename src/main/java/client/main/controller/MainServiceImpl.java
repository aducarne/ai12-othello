package client.main.controller;

import common.interfaces.client.IMainService;

public class MainServiceImpl implements IMainService {
    private MainCore controller;

    public MainServiceImpl(MainCore controller) {
        this.setController(controller);
    }

    public MainCore getController() {
        return controller;
    }

    public void setController(MainCore m) {
        this.controller = m;
    }

    @Override
    public void seeClear() {
        // TODO : do its implementation
    }

    @Override
    public void avatarImport() {
        // TODO : do its implementation
    }
}
