package client.main.controller;

import client.data.Configuration;
import client.main.model.MainApplicationModel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class MainControllerFX {
    private MainCore mainCore;
    private MainApplicationModel model;

    public MainControllerFX() {
        model = new MainApplicationModel();
    }

    public MainApplicationModel getModel() {
        return this.model;
    }

    public void launch(String fxmlResource, Stage stage, MainCore mainCore) throws IOException {
        this.setMainCore(mainCore);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass()
                .getResource(fxmlResource));
        BorderPane root = fxmlLoader.load();
        FXConnection controller = fxmlLoader.getController();
        controller.setMainController(this);

        Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);

        controller.launch(scene, stage);
    }

    public MainCore getMainCore() {
        return mainCore;
    }

    public void setMainCore(MainCore mainCore) {
        this.mainCore = mainCore;
    }

}
