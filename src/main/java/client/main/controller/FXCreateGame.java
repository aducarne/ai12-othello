package client.main.controller;

import client.data.Configuration;
import common.model.UserLight;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class FXCreateGame implements Initializable {

    private static final Logger LOGGER = LogManager.getLogger(FXCreateGame.class);
    private MainControllerFX mainController;
    private Stage stage;
    private boolean limitMoveSelected = false;

    @FXML
    private RadioButton spectatorsNo = new RadioButton();
    @FXML
    private RadioButton spectatorsYes = new RadioButton();
    @FXML
    private RadioButton chatNo = new RadioButton();
    @FXML
    private RadioButton chatYes = new RadioButton();
    @FXML
    private RadioButton colorWhite = new RadioButton();
    @FXML
    private RadioButton colorBlack = new RadioButton();

    @FXML
    private TextField createGameTextFieldLimitMove;

    @FXML
    private Text errorFields;

    @FXML
    private Text errorFieldsInteger;

    @FXML
    private CheckBox createGameCheckBoxLimitMove;

    public FXCreateGame() {
        stage = new Stage();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {

        errorFields.setVisible(false);
        errorFields.setFill(Color.RED);

        errorFieldsInteger.setVisible(false);
        errorFieldsInteger.setFill(Color.RED);

        configureCheckBox(this.createGameCheckBoxLimitMove);

        ToggleGroup toggleChat = new ToggleGroup();
        this.chatNo.setToggleGroup(toggleChat);
        this.chatYes.setToggleGroup(toggleChat);

        ToggleGroup toggleSpectators = new ToggleGroup();
        this.spectatorsNo.setToggleGroup(toggleSpectators);
        this.spectatorsYes.setToggleGroup(toggleSpectators);

        ToggleGroup toggleColor = new ToggleGroup();
        this.colorWhite.setToggleGroup(toggleColor);
        this.colorBlack.setToggleGroup(toggleColor);

    }

    @FXML
    private void handleButtonClick(MouseEvent e) throws IOException {
        String buttonSource = ((Button) e.getSource()).getId();
        switch (buttonSource) {
            case "createGameButtonValidate":
                createGame((Stage) ((Node) e.getSource()).getScene().getWindow());
                break;
            case "createGameButtonClose":
                stage = (Stage) ((Node) e.getSource()).getScene().getWindow();

                FXMLLoader fxmlLoader = new FXMLLoader(getClass()
                        .getResource("../fxml/home.fxml"));
                BorderPane root = fxmlLoader.load();
                FXHome controller = fxmlLoader.getController();
                controller.setMainController(this.getMainController());
                Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
                controller.launch(scene, stage);

                this.getMainController().getModel().notifyControllers("updateData", null);
                break;
            default:
                LOGGER.debug("{} A handler button was triggered, but no action was performed.", buttonSource);
        }
    }

    @FXML
    private void handleCheckBoxClick(MouseEvent e) {
        String checkBoxSource = ((CheckBox) e.getSource()).getId();
        switch (checkBoxSource) {
            case "createGameCheckBoxLimitMove":
                if (this.limitMoveSelected) {
                    this.createGameTextFieldLimitMove.setText("0");
                    this.createGameTextFieldLimitMove.setVisible(false);
                } else {
                    this.createGameTextFieldLimitMove.setText("");
                    this.createGameTextFieldLimitMove.setVisible(true);
                }
                break;
            default:
                LOGGER.debug("[{}] A handler button was triggered, but no action was performed.", checkBoxSource);
        }
    }

    private void createGame(Stage fxStage) {

        UserLight userLight = this.getMainController().getModel().getUserLight();

        try {
            boolean chat = chatYes.isSelected();
            boolean spectators = spectatorsYes.isSelected();
            boolean colorW = colorWhite.isSelected();
            int limitMove = Integer.parseInt(createGameTextFieldLimitMove.getText());

            BorderPane layout = FXMLLoader.load(new URL(client.main.controller.FXHome.class
                    .getResource("../fxml/createGame.fxml").toExternalForm()));

            Scene scene = new Scene(layout, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
            scene.getStylesheets().add(Configuration.CSS_FILE_PATH);

            LOGGER.debug("{} {} {} {}", chat, spectators, colorW, limitMove);
            this.getMainController().getMainCore().getMainToData().newGame(userLight, spectators, chat, colorW, limitMove, fxStage);
        } catch (NumberFormatException e) {
            errorFieldsInteger.setVisible(true);
        } catch (Exception e) {
            if (!(chatNo.isSelected() || chatYes.isSelected()) || !(spectatorsNo.isSelected() || spectatorsYes.isSelected())
                    || !(colorBlack.isSelected() || colorWhite.isSelected())
                    || createGameTextFieldLimitMove.getText().trim().isEmpty()) {
                errorFields.setVisible(true);
            } else {
                LOGGER.debug(e);
            }
        }

    }

    private void configureCheckBox(CheckBox checkBox) {

        checkBox.selectedProperty().addListener((obs, wasSelected, isNowSelected) -> {
            if (Boolean.TRUE.equals(isNowSelected)) {
                this.limitMoveSelected = true;
            } else {
                this.limitMoveSelected = false;
            }

        });

    }

    public MainControllerFX getMainController() {
        return mainController;
    }

    public void setMainController(MainControllerFX mainController) {
        this.mainController = mainController;
    }
}
