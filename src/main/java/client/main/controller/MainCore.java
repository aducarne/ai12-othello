package client.main.controller;

import client.main.DataToMainImpl;
import common.interfaces.client.IMainToData;

public class MainCore {
    private IMainToData mainToData;
    private DataToMainImpl dataToMain;
    private MainControllerFX mainControllerFx;

    public MainCore() {
        this.setMainControllerFx(new MainControllerFX());
        this.getFX().setMainCore(this);
        this.dataToMain = new DataToMainImpl(this);
    }

    public MainCore(MainControllerFX mcfx) {
        this.setMainControllerFx(mcfx);
        this.dataToMain = new DataToMainImpl(this);
    }

    public void setMainControllerFx(MainControllerFX mcfx) {
        this.mainControllerFx = mcfx;
    }

    public MainControllerFX getFX() {
        return mainControllerFx;
    }

    public IMainToData getMainToData() {
        return mainToData;
    }

    public void setMainToData(IMainToData mainToData) {
        this.mainToData = mainToData;
    }

    public DataToMainImpl getDataToMainImpl() {
        return dataToMain;
    }
}
