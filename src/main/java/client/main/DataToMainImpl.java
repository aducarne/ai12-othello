package client.main;

import client.data.Configuration;
import client.main.controller.FXHome;
import client.main.controller.MainCore;
import client.main.model.MainApplicationModel;
import common.interfaces.client.IDataToMain;
import common.model.GameLight;
import common.model.UserHeavy;
import common.model.UserLight;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class DataToMainImpl implements IDataToMain {
    private static final Logger LOGGER = LogManager.getLogger(DataToMainImpl.class);
    private MainCore controller;

    public DataToMainImpl(MainCore m) {
        setController(m);
    }

    public MainCore getController() {
        return controller;
    }

    public void setController(MainCore controller) {
        this.controller = controller;
    }

    @Override
    public void notifyNonAcceptance() {
        getController().getFX().getModel()
                .notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_RESPONSE_GAME_JOIN, "nok");
    }

    @Override
    public void notifyAcceptance() {
        getController().getFX().getModel()
                .notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_RESPONSE_GAME_JOIN, "ok");
    }

    @Override
    public void askHoteValidation(UserLight userProposing) {
        // TODO : do its implementation
    }

    @Override
    public void showNewGame(GameLight gl) {
        getController().getFX().getModel().addGameInList(gl);
        getController().getFX().getModel().notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_UPDATE_DATA,
                null);
    }

    public void showGameInProgress(GameLight gl) {
        getController().getFX().getModel().addGameInProgressInList(gl);
        getController().getFX().getModel().notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_UPDATE_DATA,
                null);
    }

    @Override
    public void sendUsersList(List<UserLight> usrList) {
        getController().getFX().getModel().getUsersList().clear();
        getController().getFX().getModel().getUsersList().addAll(usrList);

        getController().getFX().getModel().notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_UPDATE_DATA,
                null);
    }

    @Override
    public void sendGamesList(List<GameLight> gameList) {
        getController().getFX().getModel().getGamesList().clear();
        getController().getFX().getModel().getGamesList().addAll(gameList);

        getController().getFX().getModel().notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_UPDATE_DATA,
                null);
    }

    @Override
    public void sendGamesInProgress(List<GameLight> gameList) {
        getController().getFX().getModel().getGamesInProgressList().clear();
        getController().getFX().getModel().getGamesInProgressList().addAll(gameList);

        getController().getFX().getModel().notifyControllers("updateData", null);
    }

    @Override
    public void updateUserList(UserLight user) {
        MainApplicationModel model = getController().getFX().getModel();
        boolean shouldUpdate = true;
        // checks if user is already in the list
        for (UserLight alreadyConnectedUsers : model.getUsersList()) {
            shouldUpdate = shouldUpdate && !(alreadyConnectedUsers.getId().equals(user.getId()));
        }

        if (shouldUpdate) {
            model.addUserInList(user);
            model.notifyControllers(Configuration.FX_CONTROLLER_NOTIFICATION_UPDATE_DATA,
                    null);
        }

    }

    @Override
    public void sendUserInfo(UserLight userLight, UserHeavy userHeavy) {
        getController().getFX().getModel().setUserLight(userLight);
        getController().getFX().getModel().setUserHeavy(userHeavy);
    }

    @Override
    public void gameFinished(Stage s) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/home.fxml"));
        BorderPane root = null;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            LOGGER.error("Error when loading root FX", e);
        }
        FXHome homeController = fxmlLoader.getController();
        homeController.setMainController(getController().getFX());
        Scene scene = new Scene(root, Configuration.APPLICATION_WIDTH, Configuration.APPLICATION_HEIGHT);
        homeController.launch(scene, s);

        getController().getFX().getModel().notifyControllers("updateData", null);
    }

    @Override
    public void updateLocalUser(UserLight userLightUpdated) {
        this.getController().getFX().getModel().setUserLight(userLightUpdated);
    }
}
