package client.game;

import client.game.controller.MainGameController;
import client.game.model.*;
import common.interfaces.client.IGameToData;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class GameCore {
    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(GameCore.class);

    /**
     * JavaFX stage
     */
    private Stage fxStage;

    /**
     * MainGameController reference
     */
    private MainGameController controller;

    /**
     * Reference to interface implementation for calls to the outside
     */
    private IGameToData gameToData;

    /**
     * Reference to interface implementation for calls from the outside
     */
    private DataToGameImpl dataToGame;

    /**
     * Reference to the game model
     */
    private GameModel gameModel;

    /**
     * Reference to the replay data
     */
    private Replay replay;

    /**
     * Reference to chat command engine
     */
    private GameChatCommandParser chatCommandParser;

    /**
     * Class constructor
     */
    public GameCore() {
        gameModel = new GameModel();
        controller = new MainGameController(this);
        dataToGame = new DataToGameImpl(this);
        chatCommandParser = new GameChatCommandParser(this);
        replay = new Replay();
    }

    /**
     * Getter for the Game module main controller
     *
     * @return
     */
    public MainGameController getMainGameController() {
        return controller;
    }

    /**
     * Resets the FX stage content in order to put the game module one inside.
     *
     * @param fxmlResource
     * @param stage
     * @throws IOException
     */
    public void initializeFxStage(String fxmlResource, Stage stage) throws IOException {
        fxStage = stage;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(fxmlResource));
        fxmlLoader.setControllerFactory(c -> controller);
        BorderPane root = fxmlLoader.load();
        Scene scene = new Scene(root, 1200, 750);
        scene.getStylesheets().add(getClass().getResource("view/game.css").toExternalForm());

        fxStage.setScene(scene);
        fxStage.centerOnScreen();
        fxStage.setResizable(false);

        // Intercept click on quit button (X)
        fxStage.setOnCloseRequest(we -> {
            if (gameModel.getGameBoard() != null) {
                controller.onForfeit();
            }
        });
    }

    /**
     * Getter for the Replay manager object
     *
     * @return
     */
    public Replay getGameReplay() {
        return replay;
    }

    /**
     * Getter for the game module chat command parser
     *
     * @return
     */
    public GameChatCommandParser getCommandParser() {
        return chatCommandParser;
    }

    /**
     * Getter for gameToData implementation
     *
     * @return
     */
    public final IGameToData getGameToData() {
        return gameToData;
    }

    /**
     * Setter for gameToData interface implementation
     *
     * @param gameToData
     */
    public final void setGameToData(IGameToData gameToData) {
        this.gameToData = gameToData;
    }

    /**
     * Getter for dataToGame implementation
     *
     * @return
     */
    public final DataToGameImpl getDataToGame() {
        return dataToGame;
    }

    /**
     * @return the gameModel
     */
    public GameModel getGameModel() {
        return gameModel;
    }

    /**
     * @param gameModel the gameModel to set
     */
    public void setGameModel(GameModel gameModel) {
        this.gameModel = gameModel;
    }

    /**
     * Is called when the player clicks a SquareShape, then uses the playMove method
     * from GameToData to process the move. This method also asks the controller to
     * freeze the board meanwhile.
     *
     * @param sq
     */
    public void playMove(Square sq) {
        controller.freezeBoard(true);
        getMainGameController().getTimerService().stopTimer();
        gameToData.playMove(sq.getX(), sq.getY(), getGameModel().getGameUUID(), getGameModel().getGameBoard());
    }

    public Stage getStage() {
        return fxStage;
    }
}
