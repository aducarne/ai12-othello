package client.game.model;

import common.model.Board;
import common.model.Chat;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Class managing game replay boards
 */
public class Replay {
    /**
     * List of all the game's boards
     */
    private List<Board> boardList;

    /**
     * List of all the game's boards corresponding time (indexes are coherent with
     * boardList)
     */
    private List<Timestamp> movesTime;

    /**
     * Chat history
     */
    private Chat chat;

    /**
     * Current board index
     */
    private int index;

    /**
     * Constructor
     */
    public Replay() {
        index = 0;
        boardList = new ArrayList<>();
    }

    /**
     * @return the next Board after incrementing the index
     */
    public Board getNextBoard() {
        if (index < boardList.size() - 1) {
            index++;
        }
        return boardList.get(index);
    }

    /**
     * @return the previous Board after decrementing the index
     */
    public Board getPreviousBoard() {
        if (index > 0) {
            index--;
        }

        return boardList.get(index);
    }

    /**
     * Checks if the game is not finished
     *
     * @return true if there is a next board that can be displayed
     */
    public boolean hasNext() {
        return index != boardList.size() - 1;
    }

    /**
     * @return true if there is a previous board that can be displayed
     */
    public boolean hasPrevious() {
        return index != 0;
    }

    /**
     * Sets the full game board list
     *
     * @param list : Game's Board list
     */
    public void setBoardList(List<Board> list) {
        boardList = list;
    }

    /**
     * Gets the chat history (might be null if not set)
     *
     * @return
     */
    public final Chat getChat() {
        return chat;
    }

    /**
     * Sets the chat history
     *
     * @param chat
     */
    public final void setChat(Chat chat) {
        this.chat = chat;
    }

    /**
     * Gets the time associated to the current Board (index)
     *
     * @return
     */
    public Timestamp getCurrentBoardTime() {
        return movesTime.get(index);
    }

    public final List<Timestamp> getMovesTime() {
        return movesTime;
    }

    public final void setMovesTime(List<Timestamp> movesTime) {
        this.movesTime = movesTime;
    }
}
