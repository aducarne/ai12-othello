package client.game.model;

public class Square {

    private int x;
    private int y;
    private int value;

    public Square() {
    }

    public Square(int x, int y, int v) {
        super();
        this.x = x;
        this.y = y;
        this.value = v;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int v) {
        this.value = v;
    }

}
