package client.game.model;

import client.game.GameCore;
import client.game.view.DialogGame;
import common.interfaces.client.IDataToGame;
import common.model.*;
import javafx.application.Platform;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class DataToGameImpl implements IDataToGame {
    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(DataToGameImpl.class);

    private GameCore core;

    /**
     * @param moduleCore
     */
    public DataToGameImpl(GameCore moduleCore) {
        core = moduleCore;
    }

    /**
     * @param b : Board information to display
     */
    @Override
    public void updateInterface(Board b) {
        LOGGER.info("IHM-GAME | Game Board update called");

        String isThisClientTurn = String.valueOf(!isThisClientTurn(b));
        LOGGER.debug("Update interface --- freezed ? {}", isThisClientTurn);

        AtomicInteger userChoice = new AtomicInteger();

        /**
         * The game model update method needs to be performed in a JavaFX Thread. To do
         * so we call the method inside a JavaFX platform that will run at the first
         * opportunity it will get.
         */
        Platform.runLater(() -> {
            /*
             * If it is the first call to the update method, the players pseudo and color
             * would not have been registered yet, so we specify them
             */
            core.getMainGameController().setLocalPlayerData(b);
            core.getMainGameController().updateWhichTurnDisplay(b);
            core.getGameModel().update(b);

            // Freeze / unfreeze given the player's turn
            core.getMainGameController().freezeBoard(!isThisClientTurn(b));

            LOGGER.debug("Client interface updated");

            // End game
            UUID localPlayerUUID = core.getGameToData().getLocalPlayerUUID();
            int localPlayer = localPlayerUUID.equals(b.getPlayer1().getId()) ? 1
                    : localPlayerUUID.equals(b.getPlayer2().getId()) ? 2 : 0;

            if (b.getNextPlayer() == 0 && core.getGameModel().getCurrentGame() != null) {
                core.getMainGameController().gameFinished();

                // Gets the two scores (index 0 is black score and index 1 is white score)
                int[] scores = core.getGameToData().getScore(b);

                userChoice.set(DialogGame.dialogEndGame(b.getPlayer1().getPseudo(), scores[0],
                        b.getPlayer2().getPseudo(), scores[1], localPlayer));
                core.getGameToData().gameFinished(core.getStage());
            } else if (b.getNextPlayer() == -1) { // Forfeit player 1
                core.getMainGameController().gameFinished();
                userChoice.set(DialogGame.dialogForfeit(b.getPlayer1().getPseudo(), true, localPlayer,
                        core.getMainGameController().getTimerService().isPlayerTimeUp()));
                core.getGameToData().gameFinished(core.getStage());
            } else if (b.getNextPlayer() == -2) { // Forfeit player 2
                core.getMainGameController().gameFinished();
                userChoice.set(DialogGame.dialogForfeit(b.getPlayer2().getPseudo(), false, localPlayer,
                        core.getMainGameController().getTimerService().isPlayerTimeUp()));
                core.getGameToData().gameFinished(core.getStage());
            } else if (core.getGameModel().getCurrentGame() != null
                    && core.getGameModel().getCurrentGame().getLimitMove() > 0) { // We start the turn timer
                core.getMainGameController().getTimerService().startTimer();
            }

            if (b.getNextPlayer() == 0 || b.getNextPlayer() == -1 || b.getNextPlayer() == -2) {
                switch (userChoice.get()) {
                    case DialogGame.BUTTON_SAVE:
                        core.getGameToData().askGame(getGameUUID(), core.getGameToData().getLocalPlayerUUID());
                        break;
                    case DialogGame.BUTTON_PROFILE:
                        // core.getGameToData().;
                        break;
                    case DialogGame.BUTTON_MENU:
                        core.getGameToData().gameFinished(core.getStage());
                        break;
                    case DialogGame.BUTTON_TIMEOUT:
                        core.getGameToData().gameFinished(core.getStage());
                        break;
                    default: // Do nothing
                        break;
                }
            }
        });
    }

    /**
     * Given a Board object, checks if the player allowed to play is this one, or
     * the other one.
     *
     * @param b : current Board
     * @return true if this player's client is the one allowed to play
     */
    public boolean isThisClientTurn(Board b) {
        UUID localPlayerUUID = core.getGameToData().getLocalPlayerUUID();

        /**
         * This is the case when we are in replay mode there are no players set
         */
        if (b.getPlayer1().getId() == null || b.getPlayer2().getId() == null) {
            return false;
        }

        if (b.getNextPlayer() == 1) {
            return b.getPlayer1().getId().equals(localPlayerUUID);
        } else if (b.getNextPlayer() == 2) {
            return b.getPlayer2().getId().equals(localPlayerUUID);
        }

        return false; // Next player is 0 if the game is finished or local player is a spectator
    }

    /**
     * @param m : Message to display
     */
    @Override
    public void displayMessage(Message m) {
        Platform.runLater(() -> {
            /**
             * We parse the message to determine if it is a command or a simple message
             *
             * If the message is a command, it needs to be an external command
             */
            if (core.getCommandParser().check(m.getMessage(), false)) {
                core.getCommandParser().processExternalCommand(m);
            } else {
                core.getMainGameController().newSimpleMessageToHistory(m);
            }
        });

    }

    /**
     * @param gl : Game data
     * @param s  : JavaFX Stage (shared with IHM-MAIN module)
     */
    @Override
    public void getGameScreen(GameLight gl, Stage s) {
        LOGGER.info("IHM-GAME | GameScreen initialization called");
        core.getGameModel().resetModel();
        try {
            core.getGameModel().setCurrentGame(gl);
            core.initializeFxStage("view/ihm_game.fxml", s);

            // We wait until it's the player's turn to release HMI
            core.getMainGameController().freezeBoard(true);
        } catch (IOException e) {
            LOGGER.error("Could not instantiate FX scene", e);
        }
    }

    /**
     * Gets the current game UUID (null if no game set)
     *
     * @return the game UUID
     */
    public UUID getGameUUID() {
        return core.getGameModel().getCurrentGame() != null ? core.getGameModel().getCurrentGame().getId() : null;
    }

    /**
     * Chekcs if the chat is authorized for the actual game
     *
     * @return true if chat is authorized (false otherwise or if no game has been
     * declared)
     */
    public boolean isChatAuthorized() {
        if (core.getGameModel().getCurrentGame() == null) {
            if (core.getGameReplay().getChat() != null) { // We are in replay mode
                return true;
            } else {
                LOGGER.error("No game defined");
            }
            return false;
        }
        return core.getGameModel().getCurrentGame().isOkChat();
    }

    /**
     * @param gl : Game data
     * @param s  : JavaFX Stage (shared with IHM-MAIN module)
     */
    @Override
    public void beginGameDisplay(GameLight gl, Stage s) {
        getGameScreen(gl, s);
        core.getMainGameController().changeForfeitButtonToQuit();
    }

    /**
     * @param userToJoin
     */
    @Override
    public void askHostValidation(UserLight userToJoin) {
        Platform.runLater(() -> {
            // Launch acceptation dialog and get user input
            boolean userChoice = DialogGame.dialogAcceptOpponent(core.getStage(), userToJoin.getPseudo());
            // Close waiting dialog if host accept to play
            if (userChoice) {
                DialogGame.closeDialog(core.getMainGameController().getDialogWaitOpponnent());
            }

            try {
                core.getGameToData().acceptanceAcquired(userToJoin.getId(), userChoice);
            } catch (Exception e) {
                LOGGER.error(e);
            }
        });
    }

    @Override
    public void updateSpectatorList(List<UserZero> spectators) {
        core.getMainGameController().setSpectatorList(spectators);
    }

    @Override
    public void replayGame(List<Board> boards, List<Timestamp> movesTime, Chat chat, Stage sc) {
        core.getGameModel().resetModel();

        core.getGameReplay().setBoardList(boards);
        core.getGameReplay().setChat(chat);
        core.getGameReplay().setMovesTime(movesTime);

        // Init stage
        try {
            core.initializeFxStage("view/ihm_game.fxml", sc);
        } catch (IOException e) {
            LOGGER.error("Could not instatiate FX Stage");
        }
        core.getMainGameController().displayReplayGameButtons(true);
        core.getMainGameController().freezeBoard(true);
        core.getMainGameController().freezeChat();
        core.getMainGameController().changeForfeitButtonToQuit();

        // Displays the init board
        core.getMainGameController().onPreviousMove();
    }

    @Override
    public void notifyPlayerRejection() {
        Platform.runLater(() -> core.getGameToData().gameFinished(core.getStage()));
    }
}
