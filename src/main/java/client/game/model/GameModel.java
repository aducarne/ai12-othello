package client.game.model;

import common.model.Board;
import common.model.GameLight;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;

public class GameModel {

    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(GameModel.class);

    /**
     * Holds the current Game
     */
    private GameLight currentGame;

    /**
     * Represents the board
     */
    private Board gameBoard;

    /**
     * BoardChanged flag raised when a change is detected on the board
     */
    private boolean boardChanged;

    /**
     * An ObservableSet containing all the squares to be drawn
     */
    private ObservableList<Square> squareList;

    /**
     * Class constructor
     */
    public GameModel() {
        squareList = FXCollections.observableArrayList();
        resetModel();
    }

    /**
     * @return the currentGame
     */
    public GameLight getCurrentGame() {
        return currentGame;
    }

    /**
     * Setter for the currentGame
     *
     * @param currentGame
     */
    public void setCurrentGame(GameLight currentGame) {
        this.currentGame = currentGame;
    }

    /**
     * Resets all the objects related to a game
     */
    public void resetModel() {
        gameBoard = null;
        boardChanged = false;
        currentGame = null;
    }

    public Board getGameBoard() {
        return gameBoard;
    }

    public void setGameBoard(Board gameBoard) {
        this.gameBoard = gameBoard;
    }

    /**
     * Gets the current game UUID (null if no game set)
     *
     * @return the game UUID
     */
    public UUID getGameUUID() {
        return currentGame != null ? currentGame.getId() : null;
    }

    /**
     * @return the squareSet
     */
    public ObservableList<Square> getSquareList() {
        return squareList;
    }

    /**
     * @param squareList the squareSet to set
     */
    public void setSquareList(ObservableList<Square> squareList) {
        this.squareList = squareList;
    }

    /**
     * Checks if the chat is authorized for the actual game
     *
     * @return true if chat is authorized (false otherwise or if no game has been
     * declared)
     */
    public boolean isChatAuthorized() {
        if (currentGame == null) {
            LOGGER.error("No game defined");
            return false;
        }
        return currentGame.isOkChat();
    }

    /**
     * Transforms the board into an ObservableSet of squares
     */
    public void buildBoardSquares() {
        Board emptyBoard = new Board(); // Create a completely empty board for initialization

        emptyBoard.setState(3, 3, 0);
        emptyBoard.setState(3, 4, 0);
        emptyBoard.setState(4, 3, 0);
        emptyBoard.setState(4, 4, 0);

        gameBoard = emptyBoard;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squareList.add(new Square(i, j, emptyBoard.getStates()[i][j])); // Empty board
            }
        }
    }

    /**
     * Deletes all the board tile squares and then re-creates them.
     * <p>
     * Use this method to update colors of the board
     */
    public void redraw() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squareList.remove(i * 8 + j);
                squareList.add(new Square(i, j, gameBoard.getStates()[i][j]));
            }
        }
    }

    /**
     * Updates the current displayed board by comparing it to the given newBoard
     *
     * @param newBoard
     */
    public void update(Board newBoard) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (gameBoard.getStates()[i][j] != newBoard.getStates()[i][j]) {
                    squareList.remove(i * 8 + j);
                    squareList.add(i * 8 + j, new Square(i, j, newBoard.getStates()[i][j]));
                    boardChanged = true;
                }
            }
        }
        gameBoard = newBoard;
    }

    /**
     * Called when a move is performed to reset the boardChanged flag
     */
    public void resetBoardChangedFlag() {
        boardChanged = false;
    }

    /**
     * Checks if the board changed flag is raised
     *
     * @return true if the board has been changed since the last played move
     */
    public final boolean hasBoardChanged() {
        return boardChanged;
    }

}
