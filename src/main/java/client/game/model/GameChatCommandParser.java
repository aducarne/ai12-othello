package client.game.model;

import client.game.GameCore;
import client.game.view.BoardConstants;
import common.model.Message;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Main parser and processor of chat commands
 */
public class GameChatCommandParser {
    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(GameChatCommandParser.class);
    /**
     * Color used for commands result display
     */
    private static final Color COMMANDS_COLOR = Color.CRIMSON;
    /**
     * Font used for commands result display
     */
    private static final Font COMMANDS_FONT = Font.font("Verdana", FontPosture.ITALIC, 12);
    /**
     * Game module core
     */
    private GameCore core;
    /**
     * Map of commands names and scope
     * <p>
     * First param is the command name (starting with '--'). The associated method
     * wears the same name, without '--'. Second param is the command scope: True is
     * only the author, false is everyone.
     */
    private Map<String, Boolean> commands;

    /**
     * Constructor
     *
     * @param core: GameCore instance reference
     */
    public GameChatCommandParser(GameCore core) {
        this.core = core;

        bindCommands();
    }

    /**
     * Checks if the given string is a known command.
     *
     * @param supposedCommand: potential command
     * @param internal:        true if the command is supposed to be executed only
     *                         for the author
     * @return true if the String argument can be interpreted as a known command
     */
    public boolean check(String supposedCommand, boolean internal) {
        return commands.keySet().contains(supposedCommand)
                && (internal ? commands.get(supposedCommand) : !commands.get(supposedCommand));
    }

    /**
     * Processes a command, invoking it's related method. Only for external
     * commands, since those are contained in a message object
     *
     * @param message containing the command to process
     */
    public void processExternalCommand(Message message) {
        try {
            Method m = getClass().getMethod(message.getMessage().substring(2), Message.class);
            Text res = (Text) m.invoke(this, message);

            core.getMainGameController().newCommandMessageToHistory(res);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            LOGGER.error("Could not invoke command " + message.getMessage(), e);
        }
    }

    /**
     * Processes a command, invoking it's related method. Only for internal
     * commands.
     *
     * @param message containing the command to process
     */
    public void processInternalCommand(String command) {
        try {
            Method m = getClass().getMethod(command.substring(2));
            Text res = (Text) m.invoke(this);

            core.getMainGameController().newCommandMessageToHistory(res);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            LOGGER.error("Could not invoke command " + command, e);
        }
    }

    /**
     * Initiates the commands binding map
     */
    private void bindCommands() {
        commands = new HashMap<>();

        commands.put("--boardColor", true);
        commands.put("--spam", false);
        commands.put("--help", true);
    }

    /**
     * Changes the board's background color
     *
     * @return A JavaFX Text object containing the message to display
     */
    public Text boardColor() {
        BoardConstants.randomizeBoardFillColor();
        core.getGameModel().redraw();

        Text result = new Text("[ La couleur du plateau a changé ! ]");
        result.setFill(BoardConstants.getBoardFillColor());
        result.setFont(COMMANDS_FONT);
        return result;
    }

    /**
     * Creates a loooooooong spam message for anyone but the author
     *
     * @return
     */
    public Text spam(Message m) {
        String text;
        if (isThisClientAuthor(m)) {
            text = "[ Tu as bien spamé tout le monde. Bravo. ]";
        } else {
            String spam = "Spam.";
            StringBuilder sb = new StringBuilder("[ Tu te fais spamer... ]");
            for (int i = 0; i < 1000; i++) {
                sb.append("\n").append(spam);
            }
            sb.append("\n").append("[ ... ]");

            text = sb.toString();
        }

        Text result = new Text(text);
        result.setFill(COMMANDS_COLOR);
        result.setFont(COMMANDS_FONT);
        return result;
    }

    /**
     * Displays the list of all available commands
     *
     * @param m
     * @return
     */
    public Text help() {
        StringBuilder sb = new StringBuilder("[ Liste des commandes ]");
        commands.keySet().forEach(c -> sb.append("\n> ").append(c));
        Text result = new Text(sb.toString());
        result.setFill(COMMANDS_COLOR);
        result.setFont(COMMANDS_FONT);
        return result;
    }

    /**
     * Checks if this client instance is the command author
     *
     * @param m : Command Message
     * @return true if the client is the author
     */
    private boolean isThisClientAuthor(Message m) {
        return m.getAuthor().getId().equals(core.getGameToData().getLocalPlayerUUID());
    }
}
