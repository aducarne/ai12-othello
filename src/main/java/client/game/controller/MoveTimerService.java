package client.game.controller;

import client.game.GameCore;
import javafx.application.Platform;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Object managing the timer for player moves
 */
public class MoveTimerService extends Thread {

    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(MoveTimerService.class);

    /**
     * Current chrono value
     */
    private int chrono = -1;

    /**
     * Flag for pausing the Thread
     */
    private boolean pauseTimer = false;

    /**
     * Game module core
     */
    private GameCore gameCore;

    /**
     * Constructor
     *
     * @param moduleCore : Game module core reference
     */
    public MoveTimerService(GameCore moduleCore) {
        gameCore = moduleCore;

        start();
    }

    @Override
    public void run() {
        try {
            while (true) {
                while (chrono > 0 && !pauseTimer) {
                    Platform.runLater(() -> {
                        gameCore.getMainGameController().setTimerText("Temps restant : " + chrono);
                        gameCore.getMainGameController().disablesTimerLabel(
                                !gameCore.getDataToGame().isThisClientTurn(gameCore.getGameModel().getGameBoard()));
                    });

                    chrono--;

                    Thread.sleep(1000);
                }
                if (chrono == 0) { // Player has lost
                    Platform.runLater(() -> {
                        gameCore.getMainGameController().setTimerText("Temps écoulé !");
                    });

                    playerTimeIsUp();
                    stopTimer();
                }

                Thread.sleep(500);

            }
        } catch (InterruptedException e) {
            LOGGER.error("Chrono Thread interrupted", e);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Starts and continues the timer
     */
    public void startTimer() {
        if (isAlive()) {
            // Reset timer if it is a new turn, otherwise continue.
            if (gameCore.getGameModel().hasBoardChanged()) {
                chrono = gameCore.getGameModel().getCurrentGame().getLimitMove();
            }
            pauseTimer = false;
        }
    }

    /**
     * Stops the timer Thread
     */
    public void stopTimer() {
        pauseTimer = true;
        gameCore.getGameModel().resetBoardChangedFlag();
    }

    /**
     * Called when a player's timer is down to 0: he/she has lost the game.
     */
    private void playerTimeIsUp() {
        if (gameCore.getDataToGame().isThisClientTurn(gameCore.getGameModel().getGameBoard())) {
            gameCore.getMainGameController().onForfeit(); // Delegates to forfeit method
        }
    }

    /**
     * This method shall be called when the forfeit dialog is constructed, to check
     * if the end game cause is a real forfeit or is because a player time is up.
     *
     * @return true if the chrono is down to 0.
     */
    public boolean isPlayerTimeUp() {
        return chrono == 0;
    }
}
