package client.game.controller;

import client.game.GameCore;
import client.game.model.Square;
import client.game.view.DialogGame;
import client.game.view.SquareShape;
import common.model.Board;
import common.model.Chat;
import common.model.Message;
import common.model.UserZero;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

/**
 * IHM-Game main controller class.
 * <p>
 * Links the FXML elements to the correct methods in the specialized controllers
 */
public class MainGameController {
    /**
     * Text replacement for forfeit button when client is a spectator
     */
    public static final String SPECTATOR_LEAVE_BTN_TEXT = "Quitter";
    /**
     * Class logger
     */
    private static final Logger LOGGER = LogManager.getLogger(MainGameController.class);
    @FXML
    Button openButton;
    @FXML
    TextArea areaSendMessage;
    @FXML
    Button btnSendMessage;
    @FXML
    ListView<Text> areaChatHistory;
    @FXML
    ListView<Text> listParticipants;
    @FXML
    TabPane sideRightTabPane;
    @FXML
    Button btnForfeit;
    @FXML
    GridPane gameBoard;
    @FXML
    Label labelP1;
    @FXML
    Label labelP2;
    @FXML
    Label labelPlaying;
    @FXML
    Label labelTimer;
    /**
     * Board side labels
     */
    @FXML
    GridPane gridTopLabels;
    @FXML
    GridPane gridLeftLabels;
    @FXML
    GridPane gridRightLabels;
    @FXML
    GridPane gridBottomLabels;
    @FXML
    Button btnNextMove;
    @FXML
    Button btnPrevMove;
    /**
     * Thread managing the chrono
     */
    private MoveTimerService chronoThreadService;
    /**
     * Dialog waiting for opponnent
     */
    private Dialog dialogWaitOpponnent;
    /**
     * Application game core
     */
    private GameCore gameCore;

    /**
     * Controller constructor, instantiates the secondary controllers
     *
     * @param core : Sets the module Core reference
     */
    public MainGameController(GameCore core) {
        gameCore = core;
        chronoThreadService = new MoveTimerService(gameCore);
    }

    /**
     * Method called after the display of the game Java FX scene
     */
    public void initialize() {
        // Remove access to the chat panel if the game configuration says so
        if (!gameCore.getDataToGame().isChatAuthorized()) {
            sideRightTabPane.getTabs().get(1).setDisable(true);
            sideRightTabPane.getTabs().get(1)
                    .setTooltip(new Tooltip("La discussion Chat est désactivée pour cette partie."));
        }
        initModelBiding();
        gameCore.getGameModel().buildBoardSquares();

        /*
         * This message is displayed only on the creator client Current game is null if
         * we are not in replay
         */
        if (gameCore.getGameModel().getCurrentGame() != null && gameCore.getGameModel().getCurrentGame()
                .getGameCreator().getId().equals(gameCore.getGameToData().getLocalPlayerUUID())) {
            Platform.runLater(() -> {
                dialogWaitOpponnent = DialogGame.dialogWaitingOpponent(gameCore.getStage());
                dialogWaitOpponnent.show();
            });
        }

        // Hide next and previous move buttons by default.
        displayReplayGameButtons(false);
    }

    /**
     * Hides or shows the two replay buttons (next or previous move).
     *
     * @param display: true will display the buttons (false hides)
     */
    public void displayReplayGameButtons(boolean display) {
        btnNextMove.setVisible(display);
        btnPrevMove.setVisible(display);
    }

    /**
     * Sets the local player information in the top bar. Local player will always be
     * displayed as player one, and it's oponent as player two
     *
     * @param b
     */
    public void setLocalPlayerData(Board b) {
        // Players data has not been registered yet
        if (labelP1.getText().isEmpty()) {
            labelP1.setText(b.getPlayer1().getPseudo().replace("\"", ""));
            labelP2.setText(b.getPlayer2().getPseudo().replace("\"", ""));

            /*
             * Player 1 is the black player, and 2 is the white
             */
            labelP1.setBackground(new Background(new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY)));
            labelP1.setTextFill(Color.WHITE);
            labelP2.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }

    /**
     * Updates the label indicating whose turn it is
     *
     * @param b: Game board containing information about whose turn is it
     */
    public void updateWhichTurnDisplay(Board b) {
        UUID localPlayerUUID = gameCore.getGameToData().getLocalPlayerUUID();

        /**
         * This is the case when we are in replay mode there are no players ID set
         */
        if (b.getPlayer1().getId() == null || b.getPlayer2().getId() == null) {
            labelPlaying.setText("");
            labelTimer.setText("");
            return;
        }

        if (b.getNextPlayer() == 1) {
            if (b.getPlayer1().getId().equals(localPlayerUUID)) {
                labelPlaying.setText("C'est votre tour !");
            } else {
                labelPlaying.setText("Tour de " + b.getPlayer1().getPseudo());
            }
        } else if (b.getNextPlayer() == 2) {
            if (b.getPlayer2().getId().equals(localPlayerUUID)) {
                labelPlaying.setText("C'est votre tour !");
            } else {
                labelPlaying.setText("Tour de " + b.getPlayer2().getPseudo());
            }
        } else if (b.getNextPlayer() == 0) {
            labelPlaying.setText("Partie terminée !");
        } else if (b.getNextPlayer() == -1) { // Player 1 has forfeit
            labelPlaying.setText(
                    "Partie terminée ! " + (chronoThreadService.isPlayerTimeUp() ? "Temps écoulé pour " : "Abandon de ")
                            + b.getPlayer1().getPseudo());
        } else if (b.getNextPlayer() == -2) { // Player 2 has forfeit
            labelPlaying.setText(
                    "Partie terminée ! " + (chronoThreadService.isPlayerTimeUp() ? "Temps écoulé pour " : "Abandon de ")
                            + b.getPlayer2().getPseudo());
        }
    }

    /**
     * Changes the text of forfeit button to quit (in case this client is a
     * spectator)
     */
    public void changeForfeitButtonToQuit() {
        btnForfeit.setText(SPECTATOR_LEAVE_BTN_TEXT);
    }

    /**
     * Called upon a click on the send button
     */
    public void onSendMessage() {

        if (!areaSendMessage.getText().isEmpty()) {
            sendMessage();
        }
    }

    /**
     * Displays a message to the chat history area after its receiving.
     * <p>
     * Indicates the author, the time and the message
     */
    public void newSimpleMessageToHistory(Message m) {
        LOGGER.debug("Message received -> {}", m.getMessage());
        String time = new SimpleDateFormat("HH:mm:ss").format(m.getHourMessage());

        Text tPlayer = new Text("---- " + m.getAuthor().getPseudo() + " à " + time + " ----");
        Text tMessage = new Text("> " + m.getMessage());

        if (m.getAuthor().getId() != null) {
            /**
             * In replay mode the author id is unknown
             */
            tPlayer.setFill(
                    m.getAuthor().getId().equals(gameCore.getGameToData().getLocalPlayerUUID()) ? Color.BLUEVIOLET
                            : Color.DARKSLATEBLUE);
        }

        areaChatHistory.getItems().add(tPlayer);
        areaChatHistory.getItems().add(tMessage);
    }

    /**
     * Displays all the messages from the given chat object until the specified time
     *
     * @param t : limit time
     * @param c : chat object
     */
    public void displayMessagesUntil(Timestamp t, Chat c) {
        areaChatHistory.getItems().clear();

        c.getChatList().forEach(m -> {
            if (m.getHourMessage().getTime() <= t.getTime()) {
                newSimpleMessageToHistory(m);
            }
        });
    }

    /**
     * Displays a command result to the chat history
     *
     * @param toDisplay: result of the command
     */
    public void newCommandMessageToHistory(Text toDisplay) {
        areaChatHistory.getItems().add(toDisplay);
    }

    /**
     * Updates the list view of spectators
     *
     * @param spectators : list to display
     */
    public void setSpectatorList(List<UserZero> spectators) {
        listParticipants.getItems().clear();

        spectators.forEach(s -> {
            listParticipants.getItems().add(new Text(s.getPseudo()));
        });
    }

    /**
     * Getter Dialog waiting for opponnent
     */
    public Dialog getDialogWaitOpponnent() {
        return dialogWaitOpponnent;
    }

    /**
     * Called to freeze forfeit button and board
     */
    public void gameFinished() {
        freezeBoard(true);
        btnForfeit.setDisable(true);
        chronoThreadService.stopTimer();

        // Overrides modification from the game core initialization
        gameCore.getStage().setOnCloseRequest(e -> {
        });
    }

    /**
     * Called upon a click on the forfeit button
     */
    public void onForfeit() {
        LOGGER.debug("Forfeit");

        /*
         * If the local client is a spectator, we don't send a move, we just leave the
         * game
         */
        if (btnForfeit.getText().equals(SPECTATOR_LEAVE_BTN_TEXT)) {
            gameCore.getGameToData().gameFinished(gameCore.getStage());
            gameCore.getGameToData().removeSpectator(gameCore.getGameModel().getGameUUID());
        } else {
            freezeBoard(true);
            chronoThreadService.stopTimer();

            /**
             * To inform on a forfeit we simulate a negative move
             */
            gameCore.getGameToData().playMove(-1, -1, gameCore.getGameModel().getGameUUID(),
                    gameCore.getGameModel().getGameBoard());
        }
    }

    /**
     * Service getter for the Thread managing the move timer
     *
     * @return
     */
    public MoveTimerService getTimerService() {
        return chronoThreadService;
    }

    /**
     * Changes the timer label text
     *
     * @param text : text to display
     */
    public void setTimerText(String text) {
        labelTimer.setText(text);
    }

    /**
     * Disables or enables the timer label
     *
     * @param doDisable : true to disable
     */
    public void disablesTimerLabel(boolean doDisable) {
        labelTimer.setDisable(doDisable);
    }

    /**
     * ActionKeyPressed event managed by the areaSendMessage TextArea.
     * <p>
     * It is used to listen to Enter key, where we want to send a message when Enter
     * is pressed
     *
     * @param event: KeyEvent created upon the key pressed action
     */
    @FXML
    public void onActionKeyPressed(KeyEvent event) {
        /*
         * If there is no message and we press enter, we do not want to send an empty
         * message. In that case we consume the event, and do nothing afterwards.
         */
        if (areaSendMessage.getText().trim().isEmpty() && event.getCode() == KeyCode.ENTER) {
            areaSendMessage.clear();
            event.consume();
        }
        /*
         * If the message is not empty, we send it if enter is pressed. We check if
         * shift is down, if it is the case, the message will not be send, but a new
         * line is added to the text area.
         */
        else if (event.getCode() == KeyCode.ENTER) {
            event.consume();
            if (event.isShiftDown()) { // New line
                areaSendMessage.appendText(System.getProperty("line.separator"));
            } else {
                sendMessage(); // New message
            }
        }
    }

    /**
     * Calls for the main SendMessage in the side controller and clears the message
     * in the TextArea.
     * <p>
     * We check here if the message is parseable as a command
     */
    private void sendMessage() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        String messageText = areaSendMessage.getText().trim();

        /**
         * Check if the message is an internal command
         */
        if (gameCore.getCommandParser().check(messageText, true)) {
            gameCore.getCommandParser().processInternalCommand(messageText);
        } else {
            /**
             * If the message is not a command, we try to evaluate it as an operation
             */
            try {
                messageText += " = " + engine.eval(messageText);
            } catch (ScriptException e) {
                /**
                 * In that case the message is a regular one, nothing is done
                 */
            }

            // Send the message
            LOGGER.debug("Message sent -> {}", messageText);
            try {
                gameCore.getGameToData().newMessage(messageText, gameCore.getDataToGame().getGameUUID());
            } catch (Exception e) {
                LOGGER.error("No game set yet");
            }

        }

        areaSendMessage.clear();
    }

    /**
     * Initiates the model binding. First it creates the response function, then it
     * ads it as a listener.
     */
    private void initModelBiding() {
        ListChangeListener<Square> f;
        f = change -> {
            while (change.next()) {
                if (change.wasRemoved()) {
                    gameBoard.getChildren().remove(change.getRemoved().get(0));
                }
                if (change.wasAdded()) {
                    SquareShape sq = new SquareShape(change.getAddedSubList().get(0), gameCore);
                    gameBoard.add(sq, change.getAddedSubList().get(0).getX(), change.getAddedSubList().get(0).getY());
                    sq.getRect().widthProperty().bind(gameBoard.widthProperty().divide(8));
                    sq.getRect().heightProperty().bind(gameBoard.widthProperty().divide(8));
                }
            }
        };

        gameCore.getGameModel().getSquareList().addListener(f);
    }

    /**
     * Prevents the user from typing chat messages and sending them
     */
    public void freezeChat() {
        areaSendMessage.setDisable(true);
        btnSendMessage.setDisable(true);
    }

    /**
     * Disables or enables the gameBoard. Called after the player made a move and
     * after the board update. <br>
     * <b>Spectators' boards are and will stay freezed.</b>
     *
     * @param doFreeze
     */
    public void freezeBoard(boolean doFreeze) {
        gameBoard.setDisable(doFreeze);
    }

    /**
     * Called upon a click on the "Coup suivant" button (while reviewing a game)
     */
    public void onNextMove() {
        gameCore.getDataToGame().updateInterface(gameCore.getGameReplay().getNextBoard());
        if (!gameCore.getGameReplay().hasNext()) {
            btnNextMove.setDisable(true);
        }
        btnPrevMove.setDisable(false);

        displayMessagesUntil(gameCore.getGameReplay().getCurrentBoardTime(), gameCore.getGameReplay().getChat());
    }

    /**
     * Called upon a click on the "Coup précédent" button (while reviewing a game)
     */
    public void onPreviousMove() {
        gameCore.getDataToGame().updateInterface(gameCore.getGameReplay().getPreviousBoard());
        if (!gameCore.getGameReplay().hasPrevious()) {
            btnPrevMove.setDisable(true);
        }
        btnNextMove.setDisable(false);

        displayMessagesUntil(gameCore.getGameReplay().getCurrentBoardTime(), gameCore.getGameReplay().getChat());
    }
}
