package client.game.view;

import javafx.scene.paint.Color;

import java.util.Random;

/**
 * Utility class implementing constants to use for the board display
 */
public class BoardConstants {

    /**
     * Board tiles background color
     */
    private static Color boardFillColor = Color.DARKGREEN;
    private static Color[] availableBoardColors = {Color.DARKGREEN, Color.DARKGOLDENROD, Color.DARKSLATEBLUE,
            Color.DARKCYAN, Color.DARKGRAY, Color.DARKSLATEGREY, Color.CORNFLOWERBLUE};

    private BoardConstants() {
    }

    /**
     * Gets the background color to use for board's tiles
     *
     * @return Color
     */
    public static final Color getBoardFillColor() {
        return boardFillColor;
    }

    /**
     * Sets the background color to use for board's tiles
     *
     * @param boardFillColor: new Color to use
     */
    public static final void setBoardFillColor(Color boardFillColor) {
        BoardConstants.boardFillColor = boardFillColor;
    }

    public static final void randomizeBoardFillColor() {
        setBoardFillColor(availableBoardColors[new Random().nextInt(availableBoardColors.length)]);
    }

}
