package client.game.view;

import client.game.GameCore;
import client.game.model.Square;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class SquareShape extends StackPane {
    private Square square;
    private Rectangle rect;
    private Circle color;
    private GameCore gameCore;

    public SquareShape(Square sq, GameCore gc) {
        square = sq;
        gameCore = gc;
        draw();
        new SquareController().bind();
    }

    /**
     * @return the square
     */
    public Square getSquare() {
        return square;
    }

    /**
     * @param square the square to set
     */
    public void setSquare(Square square) {
        this.square = square;
    }

    /**
     * @return the rect
     */
    public Rectangle getRect() {
        return rect;
    }

    /**
     * @param rect the rect to set
     */
    public void setRect(Rectangle rect) {
        this.rect = rect;
    }

    /**
     * @return the color
     */
    public Circle getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(Circle color) {
        this.color = color;
    }

    /**
     * Draws the rectangle and the circle of the SquareShape
     */
    private void draw() {
        rect = new Rectangle();
        rect.setFill(BoardConstants.getBoardFillColor());

//		Image img = new Image(GameCore.class.getResource("view/test.jpg").toExternalForm());
//		rect.setFill(new ImagePattern(img));

        rect.setStroke(Color.WHITESMOKE);
        rect.setStrokeWidth(1);

        color = new Circle(25, 25, 30);
        Color c = Color.TRANSPARENT;

        if (square.getValue() == 1) {
            c = Color.BLACK;
        } else if (square.getValue() == 2) {
            c = Color.WHITE;
        }

        color.setFill(c);

        getChildren().addAll(rect, color);
    }

    /**
     * Inner class handling click actions on the SquareShape
     */
    private class SquareController {
        private void bind() {
            setOnMouseClicked(evt -> gameCore.playMove(square));
        }
    }
}
