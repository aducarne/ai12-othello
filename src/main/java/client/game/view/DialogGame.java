package client.game.view;

import client.data.Configuration;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.util.Duration;

import java.util.Optional;

public class DialogGame {

    /**
     * Constant variable for save button
     */
    public static final int BUTTON_SAVE = 1;
    /**
     * Constant variable for profile button
     */
    public static final int BUTTON_PROFILE = 2;
    /**
     * Constant variable for dialog timeout
     */
    public static final int BUTTON_TIMEOUT = 3;
    /**
     * Constant variable for dialog back to menu
     */
    public static final int BUTTON_MENU = 4;

    private DialogGame() {
    }

    /**
     * @param window
     * @return Dialog window
     */
    public static Dialog dialogWaitingOpponent(Window window) {
        // Dialog creation & configuration
        Dialog dialog = new Dialog();
        dialog.initOwner(window);
        dialog.setTitle("Partie créée");
        dialog.setContentText("Merci de patienter qu'un adversaire rejoigne votre partie.");

        // Making the dialog not possible to close by user
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
        Node button = dialog.getDialogPane().lookupButton(ButtonType.CANCEL);
        button.managedProperty().bind(button.visibleProperty());
        button.setVisible(false);
        dialog.initStyle(StageStyle.UNDECORATED);

        return dialog;
    }

    /**
     * @param window
     * @param opponent
     * @return Boolean : User choice
     */
    public static boolean dialogAcceptOpponent(Window window, String opponent) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        alert.initOwner(window);
        alert.setTitle("Notification");
        alert.setHeaderText("Un adversaire demande à rejoindre votre partie.");
        alert.setContentText("Acceptez-vous de jouer contre : " + opponent);

        // Display the dialog and wait for user choice
        Optional<ButtonType> result = alert.showAndWait();
        return result.orElse(ButtonType.NO) == ButtonType.OK;
    }

    /**
     * Displays a Dialog with the given information
     *
     * @param playerBlackPseudo : black player's pseudo
     * @param blackScore        : black player's final score
     * @param playerWhitePseudo : white player's pseudo
     * @param whiteScore        : white player's final score
     * @param localPlayer       : 1 is Black, 2 is White, 0 is Spectator
     * @return int : user choice
     */
    public static int dialogEndGame(String playerBlackPseudo, int blackScore, String playerWhitePseudo, int whiteScore,
                                    int localPlayer) {
        // Alert creation
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        // Alert setting
        alert.setTitle("Partie terminée !");
        switch (localPlayer) {
            case 1:
                alert.setHeaderText((blackScore > whiteScore ? Configuration.END_GAME_WIN : Configuration.END_GAME_LOSE)
                        + blackScore + " - " + whiteScore);
                break;
            case 2:
                alert.setHeaderText((whiteScore > blackScore ? Configuration.END_GAME_WIN : Configuration.END_GAME_LOSE)
                        + whiteScore + " - " + blackScore);
                break;
            default:
                alert.setHeaderText(playerBlackPseudo + " " + blackScore + " - " + playerWhitePseudo + " " + whiteScore);
                break;
        }
        // Call default end game dialog
        return dialogEnd(alert);
    }

    /**
     * @param playerForfeitPseudo : Pseudo of the player that forfeited
     * @param playerBlack         : true if the forfeit player is black, false for
     *                            white
     * @param localPlayer         : 1 is Black, 2 is White, 0 is Spectator
     * @param forfeitTimeUp       : true if the forfeit is due to a time up
     * @return int : User choice
     */
    public static int dialogForfeit(String playerForfeitPseudo, boolean playerBlack, int localPlayer,
                                    boolean forfeitTimeUp) {
        // Alert creation
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);

        // Alert setting
        alert.setTitle("Partie terminée !");
        switch (localPlayer) {
            case 1:
                if (forfeitTimeUp) {
                    alert.setHeaderText(
                            !playerBlack ? Configuration.END_GAME_WIN + playerForfeitPseudo + Configuration.END_GAME_TIMEOUT
                                    : "Vous avez perdu : vous avez écoulé tout votre temps.");
                } else {
                    alert.setHeaderText(!playerBlack ? Configuration.END_GAME_WIN + playerForfeitPseudo + " a abandonné."
                            : "Vous avez perdu : vous avez abandonné.");
                }
                break;
            case 2:
                if (forfeitTimeUp) {
                    alert.setHeaderText(
                            playerBlack ? Configuration.END_GAME_WIN + playerForfeitPseudo + Configuration.END_GAME_TIMEOUT
                                    : "Vous avez perdu : vous avez écoulé tout votre temps.");
                } else {
                    alert.setHeaderText(playerBlack ? Configuration.END_GAME_WIN + playerForfeitPseudo + " a abandonné."
                            : "Vous avez perdu : vous avez abandonné.");
                }
                break;
            default:
                if (forfeitTimeUp) {
                    alert.setHeaderText(playerForfeitPseudo + Configuration.END_GAME_TIMEOUT);
                } else {
                    alert.setHeaderText(playerForfeitPseudo + " a abandonné la partie.");
                }
                break;
        }

        // Call default end game dialog
        return dialogEnd(alert);
    }

    /**
     * @param alert
     * @return
     */
    public static int dialogEnd(Alert alert) {
        // Creation of buttons
        ButtonType buttonSave = new ButtonType("Sauvegarder le replay");
        ButtonType buttonProfile = new ButtonType("Mon profil");
        ButtonType buttonMenu = new ButtonType("Menu principal");

        // Assignation of buttons to dialog
        // Don't use buttonProfile because not bound to Main
        alert.getButtonTypes().setAll(buttonSave, buttonMenu);

        // Creation of a timer
        PauseTransition delay = new PauseTransition(Duration.seconds(10));

        // Event on timer end
        delay.setOnFinished(event -> {
            alert.setResult(ButtonType.CANCEL);
            alert.hide();
        });
        delay.play(); // Start the timer

        // Display the alert
        Optional<ButtonType> result = alert.showAndWait();

        // Retrieve user choice (timeout included)
        if (result.isPresent()) {
            if (result.get() == buttonProfile) {
                return BUTTON_PROFILE;
            } else if (result.get() == buttonSave) {
                return BUTTON_SAVE;
            } else if (result.get() == buttonMenu) {
                return BUTTON_MENU;
            }
        }
        return BUTTON_TIMEOUT;
    }

    public static void closeDialog(Dialog dialog) {
        Platform.runLater(dialog::close);
    }
}
