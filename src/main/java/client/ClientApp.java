package client;

import client.data.Configuration;
import client.data.DataCoreClient;
import client.game.GameCore;
import client.main.controller.MainCore;
import client.network.ComCoreClient;
import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Client application
 */
@SpringBootApplication
public class ClientApp extends Application {
    private static final Logger LOGGER = LogManager.getLogger(ClientApp.class);

    private static MainCore mainCore;

    /**
     * Run the client app (JavaFX)
     *
     * @param args additional args
     */
    public static void main(String[] args) {
        LOGGER.debug("ClientApp");
        launch(args);
    }

    private static void initCores() {
        // IHM Main Core
        mainCore = new MainCore();

        DataCoreClient dataCoreClient = new DataCoreClient();
        ComCoreClient comCoreClient = new ComCoreClient();

        // IHM Game Core
        GameCore gameCore = new GameCore();

        // Set implementations
        dataCoreClient.setDataToCom(comCoreClient.getDataToComClientImpl());
        dataCoreClient.setDataToMain(mainCore.getDataToMainImpl());
        dataCoreClient.setDataToGame(gameCore.getDataToGame());
        comCoreClient.setComToDataClient(dataCoreClient.getClientComToDataImpl());
        mainCore.setMainToData(dataCoreClient.getMainToDataImpl());
        gameCore.setGameToData(dataCoreClient.getGameToDataImpl());
    }

    @Override
    public void init() {
        initCores();
    }

    @Override
    public void start(Stage stage) throws Exception {
        LOGGER.info(Configuration.APPLICATION_NAME + " is starting...");
        mainCore.getFX().launch("../fxml/login.fxml", stage, mainCore);
    }
}
