package client.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import common.exceptions.OthelloException;
import common.interfaces.client.IMainToData;
import common.model.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainToDataImpl implements IMainToData {
    private static final Logger LOGGER = LogManager.getLogger(MainToDataImpl.class);

    //Initializing 2 lists in order to recreate the game logic (for game saving)
    private static List<Integer> listeDirectionX = new ArrayList<>(Arrays.asList(-1, 0, 1));
    private static List<Integer> listeDirectionY = new ArrayList<>(Arrays.asList(-1, 0, 1));

    private DataCoreClient dataCoreClient;

    MainToDataImpl(DataCoreClient dataCoreClient) {
        this.dataCoreClient = dataCoreClient;
    }

    /**
     * Creating the file corresponding to a user.
     *
     * @param userLight: UserLight corresponding to the user to create
     * @param userHeavy: UserHeavy correspondind to the user to create
     */
    private static void createUser(UserLight userLight, UserHeavy userHeavy) throws OthelloException {
        //Setting the path of the file to create
        String userFilePath = String.format(Configuration.JSON_FORMAT_USERS, Configuration.SAVE_DIR_USERS,
                File.separator, userLight.getPseudo());
        File userFile = new File(userFilePath);

        //If the file already exists, it means that a player with the same pseudo already exists
        if (userFile.exists()) {
            throw new OthelloException("This user already exists on this computer");
        }

        //Else, creating the JsonObject corresponding to the user to create
        JsonObject userJson = new JsonObject();
        userJson.addProperty("id", userLight.getId().toString());
        userJson.addProperty(Configuration.PROPERTY_NAME_USER_PSEUDO, userLight.getPseudo());
        userJson.addProperty("lastName", userLight.getLastName());
        userJson.addProperty("firstName", userLight.getFirstName());
        if (userLight.getDateOfBirth() == null) {
            userJson.addProperty(Configuration.PROPERTY_NAME_USER_DATEOFBIRTH, (String) null);
        } else {
            userJson.addProperty(Configuration.PROPERTY_NAME_USER_DATEOFBIRTH,
                    new SimpleDateFormat("dd-MM-yyyy").format(userLight.getDateOfBirth()));
        }
        userJson.addProperty("avatarId", userLight.getAvatarId());
        userJson.addProperty("playedGames", userLight.getPlayedGames());
        userJson.addProperty("wonGames", userLight.getWonGames());
        userJson.addProperty("password", userHeavy.getPassword());
        userJson.addProperty("serverAddress", userHeavy.getServerAddress());
        userJson.addProperty("serverPort", userHeavy.getServerPort());

        //Creating folder that will contains the file if it does not exist
        userFile.getParentFile().mkdirs();
        try (FileWriter fw = new FileWriter(userFilePath)) {
            fw.write(userJson.toString());
        } catch (IOException e) {
            LOGGER.error("Error while opening user file", e);
        }
    }

    @Override
    public void selectGame(GameLight game, Stage s) {
        dataCoreClient.getDataToGame().getGameScreen(game, s);
        dataCoreClient.getDataToCom().askJoinGame(game.getId(), dataCoreClient.getLocalUserLight().getId());
    }

    @Override
    public void newGame(UserLight ul, boolean spectatorsOk, boolean chatOk, boolean creatorPlayWhite, int limitMove,
                        Stage stageFx) {
        GameLight gameCreated = new GameLight(UUID.randomUUID(), dataCoreClient.getLocalUserLight(), null,
                GameStatus.PENDING, spectatorsOk, chatOk, creatorPlayWhite, limitMove);
        dataCoreClient.getDataToCom().addNewGameAvailable(gameCreated);
        dataCoreClient.getDataToGame().getGameScreen(gameCreated, stageFx);
    }

    @Override
    public void requestToDataToSpecialGame(GameLight game, UserLight user, Stage stageFx) {
        dataCoreClient.getDataToGame().beginGameDisplay(game, stageFx);
        dataCoreClient.getDataToCom().addSpectator(game, user);
    }

    @Override
    public void sendUserInfo(String pseudo, String lastName, String firstName, Date dateOfBirth, int avatarId,
                             String password) throws OthelloException {
        //Checking if one required information is missing
        if (lastName == null || firstName == null || password == null) {
            throw new OthelloException("One or more required field is empty");
        }
        //If not, we create the user
        UUID id = UUID.randomUUID();
        createUser(new UserLight(id, pseudo, lastName, firstName, dateOfBirth, avatarId, 0, 0),
                new UserHeavy(id, pseudo, password, null, null, null));
    }

    @Override
    public void updateProfile(UserLight profileLight, UserHeavy profileHeavy) throws OthelloException {
        //Setting path of the user file
        String userFilePath = String.format(Configuration.JSON_FORMAT_USERS, Configuration.SAVE_DIR_USERS,
                File.separator, dataCoreClient.getLocalUserHeavy().getPseudo());
        File userFile = new File(userFilePath);
        //If the file is not found, there is a problem
        if (!userFile.exists()) {
            throw new OthelloException(
                    "File for user " + dataCoreClient.getLocalUserHeavy().getPseudo() + " not found");
        }

        //Renaming the actual file of a user to do backup file
        String oldFilePath = userFilePath + profileLight.getId().toString();
        File oldFile = new File(oldFilePath);
        if (oldFile.exists()) {
            throw new OthelloException("Error while backuping old data");
        }
        userFile.renameTo(oldFile);

        //Recreating the user with his new informations
        try {
            createUser(profileLight, profileHeavy);
            dataCoreClient.setLocalUserHeavy(profileHeavy);
            dataCoreClient.setLocalUserLight(profileLight);
            dataCoreClient.getDataToMain().updateLocalUser(dataCoreClient.getLocalUserLight());
        } catch (OthelloException e) {
            LOGGER.error(e.getMessage());
            //If there is a problem, we recover the backup file
            oldFile.renameTo(userFile);
        }
        //Deleting the old file
        oldFile.delete();
    }

    @Override
    public void identify(String login, String password, String address)
            throws OthelloException, IOException, ParseException {
        //If a required field is empty, this is wrong
        if (login.isEmpty() || password.isEmpty()) {
            throw new OthelloException("Data error: due to empty login or password");
        }
        //Calling login function
        login(login, password);
        //Connecting user to server
        dataCoreClient.getDataToCom().connectUserOnline(getUserLightFromFile(login), address);
    }

    /**
     * Get UserLight object from a user file
     *
     * @param pseudo: pseudo of the user
     */
    private UserLight getUserLightFromFile(String pseudo) throws OthelloException, IOException, ParseException {
        //Setting the path of the user file
        String path = String.format(Configuration.JSON_FORMAT_USERS, Configuration.SAVE_DIR_USERS, File.separator,
                pseudo);
        File userFile = new File(path);
        //If the file does not exist, this is a problem
        if (!userFile.exists()) {
            throw new OthelloException(Configuration.USER_PROFILE_DOES_NOT_EXIST);
        }

        //Creating a JsonObject containing informations from file
        JsonParser parser = new JsonParser();
        JsonObject userObject = parser.parse(new String(Files.readAllBytes(Paths.get(path)))).getAsJsonObject();

        //Returning UserLight object created form the informations of the JsonObject
        return new UserLight(UUID.fromString(userObject.get("id").getAsString()),
                userObject.get(Configuration.PROPERTY_NAME_USER_PSEUDO).getAsString(),
                userObject.get("lastName").getAsString(), userObject.get("firstName").getAsString(),
                userObject.get(Configuration.PROPERTY_NAME_USER_DATEOFBIRTH).isJsonNull() ? null
                        : new SimpleDateFormat("dd-MM-yyyy")
                        .parse(userObject.get(Configuration.PROPERTY_NAME_USER_DATEOFBIRTH).getAsString()),
                userObject.get("avatarId").getAsInt(), userObject.get("playedGames").getAsInt(),
                userObject.get("wonGames").getAsInt());
    }

    /**
     * Get UserHeavy object from a user file
     *
     * @param pseudo: pseudo of the user
     */
    private UserHeavy getUserHeavyFromFile(String pseudo) throws OthelloException, IOException {
        //Setting the path of the user file
        String path = String.format(Configuration.JSON_FORMAT_USERS, Configuration.SAVE_DIR_USERS, File.separator,
                pseudo);
        File userFile = new File(path);
        //If the file does not exist, this is a problem
        if (!userFile.exists()) {
            throw new OthelloException(Configuration.USER_PROFILE_DOES_NOT_EXIST);
        }

        //Creating a JsonObject containing informations from file
        JsonParser parser = new JsonParser();
        JsonObject userObject = parser.parse(new String(Files.readAllBytes(Paths.get(path)))).getAsJsonObject();

        //Returning UserLight object created form the informations of the JsonObject
        return new UserHeavy(UUID.fromString(userObject.get("id").getAsString()),
                userObject.get(Configuration.PROPERTY_NAME_USER_PSEUDO).getAsString(),
                userObject.get("password").getAsString(), userObject.get("serverAddress").toString(),
                userObject.get("serverPort").toString(), null);
    }

    /**
     * Check login informations given by user
     *
     * @param login:    login given by user
     * @param password: password given by user
     */
    private void login(String login, String password) throws OthelloException, IOException, ParseException {
        //Setting the path to the user file
        String path = String.format(Configuration.JSON_FORMAT_USERS, Configuration.SAVE_DIR_USERS, File.separator,
                login);
        File userFile = new File(path);
        //If the file does not exist, it means that the pseudo is wrong
        if (!userFile.exists()) {
            throw new OthelloException(Configuration.USER_PROFILE_DOES_NOT_EXIST); // throw related error
        } else {
            //If it exists, we get a UserLight and a UserHeavy from the file
            UserLight userLight = getUserLightFromFile(login);
            UserHeavy userHeavy = getUserHeavyFromFile(login);

            //If the password does not match the UserHeavy one, it is wrong
            if (!userHeavy.getPassword().equals(password)) {
                throw new OthelloException("Data error: User password does not match"); // throw related error
            } else {
                //Else, we set local player data and connect user online
                dataCoreClient.setLocalUserHeavy(userHeavy);
                dataCoreClient.setLocalUserLight(userLight);
                dataCoreClient.getDataToMain().sendUserInfo(userLight, userHeavy);
            }
        }
    }

    @Override
    public void disconnect() {
        //Sending disconnection request to the server
        dataCoreClient.getDataToCom().sendDisconnectingRequest(dataCoreClient.getLocalUserLight().getId());
    }

    @Override
    public List<String> getSavedGames() {
        List<String> listSavedGames = new ArrayList<>();
        //Setting the path of the folder containing saved games of the local user
        String pathToSavedGames = Configuration.SAVE_DIR + File.separator
                + dataCoreClient.getLocalUserLight().getId().toString();
        File folderSavedGames = new File(pathToSavedGames);

        //Security to only get files terminating by .json
        String pattern = ".*\\.json";

        //Adding in the list of saved games every file matching the pattern
        for (File f : Objects.requireNonNull(folderSavedGames.listFiles())) {
            if (f.isFile() && f.getName().matches(pattern)) {
                listSavedGames.add(f.getName());
            }
        }
        return listSavedGames;
    }

    @Override
    public void replayGame(Stage stage, String file) throws IOException, ParseException {
        //Initialisating a list of boards and a new board
        List<Board> boards = new ArrayList<>();
        Board board = new Board();

        //Intialisating a list of timestamp corresponding to the timestamp of the moves
        List<Timestamp> movesTime = new ArrayList<>();
        movesTime.add(new Timestamp(0));
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

        Chat chat = null;

        //Setting path to the file containing the game to replay
        String pathToFile = Configuration.SAVE_DIR + File.separator
                + dataCoreClient.getLocalUserLight().getId().toString() + File.separator + file;

        //Creating a JsonObject containing the informations of the file
        JsonParser parser = new JsonParser();
        JsonObject jsonGame = parser.parse(new String(Files.readAllBytes(Paths.get(pathToFile)))).getAsJsonObject();

        //Getting moves element of the JsonObject as a JsonArray
        JsonArray moves = jsonGame.getAsJsonArray("moves");

        //Creating fake users to set them in the Board object
        UserLight fakeUser1 = new UserLight(null, jsonGame.get("player1").toString(), null, null, null, -1, -1, -1);
        UserLight fakeUser2 = new UserLight(null, jsonGame.get("player2").toString(), null, null, null, -1, -1, -1);
        if (fakeUser1.getPseudo().equals(jsonGame.get("playerWhite").toString())) {
            board.setPlayer1(fakeUser2);
            board.setPlayer2(fakeUser1);
        } else {
            board.setPlayer1(fakeUser1);
            board.setPlayer2(fakeUser2);
        }
        boards.add(new Board(board));

        //Travelling the moves list
        //For each move, we add its time in the timestamp list,
        //we calculate the new board corresponding to this move,
        //and we add the new board in the boards list
        for (int i = 0; i < moves.size(); i++) {
            JsonObject moveObj = (JsonObject) moves.get(i);
            movesTime.add(new Timestamp(dateFormat.parse(moveObj.get("time").getAsString()).getTime()));
            board = updateBoard(board, moveObj.get("X").getAsInt(), moveObj.get("Y").getAsInt(), board.getNextPlayer());
            board = choseNextPlayer(board);
            boards.add(new Board(board));
        }

        //If chat is present, we create a chat containing all its messages
        if (jsonGame.has("chat")) {
            JsonArray chatArray = jsonGame.getAsJsonArray("chat");
            chat = new Chat();
            for (int i = 0; i < chatArray.size(); i++) {
                JsonObject messageObj = (JsonObject) chatArray.get(i);
                Message m = new Message(
                        new Timestamp(dateFormat.parse(messageObj.get("hourMessage").getAsString()).getTime()),
                        messageObj.get("message").getAsString(),
                        new UserZero(null, messageObj.get("author").getAsString()));
                chat.getChatList().add(m);
            }
        }

        //Sending all the information to IHM Game
        dataCoreClient.getDataToGame().replayGame(boards, movesTime, chat, stage);
    }

    /**
     * Get the value of the color opposed to the one provided in parameter (1 if parameter equals 2, 2 if parameter equals 1, else 0)
     *
     * @param color: value of the color you want to get the opposite
     */
    private int getOpposedColor(int color) {
        int opposedColor = color;
        if (color > 0) {
            opposedColor = color % 2 + 1;
        }
        return opposedColor;
    }

    /**
     * Check if the move is possible
     *
     * @param posX:      x axis movement
     * @param posY:      y axis movement
     * @param moveColor: color of the pawn to move
     * @param board:     Board from which we have to count score
     */
    private boolean isMoveValid(Board board, int posX, int posY, int moveColor) {
        //If the move is on a case already occupied, it is not valid
        if (!board.onBoard(posX, posY) || board.getColor(posX, posY) != 0) {
            return false;
        }
        //Creating a counter that represents number of cases traveled in each direction
        int cpt;
        //Checking in all directions
        for (int directionX : listeDirectionX) {
            for (int directionY : listeDirectionY) {
                //Not checking in direction 0,0 because it doesn't represent a direction
                if (!(directionX == 0 && directionY == 0)) {
                    //Counter initialized to 1 because we check at least the closest case in each direction
                    cpt = 1;
                    LOGGER.debug("DEBUG DIRECTION : {} {}", directionX, directionY);
                    //We continue to travel in a direction while we are on the board,
                    //and while the case travelled is occupied by the opposed color
                    while (board.onBoard(posX + directionX * cpt, posY + directionY * cpt)
                            && board.getColor(posX + directionX * cpt,
                            posY + directionY * cpt) == getOpposedColor(moveColor)) {
                        cpt++;
                        LOGGER.debug("DEBUG : {} {}", posX + directionX * cpt, posY + directionY * cpt);
                    }
                    //Once we are not on a case occupied by the opposite color,
                    // we check if we are still on the board, and if we travelled more than just the closest case,
                    //and finally if the last case we saw is occupied by our color
                    if (board.onBoard(posX + directionX * cpt, posY + directionY * cpt) && cpt > 1
                            && board.getColor(posX + directionX * cpt, posY + directionY * cpt) == moveColor) {
                        //If this is the case, it means that the move is framing at least one tile of the opposite color,
                        //so the move is valid
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Update the board according to the move given
     *
     * @param board:     old board
     * @param posX:      position x of the move
     * @param posY:      position y of the move
     * @param moveColor: color of the move
     */
    private Board updateBoard(Board board, int posX, int posY, int moveColor) {
        //It works like isMoveValid
        int cpt;
        for (int directionX : listeDirectionX) {
            for (int directionY : listeDirectionY) {
                if (!(directionX == 0 && directionY == 0)) {
                    cpt = 1;
                    while (board.onBoard(posX + directionX * cpt, posY + directionY * cpt)
                            && board.getColor(posX + directionX * cpt,
                            posY + directionY * cpt) == getOpposedColor(moveColor)) {
                        cpt++;
                    }
                    if (board.onBoard(posX + directionX * cpt, posY + directionY * cpt) && cpt > 1
                            && board.getColor(posX + directionX * cpt, posY + directionY * cpt) == moveColor) {
                        //Unless that once we find tiles framed by our color, we change their color
                        while (cpt > 0) {
                            cpt--;
                            board.setState(posX + directionX * cpt, posY + directionY * cpt, moveColor);
                        }
                    }
                }
            }
        }
        //Returning the new board
        return board;
    }

    /**
     * Check if there is a move valid.
     *
     * @param board:     board on which we check if there is a valid move
     * @param moveColor: color to check
     */
    private boolean isThereAMoveValid(Board board, int moveColor) {
        //For each case, we test if a move of the moveColor would be valid.
        //Once we find one case for which it's true, we return true.
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (isMoveValid(board, i, j, moveColor)) {
                    return true;
                }
            }
        }
        //If no case is a move valid, we return false
        return false;
    }

    /**
     * Chose next player for a given board
     *
     * @param board: the board to check who is the next player
     */
    private Board choseNextPlayer(Board board) {
        //If the player who didn't play has a valid move, it's his turn
        if (isThereAMoveValid(board, getOpposedColor(board.getNextPlayer()))) {
            board.setNextPlayer(getOpposedColor(board.getNextPlayer()));
            //Else, if the player who just played has a valid move, it's again his turn
        } else if (isThereAMoveValid(board, board.getNextPlayer())) {
            return board;
            //Else, nobody can play, so there is no next player
        } else {
            board.setNextPlayer(0);
        }
        return board;
    }
}
