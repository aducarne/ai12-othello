package client.data;

import java.io.File;

public class Configuration {

    public static final String SAVE_DIR = System.getProperty("user.home") + File.separator + "OthelloData";
    public static final String SAVE_DIR_USERS = SAVE_DIR + File.separator + "users";
    // app properties
    public static final String APPLICATION_NAME = "Othello";
    public static final int APPLICATION_WIDTH = 889;
    public static final int APPLICATION_HEIGHT = 500;
    public static final String DEFAULT_SERVER_ADDRESS = "localhost";
    public static final String DEFAULT_SERVER_PORT = "8080";
    // business object properties
    public static final String FX_CONTROLLER_NOTIFICATION_UPDATE_DATA = "updateData";
    public static final String FX_CONTROLLER_NOTIFICATION_RESPONSE_GAME_JOIN = "responseGameJoin";
    public static final String JSON_FORMAT_USERS = "%s%s%s.json";
    public static final String PROPERTY_NAME_USER_DATEOFBIRTH = "dateOfBirth";
    public static final String PROPERTY_NAME_USER_PSEUDO = "pseudo";
    public static final int PSEUDO_MAX_LENGTH = 20;
    // labels
    public static final String GAME_NOT_FOUND_ON_SERVER = "Game not found on server";
    public static final String USER_PROFILE_DOES_NOT_EXIST = "Data error: User profile does not exist";
    public static final String END_GAME_WIN = "Vous avez gagné : ";
    public static final String END_GAME_LOSE = "Vous avez perdu : ";
    public static final String END_GAME_TIMEOUT = " a écoulé tout son temps.";
    // css definition
    private static final File cssFile = new File("src/main/java/client/main/ressources/main.css");
    public static final String CSS_FILE_PATH = "file:///" + Configuration.cssFile.getAbsolutePath().replace("\\", "/");
    private Configuration() {
    }

}
