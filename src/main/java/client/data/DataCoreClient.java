package client.data;

import common.interfaces.client.*;
import common.model.GameLight;
import common.model.UserHeavy;
import common.model.UserLight;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public class DataCoreClient {
    private UserHeavy localUserHeavy;
    private UserLight localUserLight;
    private List<UserLight> listConnectedUsers;

    private List<GameLight> listGameLight;

    private MainToDataImpl mainToDataImpl;
    private ComToDataClientImpl comToDataClientImpl;
    private GameToDataImpl gameToDataImpl;

    private IDataToCom dataToCom;
    private IDataToMain dataToMain;
    private IDataToGame dataToGame;

    public DataCoreClient() {
        mainToDataImpl = new MainToDataImpl(this);
        comToDataClientImpl = new ComToDataClientImpl(this);
        gameToDataImpl = new GameToDataImpl(this);
        listConnectedUsers = new ArrayList<>();
        listGameLight = new ArrayList<>();

    }

    List<UserLight> getListConnectedUsers() {
        return listConnectedUsers;
    }

    void setListConnectedUsers(List<UserLight> listConnectedUsers) {
        this.listConnectedUsers = listConnectedUsers;
    }

    UserHeavy getLocalUserHeavy() {
        return localUserHeavy;
    }

    void setLocalUserHeavy(UserHeavy localUserHeavy) {
        this.localUserHeavy = localUserHeavy;
    }

    UserLight getLocalUserLight() {
        return localUserLight;
    }

    void setLocalUserLight(UserLight localUserLight) {
        this.localUserLight = localUserLight;
    }

    public IComToDataClient getClientComToDataImpl() {
        return comToDataClientImpl;
    }

    public IMainToData getMainToDataImpl() {
        return mainToDataImpl;
    }

    IDataToCom getDataToCom() {
        return dataToCom;
    }

    public void setDataToCom(IDataToCom dataToCom) {
        this.dataToCom = dataToCom;
    }

    List<GameLight> getListGameLight() {
        return listGameLight;
    }

    void setListGameLight(List<GameLight> listGameLight) {
        this.listGameLight = listGameLight;
    }

    IDataToMain getDataToMain() {
        return dataToMain;
    }

    public void setDataToMain(IDataToMain dataToMain) {
        this.dataToMain = dataToMain;
    }

    IDataToGame getDataToGame() {
        return dataToGame;
    }

    public void setDataToGame(IDataToGame dataToGame) {
        this.dataToGame = dataToGame;
    }

    public GameToDataImpl getGameToDataImpl() {
        return gameToDataImpl;
    }

    //Function to get a list iterator on the connected users list
    private ListIterator<UserLight> setIteratorOnUserList() {
        return getListConnectedUsers().listIterator();
    }

    //Function to get the index of a user in the list of connected users
    int findUserIndex(UUID userId) {
        ListIterator<UserLight> itUser = setIteratorOnUserList();
        boolean foundUser = false;
        int index = 0;
        while (itUser.hasNext() && !foundUser) {
            if (itUser.next().getId().equals(userId))
                foundUser = true;
            else
                index++;
        }
        if (!foundUser)
            return -1;
        return index;
    }

    //Function to get the UserLight of a user in the list of connected users
    UserLight findUser(UUID userId) {
        ListIterator<UserLight> itUser = setIteratorOnUserList();
        boolean foundUser = false;
        UserLight user = null;
        while (itUser.hasNext() && !foundUser) {
            user = itUser.next();
            if (user.getId().equals(userId))
                foundUser = true;
        }
        return user;
    }
}
