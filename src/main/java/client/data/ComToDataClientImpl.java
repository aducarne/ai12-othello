package client.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import common.exceptions.OthelloException;
import common.interfaces.client.IComToDataClient;
import common.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ComToDataClientImpl implements IComToDataClient {
    private static final Logger LOGGER = LogManager.getLogger(ComToDataClientImpl.class);
    private DataCoreClient dataCoreClient;

    ComToDataClientImpl(DataCoreClient dataCoreClient) {
        this.dataCoreClient = dataCoreClient;
    }

    @Override
    public void addNewUser(UserLight user) {
        //Adding the new user to the local list of connected, but not if it's the local player
        if (!dataCoreClient.getLocalUserLight().getId().equals(user.getId())) {
            dataCoreClient.getListConnectedUsers().add(user);
            dataCoreClient.getDataToMain().updateUserList(user);
        }
    }

    @Override
    public void notifyNewGame(GameLight newGameCreated) {
        //Adding new game from the server to the local list of games
        dataCoreClient.getListGameLight().add(newGameCreated);
        LOGGER.debug("Game received");
        dataCoreClient.getDataToMain().showNewGame(newGameCreated);
    }

    @Override
    public void saveGame(GameHeavy savedGame) throws IOException {
        //Creating the path of the file that will host the saved game
        String formattedDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm").format(savedGame.getGameStart());
        String gameFilePath = Configuration.SAVE_DIR + File.separator + dataCoreClient.getLocalUserLight().getId()
                + File.separator + savedGame.getGameCreator().getPseudo() + "_VS_" + savedGame.getPlayer2().getPseudo()
                + "_" + formattedDate + ".json";
        File gameFile = new File(gameFilePath);

        //Creating JsonObject that represents the game, and adding basic properties of the game
        JsonObject gameJson = new JsonObject();
        gameJson.addProperty("player1", savedGame.getGameCreator().getPseudo());
        gameJson.addProperty("player2", savedGame.getPlayer2().getPseudo());
        gameJson.addProperty("playerWhite", savedGame.isWhiteCreator() ? savedGame.getGameCreator().getPseudo()
                : savedGame.getPlayer2().getPseudo());
        gameJson.addProperty("time", formattedDate);

        //Creating JSON array that contains all the moves of the game
        JsonObject moveJson;
        JsonArray movesArray = new JsonArray();
        for (int i = 0; i < savedGame.getMoveList().size(); i++) {
            moveJson = new JsonObject();
            moveJson.addProperty("X", savedGame.getMoveList().get(i).getX());
            moveJson.addProperty("Y", savedGame.getMoveList().get(i).getY());
            moveJson.addProperty("player", savedGame.getMoveList().get(i).getPlayer().getPseudo());
            moveJson.addProperty("time", savedGame.getMoveList().get(i).getHourMove().toString());
            movesArray.add(moveJson);
        }
        gameJson.add("moves", movesArray);

        //If the chat was activated, creating a JSON array that contains all the messages of the game
        if (savedGame.isOkChat()) {
            JsonArray messagesArray = new JsonArray();
            JsonObject messageJson;
            for (int i = 0; i < savedGame.getChat().getChatList().size(); i++) {
                messageJson = new JsonObject();
                messageJson.addProperty("author", savedGame.getChat().getChatList().get(i).getAuthor().getPseudo());
                messageJson.addProperty("message", savedGame.getChat().getChatList().get(i).getMessage());
                messageJson.addProperty("hourMessage",
                        savedGame.getChat().getChatList().get(i).getHourMessage().toString());
                messagesArray.add(messageJson);
            }
            gameJson.add("chat", messagesArray);
        }

        //If the folder containing the file does not exist, it will be created
        boolean isDirectoryCreated = gameFile.getParentFile().mkdirs();
        try (FileWriter fw = new FileWriter(gameFilePath)) {
            fw.write(gameJson.toString());
        }
    }

    @Override
    public void receiveChatMessage(Message message) {
        dataCoreClient.getDataToGame().displayMessage(message);
    }

    @Override
    public void addSpectatorToGame(List<UserZero> spectators) {
        dataCoreClient.getDataToGame().updateSpectatorList(spectators);
    }

    @Override
    public void removeSpectatorFromClient(List<UserZero> spectatorList) {
        dataCoreClient.getDataToGame().updateSpectatorList(spectatorList);
    }

    @Override
    public void receiveJoinRequestAnswer(Boolean isAccepted, UUID gameUUID) {
        if (Boolean.TRUE.equals(isAccepted)) {
            dataCoreClient.getDataToMain().notifyAcceptance();
        } else {
            dataCoreClient.getDataToMain().notifyNonAcceptance();
            dataCoreClient.getDataToGame().notifyPlayerRejection();
        }
    }

    @Override
    public void receiveJoinRequest(UUID userProposingUUID, UUID gameUUID) throws OthelloException {
        UserLight userProposing = dataCoreClient.findUser(userProposingUUID);
        //If user who wants to join is not connected anymore, throw exception
        if (userProposing == null) {
            throw new OthelloException("User who wants to join is not connected anymore (not found)");
        }
        dataCoreClient.getDataToGame().askHostValidation(userProposing);
    }

    @Override
    public void notifyEndGame(Board board) {
        //We use the same function that is used to update the board,
        //because an ended game is just a particular board
        dataCoreClient.getDataToGame().updateInterface(board);
    }

    @Override
    public void updateBoard(Board board) {
        dataCoreClient.getDataToGame().updateInterface(board);
    }

    @Override
    public void receivePlayersnGames(List<UserLight> players, List<GameLight> gamesAvailable) {
        forwardPlayers(players);
        forwardGames(gamesAvailable);
    }

    @Override
    public void forwardPlayers(List<UserLight> players) {
        //Updating local list according to remote list
        dataCoreClient.setListConnectedUsers(players);

        if (dataCoreClient.findUserIndex(dataCoreClient.getLocalUserLight().getId()) != -1) {
            UserLight localUserFromServer = dataCoreClient.findUser(dataCoreClient.getLocalUserLight().getId());
            //If local user has been updated on the server (because of the update of his stats after a played game),
            //it is updated locally (in his file and in the application
            if (localUserFromServer.getPlayedGames() != dataCoreClient.getLocalUserLight().getPlayedGames()) {
                try {
                    dataCoreClient.getMainToDataImpl().updateProfile(localUserFromServer,
                            dataCoreClient.getLocalUserHeavy());
                } catch (OthelloException e) {
                    LOGGER.error("Error while updating user", e);
                }
            }
            //Local player is removed from the local list of connected users
            //(we don't want to display our own name in the list)
            dataCoreClient.getListConnectedUsers()
                    .remove(dataCoreClient.findUserIndex(dataCoreClient.getLocalUserLight().getId()));
        }
        //New list is transferred to IHM Main
        dataCoreClient.getDataToMain().sendUsersList(players);
    }

    /**
     * Forward new games to IHM Main
     *
     * @param games : the list of games with status "PENDING"
     */
    private void forwardNewGames(List<GameLight> games) {
        dataCoreClient.getDataToMain().sendGamesList(games);
    }

    /**
     * Forward games in progress to IHM Main
     *
     * @param games : the list of games with status "IN_PROGRESS"
     */
    private void forwardGamesInProgress(List<GameLight> games) {
        dataCoreClient.getDataToMain().sendGamesInProgress(games);
    }

    @Override
    public void forwardGames(List<GameLight> games) {
        //Updating local list of available games
        dataCoreClient.setListGameLight(games);

        //Creating 2 different lists : one for new games,
        //the other for games in progress
        List<GameLight> newGames = new ArrayList<>();
        List<GameLight> gamesInProgress = new ArrayList<>();
        for (GameLight game : games) {
            //If status is "PENDING", this is a new game
            if (game.getStatus().equals(GameStatus.PENDING))
                newGames.add(game);
                //If status is "IN_PROGRESS", this is a game in progress
            else if (game.getStatus().equals(GameStatus.IN_PROGRESS))
                gamesInProgress.add(game);
        }
        //Removing on the client games with status "ENDED"
        dataCoreClient.getListGameLight().removeIf(game -> game.getStatus().equals(GameStatus.ENDED));
        forwardNewGames(newGames);
        forwardGamesInProgress(gamesInProgress);
    }

    @Override
    public void launchGame(UUID gameId, Board board) {
        //Launching a game is just updating the game interface with a fresh board
        dataCoreClient.getDataToGame().updateInterface(board);
    }
}
