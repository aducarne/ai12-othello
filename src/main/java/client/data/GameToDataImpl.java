package client.data;

import common.exceptions.OthelloException;
import common.interfaces.client.IGameToData;
import common.model.*;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Timestamp;
import java.util.*;

public class GameToDataImpl implements IGameToData {
    private static final Logger LOGGER = LogManager.getLogger(GameToDataImpl.class);

    //Instanciating 2 lists useful for the game logic algorithm
    private static List<Integer> listeDirectionX = new ArrayList<>(Arrays.asList(-1, 0, 1));
    private static List<Integer> listeDirectionY = new ArrayList<>(Arrays.asList(-1, 0, 1));

    private DataCoreClient dataCoreClient;

    GameToDataImpl(DataCoreClient dataCoreClient) {
        this.dataCoreClient = dataCoreClient;
    }

    /**
     * Get the value of the color opposed to the one provided in parameter (1 if parameter equals 2, 2 if parameter equals 1, else 0)
     *
     * @param color: value of the color you want to get the opposite
     */
    private int getOpposedColor(int color) {
        int opposedColor = color;
        if (color > 0) {
            opposedColor = color % 2 + 1;
        }
        return opposedColor;
    }

    /**
     * Check if the move is possible
     *
     * @param posX:      x axis movement
     * @param posY:      y axis movement
     * @param moveColor: color of the pawn to move
     * @param board:     Board from which we have to count score
     */
    private boolean isMoveValid(Board board, int posX, int posY, int moveColor) {
        //If the move is on a case already occupied, it is not valid
        if (!board.onBoard(posX, posY) || board.getColor(posX, posY) != 0) {
            return false;
        }
        //Creating a counter that represents number of cases traveled in each direction
        int cpt;
        //Checking in all directions
        for (int directionX : listeDirectionX) {
            for (int directionY : listeDirectionY) {
                //Not checking in direction 0,0 because it doesn't represent a direction
                if (!(directionX == 0 && directionY == 0)) {
                    //Counter initialized to 1 because we check at least the closest case in each direction
                    cpt = 1;
                    LOGGER.debug("DEBUG DIRECTION : {} {}", directionX, directionY);
                    //We continue to travel in a direction while we are on the board,
                    //and while the case travelled is occupied by the opposed color
                    while (board.onBoard(posX + directionX * cpt, posY + directionY * cpt)
                            && board.getColor(posX + directionX * cpt,
                            posY + directionY * cpt) == getOpposedColor(moveColor)) {
                        cpt++;
                        LOGGER.debug("DEBUG : {} {}", posX + directionX * cpt, posY + directionY * cpt);
                    }
                    //Once we are not on a case occupied by the opposite color,
                    // we check if we are still on the board, and if we travelled more than just the closest case,
                    //and finally if the last case we saw is occupied by our color
                    if (board.onBoard(posX + directionX * cpt, posY + directionY * cpt) && cpt > 1
                            && board.getColor(posX + directionX * cpt, posY + directionY * cpt) == moveColor) {
                        //If this is the case, it means that the move is framing at least one tile of the opposite color,
                        //so the move is valid
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void playMove(int x, int y, UUID gameId, Board board) {
        //If the move is due to a click on the forfeit button (x = -1) or if the move is valid,
        //it is sent to the server
        if (x == -1 || isMoveValid(board, x, y, board.getNextPlayer())) {
            Move m = new Move(dataCoreClient.getLocalUserLight().convertToUserZero(),
                    new Timestamp(new Date().getTime()), x, y);
            dataCoreClient.getDataToCom().sendMove(gameId, m, board);
        }
        //If it is not, the board is resent to the player who tried to make a move
        else
            dataCoreClient.getDataToGame().updateInterface(board);
    }

    @Override
    public void askGame(UUID gameId, UUID userId) {
        dataCoreClient.getDataToCom().requestGameSave(gameId, userId);
    }

    @Override
    public void newMessage(String m, UUID gameId) {
        //Creating a message whose author is the local player, and time is the current time
        dataCoreClient.getDataToCom().sendChatMessage(new Message(new Timestamp(new Date().getTime()), m,
                dataCoreClient.getLocalUserLight().convertToUserZero()), gameId);
    }

    @Override
    public void acceptanceAcquired(UUID uId, Boolean validation) throws OthelloException {
        //Finding the game ID using the creator ID
        UUID gameId = null;
        ListIterator<GameLight> it = dataCoreClient.getListGameLight().listIterator();
        GameLight currentGame = null;
        boolean found = false;
        //Travelling the local games lists, and finding the one for which the local user is the creator
        while (it.hasNext() && !found) {
            currentGame = it.next();
            if (currentGame.getGameCreator().getId().equals(dataCoreClient.getLocalUserLight().getId())) {
                found = true;
                gameId = currentGame.getId();
            }
        }
        //If we don't find it, there is a problem
        if (!found) {
            throw new OthelloException("Error retrieving game id");
        }
        //Else we send the join request answer to the server in order for the
        //asking player to receive the answer
        dataCoreClient.getDataToCom().sendJoinRequestAnswer(uId, gameId, validation);
    }

    @Override
    public UUID getLocalPlayerUUID() {
        return dataCoreClient.getLocalUserLight().getId();
    }

    @Override
    public int[] getScore(Board board) {
        int[] score = new int[2];
        //Travelling across the board and counting the number of 1 (representing black tiles)
        //and 2 (representing white tiles)
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (board.getStates()[i][j] == 1)
                    score[0]++;
                else if (board.getStates()[i][j] == 2)
                    score[1]++;
            }
        }
        return score;
    }

    @Override
    public void removeSpectator(UUID gameUUID) {
        UserZero user = new UserZero(dataCoreClient.getLocalUserLight().getId(), dataCoreClient.getLocalUserLight().getPseudo());
        dataCoreClient.getDataToCom().removeSpectatorRequest(user, gameUUID);
    }

    @Override
    public void gameFinished(Stage s) {
        dataCoreClient.getDataToMain().gameFinished(s);
    }
}
