package client.network;

import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;
import org.springframework.web.socket.sockjs.frame.Jackson2SockJsMessageCodec;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Class responsible for the first connection and handshake between the server and client
 */
public class WebSocketStompSessionInitializer {
    private static final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
    private static final WebSocketStompSessionHandler webSocketStompSessionHandler = new WebSocketStompSessionHandler();

    /**
     * Open a new WebSocket connection over Stomp
     *
     * @param hostname the hostname of the server
     * @param port     the port of the server
     * @return
     * @throws ExecutionException   Exception thrown when attempting to retrieve the result of a task that aborted by throwing an exception
     * @throws InterruptedException Exception thrown when the server closes the connection
     */
    public static WebSocketStompSessionHandler init(String hostname, Integer port) throws ExecutionException, InterruptedException {
        WebSocketStompSessionInitializer webSocketStompSessionInitializer = new WebSocketStompSessionInitializer();

        ListenableFuture<StompSession> session = webSocketStompSessionInitializer.connect(hostname, port);
        session.get();
        return webSocketStompSessionHandler;
    }

    /**
     * Set the configuration to be used for messaging
     *
     * @param hostname the hostname of the server
     * @param port     the port of the server
     * @return a listenable future list containing the newly opened stomp session
     */
    private ListenableFuture<StompSession> connect(String hostname, Integer port) {

        List<Transport> transports = new ArrayList<>(1);
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        container.setDefaultMaxBinaryMessageBufferSize(102400 * 1024);
        container.setDefaultMaxTextMessageBufferSize(102400 * 1024);
        transports.add(new WebSocketTransport(new StandardWebSocketClient(container)));

        SockJsClient sockJsClient = new SockJsClient(transports);
        sockJsClient.setMessageCodec(new Jackson2SockJsMessageCodec());

        WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        String url = "ws://{host}:{port}/messages";
        return stompClient.connect(url, headers, webSocketStompSessionHandler, hostname, port);
    }
}
