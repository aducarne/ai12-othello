package client.network;

import common.exceptions.OthelloException;
import common.interfaces.client.IComToDataClient;
import common.interfaces.client.IDataToCom;
import common.network.ComCoreMeta;
import common.network.messages.JoinRequestAnswerMessage;
import common.network.messages.JoinRequestMessage;
import common.network.messages.PlayerReadyMessage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * Communication core containing message controller and interfaces.
 */
@Component
public class ComCoreClient extends ComCoreMeta {

    private static final Logger LOGGER = LogManager.getLogger(ComCoreClient.class);
    private DataToComClientImpl dataToComClientImpl;
    private IComToDataClient comToDataClient;
    private MessageClientController messageClientController;

    public ComCoreClient() {
        dataToComClientImpl = new DataToComClientImpl(this);
        messageClientController = MessageClientController.getInstance();
        messageClientController.setComCore(this);
    }


    /**
     * Get the DataToCom interface implementation
     *
     * @return the implementation
     */
    public IDataToCom getDataToComClientImpl() {
        return dataToComClientImpl;
    }

    /**
     * Get the ComToData interface implementation
     *
     * @return the implementation
     */
    public IComToDataClient getComToDataClient() {
        return comToDataClient;
    }

    public void setComToDataClient(IComToDataClient comToDataClient) {
        this.comToDataClient = comToDataClient;
    }

    /**
     * Get the message controller associated to the client
     *
     * @return the message controller
     */
    public MessageClientController getMessageClientController() {
        return messageClientController;
    }

    /**
     * Init the WebSocket connection process
     *
     * @param hostname the hostname url
     * @param port     the port
     */
    public void initWebSocketConnection(String hostname, int port) {
        try {
            WebSocketStompSessionHandler webSocketStompSessionHandler = WebSocketStompSessionInitializer.init(hostname,
                    port);
            this.messageClientController.setWsHandler(webSocketStompSessionHandler);
        } catch (Exception e) {
            LOGGER.error("Error when trying to setup WebSocket connection", e);
        }
    }

    public void joinRequestMessageProcess(JoinRequestMessage message) {
        ComCoreClient comCoreClient = (ComCoreClient) message.getComCoreMeta();
        try {
            comCoreClient.getComToDataClient().receiveJoinRequest(message.getPlayerAskingID(), message.getGameID());
        } catch (OthelloException e) {
            LOGGER.error("Error when trying process JoinRequestMessage", e);
        }
    }

    public void joinRequestAnswerMessageProcess(JoinRequestAnswerMessage message) {
        if (Boolean.TRUE.equals(message.isAccepted())) {
            messageClientController.subscribeToRoom(message.getGameAskedID().toString());
        }
        PlayerReadyMessage readyMessage = new PlayerReadyMessage(message.getPlayerAskingID(), message.getGameAskedID(), message.isAccepted());
        messageClientController.sendPlayerReadyToServer(readyMessage);
        ComCoreClient comCoreClient = (ComCoreClient) message.getComCoreMeta();
        comCoreClient.getComToDataClient()
                .receiveJoinRequestAnswer(message.isAccepted(), message.getGameAskedID());
    }
}
