package client.network;

import common.interfaces.client.IDataToCom;
import common.model.*;
import common.network.messages.*;

import java.util.UUID;


public class DataToComClientImpl implements IDataToCom {

    private ComCoreClient comCoreClient;

    public DataToComClientImpl(ComCoreClient comCoreClient) {
        this.comCoreClient = comCoreClient;
    }

    @Override
    public void getPlayerByUUID(UUID playerRequested) {
        // TODO Auto-generated method stub

    }

    /**
     * This method is used when a client is trying to connect to the server.
     * His informations are sent through WebSocket for him to be added to the server's
     * list of users.
     */
    @Override
    public void connectUserOnline(UserLight userToConnect, String address) {
        String port = address.split(":")[1];
        String hostname = address.split(":")[0];
        comCoreClient.initWebSocketConnection(hostname, Integer.parseInt(port));
        comCoreClient.getMessageClientController().subscribeUser(userToConnect.getId().toString());
        LoginUserToServerMessage message = new LoginUserToServerMessage(userToConnect, address);
        comCoreClient.getMessageClientController().sendLoginUserToServer(message);
    }

    @Override
    public void addNewGameAvailable(GameLight newGame) {
        comCoreClient.getMessageClientController().subscribeToRoom(newGame.getId().toString());
        NewGameToServerMessage message = new NewGameToServerMessage(newGame);
        comCoreClient.getMessageClientController().sendNewGameAvailableToServer(message);
    }

    @Override
    public void requestGameSave(UUID gameId, UUID userId) {
        GameSaveRequestMessage message = new GameSaveRequestMessage(gameId, userId);
        comCoreClient.getMessageClientController().sendGameSaveRequestToServer(message);
    }

    @Override
    public void sendChatMessage(Message chatMessage, UUID gameID) {
        ChatMessageToServerMessage msg = new ChatMessageToServerMessage(chatMessage, gameID);
        comCoreClient.getMessageClientController().sendChatMessageToServer(msg);
    }

    @Override
    public void sendDisconnectingRequest(UUID user) {
        DisconnectionRequestMessage msg = new DisconnectionRequestMessage(user);
        comCoreClient.getMessageClientController().sendDisconnectionToServer(msg);
    }

    @Override
    public void addSpectator(GameLight game, UserLight spectator) {
        SpectatorToAddServerMessage message = new SpectatorToAddServerMessage(game, spectator);
        MessageClientController.getInstance().subscribeToRoom(game.getId().toString());
        MessageClientController.getInstance().sendSpectatorToAddToServer(message);
    }

    @Override
    public void askJoinGame(UUID gameId, UUID userId) {
        JoinRequestMessage message = new JoinRequestMessage(gameId, userId);
        MessageClientController.getInstance().sendJoinRequestMessage(message);
    }

    @Override
    public void sendJoinRequestAnswer(UUID userRequesting, UUID gameUUID, Boolean isAccepted) {
        JoinRequestAnswerMessage message = new JoinRequestAnswerMessage(userRequesting, gameUUID, isAccepted);
        MessageClientController.getInstance().sendJoinRequestAnswerMessage(message);
    }

    @Override
    public void sendMove(UUID gameID, Move move, Board board) {
        MoveToSendToServerMessage moveMessage = new MoveToSendToServerMessage(gameID, move, board);
        MessageClientController.getInstance().sendMoveToServer(moveMessage);
    }

    /**
     * Request the game list to the server
     */
    @Override
    public void getServerGameList() {
        RequestGameListMessage message = new RequestGameListMessage();
        MessageClientController.getInstance().sendRequestGameList(message);
    }

    @Override
    public void removeSpectatorRequest(UserZero user, UUID gameID) {
        RemoveSpectatorFromServerMessage message = new RemoveSpectatorFromServerMessage(user, gameID);
        MessageClientController messageClientController = client.network.MessageClientController.getInstance();
        messageClientController.sendRemoveSpectatorRequest(message);
        messageClientController.removeFromRoom();
    }
}
