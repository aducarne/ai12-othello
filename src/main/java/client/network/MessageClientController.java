package client.network;

import common.network.messages.*;

import java.util.Optional;

/**
 * Class handling message sending & receiving
 */
public class MessageClientController {

    private static MessageClientController instance = new MessageClientController();
    private WebSocketStompSessionHandler wsHandler;
    private ComCoreClient comCore;

    private MessageClientController() {
    }

    /**
     * Get the unique instance of this class
     *
     * @return the instance of MessageController
     */
    public static MessageClientController getInstance() {
        return Optional.of(instance).orElseGet(MessageClientController::new);
    }

    ComCoreClient getComCore() {
        return comCore;
    }

    void setComCore(ComCoreClient comCore) {
        this.comCore = comCore;
    }

    /**
     * Set the new WebSocket handler to use for messaging
     *
     * @param wsHandler the WebSocket handler to use
     */
    void setWsHandler(WebSocketStompSessionHandler wsHandler) {
        this.wsHandler = wsHandler;
    }

    /**
     * Send a LoginUserToServerMessage to the server
     *
     * @param message the message to send
     */
    void sendLoginUserToServer(LoginUserToServerMessage message) {
        wsHandler.sendMessage(message);
    }

    void sendNewGameAvailableToServer(NewGameToServerMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a ChatMessageToServerMessage to the server
     *
     * @param message the message to send
     */
    void sendChatMessageToServer(ChatMessageToServerMessage message) {
        // TODO send to the correct game by retrieving message.gameID
        wsHandler.sendMessage(message);
    }

    /**
     * Send a RequestGameListMessage to the server
     *
     * @param message the message to send
     */
    void sendRequestGameList(RequestGameListMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a JoinRequestMessage (a user wants to be added to an existing game,
     * he/she sends a request with his/her ID and the ID of the game to add.
     *
     * @param message: message to be send to server
     */
    void sendJoinRequestMessage(JoinRequestMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a JoinRequestMessage (a user wants to be added to an existing game,
     * he/she sends a request with his/her ID and the ID of the game to add.
     *
     * @param message: message to be send to server
     */
    void sendJoinRequestAnswerMessage(JoinRequestAnswerMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a GameListMessage to the server
     *
     * @param message the message to send
     */
    public void sendGameListToData(GameListMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a MoveToSendToServerMessage to the server
     *
     * @param message the message to send
     */
    void sendMoveToServer(MoveToSendToServerMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a DisconnectionRequestMessage to the server
     *
     * @param message the message to send
     */
    void sendDisconnectionToServer(DisconnectionRequestMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Sends a GameSaveRequestMessage to the server
     *
     * @param message: the message to send
     */
    public void sendGameSaveRequestToServer(GameSaveRequestMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a PlayerReadyMessage to the server
     *
     * @param message: the message to send
     */
    public void sendPlayerReadyToServer(PlayerReadyMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Send a SpectatorToAddClientMessage to the server
     *
     * @param message the message to send
     */
    public void sendSpectatorToAddToServer(SpectatorToAddServerMessage message) {
        wsHandler.sendMessage(message);
    }

    public void subscribeToRoom(String roomId) {
        this.wsHandler.subscribeToRoom(roomId);
    }

    void subscribeUser(String userId) {
        this.wsHandler.subscribeUser(userId);
    }

    void sendRemoveSpectatorRequest(RemoveSpectatorFromServerMessage message) {
        wsHandler.sendMessage(message);
    }

    /**
     * Remove the client from listening to the room
     */
    public void removeFromRoom() {
        wsHandler.unsubscribeFromRoom();
    }
}
