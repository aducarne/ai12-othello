package client.network;

import common.network.messages.MessageMeta;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.lang.reflect.Type;

/**
 * WebSocket message handler
 */
public class WebSocketStompSessionHandler extends StompSessionHandlerAdapter {
    public static final String SEND_MESSAGE_TOPIC = "/topic/serverMessages";
    static final String RECEIVE_MESSAGE_TOPIC = "/topic/clientMessages";
    private static final Logger LOGGER = LogManager.getLogger(WebSocketStompSessionHandler.class);
    private StompSession stompSession;
    private String currentUser;
    private StompSession.Subscription roomSubscription;

    /**
     * Send a message to the server
     *
     * @param message the message to be sent
     */
    void sendMessage(MessageMeta message) {
        stompSession.send("/topic/user/" + currentUser, message);
        LOGGER.info("Message sent to server : {}", message);
    }

    /**
     * Set the session and subscribe to a topic after handshake
     *
     * @param session          the session opened between server and client
     * @param connectedHeaders the StomHeaders of the session
     */
    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        stompSession = session;
        LOGGER.info("New session established : {}", session.getSessionId());
        synchronized (stompSession) {
            stompSession.subscribe(RECEIVE_MESSAGE_TOPIC, this);
        }
        LOGGER.info("Subscribed to " + RECEIVE_MESSAGE_TOPIC);
    }

    /**
     * Log an error when an exception is raised
     *
     * @param session   the current session
     * @param command   the current StompCommand
     * @param headers   the current StompHeaders
     * @param payload   the current payload
     * @param exception the raised exception
     */
    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        LOGGER.error("Got an exception", exception);
    }

    /**
     * Get the payload for serialization/deserialization with Jackson API
     *
     * @param headers the StompHeaders
     * @return the type of the class to serialize/deserialize
     */
    @Override
    public Type getPayloadType(StompHeaders headers) {
        return MessageMeta.class;
    }

    /**
     * Process the message received from the server
     * Triggered when the client app receives a message from the server
     *
     * @param headers the current StompHeaders
     * @param payload the message sent by the server
     */
    @Override
    public synchronized void handleFrame(StompHeaders headers, Object payload) {
        MessageMeta message = (MessageMeta) payload;
        LOGGER.info("New server message received : {}", message);
        try {
            message.setComCoreMeta(MessageClientController.getInstance().getComCore());
            message.processData();
        } catch (Exception e) {
            LOGGER.error("handleFrame", e);
        }
    }

    void subscribeToRoom(String roomId) {
        final String destination = "/topic/" + roomId + "/room";
        synchronized (stompSession) {
            roomSubscription = stompSession.subscribe(destination, this);
        }
        LOGGER.info("Subscribed to {}", destination);
    }

    /**
     * End the subscription to the room endpoint
     */
    void unsubscribeFromRoom() {
        this.roomSubscription.unsubscribe();
    }

    void subscribeUser(String userId) {
        final String destination = "/topic/" + userId + "/user";
        synchronized (stompSession) {
            stompSession.subscribe(destination, this);
        }
        LOGGER.info("Subscribed to {}", destination);
        this.currentUser = userId;
    }

}
