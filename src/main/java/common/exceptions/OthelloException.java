package common.exceptions;

public class OthelloException extends Exception {

    public OthelloException() {
        super();
    }

    public OthelloException(String message) {
        super(message);
    }

    public OthelloException(String message, Throwable cause) {
        super(message, cause);
    }
}
