package common.interfaces.server;

import common.exceptions.OthelloException;
import common.model.*;

import java.util.List;
import java.util.UUID;

/**
 * This class is an interface which provides server methods to communicate from
 * Com to Data modules.
 */
public interface IComToDataServer {

    /**
     * Add a new user to the list.
     *
     * @param userLight : the user to add
     */
    void addAuthenticatedPlayer(UserLight userLight);

    /**
     * Create a new game.
     *
     * @param gameLight : the game to create
     */
    void createGame(GameLight gameLight);

    /**
     * Request saving a game by UUID.
     *
     * @param gameID : the UUID of the game to save
     */
    void requestGameSave(UUID userID, UUID gameID);

    /**
     * Receive a new chat message to transfer it.
     *
     * @param chatMessage : Data Message to transfer
     * @param gameID      : the UUID of the game in which the message was sent
     */
    void receiveNewChatMessage(Message chatMessage, UUID gameID) throws OthelloException;

    /**
     * Notify that a user has been disconnected.
     *
     * @param userID : the UUID of the disconnected user
     */
    void disconnectUser(UUID userID) throws OthelloException;

    /**
     * Add a spectator to a game.
     *
     * @param gameLight : the game concerned
     * @param userLight : the user to add as spectator
     */
    void addSpectator(GameLight gameLight, UserLight userLight);

    /**
     * Remove a spectator from the game
     *
     * @param user:     the user to remove from the spectators list
     * @param gameUUID: the game from which to remove the user
     */
    void removeSpectatorFromServer(UserZero user, UUID gameUUID);

    /**
     * Send a move to the data module.
     *
     * @param gameID : the ID of the game
     * @param move   : the move to send
     * @param board  : the board to send
     */
    void sendMove(UUID gameID, Move move, Board board) throws OthelloException;

    List<UserLight> getListUsers();

    List<GameLight> getListGames();

    /**
     * Receive a Join Request Answer for a game A game owner received a Join Request
     * from another user and decided to accept/reject it. The server gets the ID of
     * the game, the ID of the user who wanted to become player and the answer of
     * the owner (yes/no).
     *
     * @param playerAskingID : the player asking to join
     * @param gameId:        the game concerned
     * @param isAccepted:    the answer of the game owner (true if yes, false if no)
     */
    void receiveJoinRequestAnswer(UUID playerAskingID, UUID gameId, Boolean isAccepted) throws OthelloException;

    UUID getGameCreator(UUID gameId) throws OthelloException;

    UserZero getWinner(Board board) throws OthelloException;
}
