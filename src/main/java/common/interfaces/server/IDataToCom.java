package common.interfaces.server;

import common.model.*;

import java.util.List;
import java.util.UUID;


/**
 * This class is an interface which provides server methods to communicate from
 * Data to Com modules.
 */
public interface IDataToCom {

    /**
     * Notify that a new game has been created.
     *
     * @param newGameCreated : the new game created
     */
    void notifyNewGame(GameLight newGameCreated);

    /**
     * Remove a spectator from client
     *
     * @param spectatorList
     * @param gameUUID
     */
    void removeSpectatorFromClient(List<UserZero> spectatorList, UUID gameUUID);

    /**
     * Send a move update message to the different users of a game.
     *
     * @param gameID      : the ID of the game
     * @param updateBoard : the board to update
     */
    void sendUpdateMoveMessage(UUID gameID, Board updateBoard);

    /**
     * Send a move update message to the different users of a game.
     */
    void sendEndGameSignal(UUID gameID, Board endedGame);

    void sendWinner(UUID gameID, Board update, UserZero winner);

    /**
     * Send to a newly authenticated user the list of other online players.
     *
     * @param playerToNotify : the players of the game
     * @param playersOnline  : the spectators of the game
     */

    void sendNewUserLists(UserLight playerToNotify, List<UserLight> playersOnline, List<GameLight> gamesOnline);

    /**
     * Send the game list to the Com module
     *
     * @param gameLights the game list to send
     */
    void sendGameList(List<GameLight> gameLights);

//    /**
//     * Send the Join Request Answer to the user who asked to join 
//     * a game.
//     * @param isAccepted: the answer of the game owner
//     * @param userToNotify: UUID of the user who should receive the answer
//     * */
//    void notifyJoinRequestAnswer(UUID userToNotify, boolean isAccepted);

    /**
     * Send to all users associated to a game (players + spectators) a message to
     * start the game (switch to game view, be ready for first move ...)
     *
     * @param gameID: UUID of the game to launch
     * @param board:  the board to send to clients
     */
    void launchGame(UUID gameID, Board board);

    /**
     * Send to all users a message to update the list of games online
     *
     * @param updatedGameList: list of games online
     */
    void notifyGameListChanged(List<GameLight> updatedGameList);

    /**
     * Send to all users a message to  update the list of games online
     *
     * @param updatedUserList: list of users online
     */
    void notifyUserListChanged(List<UserLight> updatedUserList);

    /**
     * Send to a requesting user a message containing a game save
     *
     * @param game:             Game Heavy object containing the game save
     * @param receiverPlayerId: UUID of the user who requested a game save
     */
    void sendGameSave(GameHeavy game, UUID receiverPlayerId);

    /**
     * Send the list of spectators to the players of the game
     *
     * @param spectatorList the list of spectator to send
     * @param gameId        the ID of the game
     */
    void sendSpectatorList(List<UserZero> spectatorList, UUID gameId);
}
