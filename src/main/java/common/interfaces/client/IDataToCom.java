package common.interfaces.client;

import common.model.*;

import java.util.UUID;

/**
 * This class is an interface which provides client methods to communicate from
 * Data to Com modules.
 */
public interface IDataToCom {
    /**
     * Get a player by its UUID.
     *
     * @param playerRequested : the UUID of the player requested
     */
    void getPlayerByUUID(UUID playerRequested);

    /**
     * Notify the server that a user is currently online.
     *
     * @param user    : the user connected
     * @param address : the address of the user
     */
    void connectUserOnline(UserLight user, String address);

    /**
     * Add a new available game in the list.
     *
     * @param newGame : the game to add to the list
     */
    void addNewGameAvailable(GameLight newGame);

    /**
     * Request a save of the current game.
     *
     * @param gameId : the UUID of the game to save
     * @param userId : the UUID of the user wanted to save the game
     */
    void requestGameSave(UUID gameId, UUID userId);

    /**
     * Send a chat message.
     *
     * @param chatMessage : the data message to be sent
     * @param gameID      : the game in which the message was sent
     */

    void sendChatMessage(Message chatMessage, UUID gameID);

    /**
     * Send a disconnecting request to the server.
     *
     * @param user : the user to disconnect
     */
    void sendDisconnectingRequest(UUID user);

    /**
     * Add a spectator to a game.
     *
     * @param game      : the game to be spectated
     * @param spectator : the user to add as a spectator
     */
    void addSpectator(GameLight game, UserLight spectator);

    /**
     * Ask to remove the user from spectators
     *
     * @param user
     * @param gameUUID
     */
    void removeSpectatorRequest(UserZero user, UUID gameUUID);

    /**
     * Ask the server to join a game.
     *
     * @param gameId : the UUID of the game to join
     * @param userId : the UUID of the user to add to the game
     */
    void askJoinGame(UUID gameId, UUID userId);

    /**
     * Send the answer of a request answer.
     *
     * @param userRequesting : the UUID of the user wanting to join
     * @param gameUUID       : the UUID of the game to join
     * @param isAccepted     : the answer of the join request
     */
    void sendJoinRequestAnswer(UUID userRequesting, UUID gameUUID, Boolean isAccepted);

    /**
     * Send a move to the server
     *
     * @param gameID : the ID of the game
     * @param board  : the board with the move to send
     * @param move   : the move to send
     */
    void sendMove(UUID gameID, Move move, Board board);

    /**
     * Get the list of games from the server
     */
    void getServerGameList();

}
