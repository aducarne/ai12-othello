package common.interfaces.client;

import common.model.GameLight;
import common.model.UserHeavy;
import common.model.UserLight;
import javafx.stage.Stage;

import java.util.List;

public interface IDataToMain {
    /**
     * Notification telling that join request as player has been refused
     */
    void notifyNonAcceptance();

    /**
     * Notification telling that join request as player has been accepted
     */
    void notifyAcceptance();

    /**
     * Request host player to join its game as spectator
     *
     * @param userLight User wanting to join
     */
    public void askHoteValidation(UserLight userLight);

    /**
     * Display new game
     *
     * @param gl Data of the new game
     */
    void showNewGame(GameLight gl);

    /**
     * Sends connected users list and ongoing games
     *
     * @param usrList Connected users list
     */
    void sendUsersList(List<UserLight> usrList);

    /**
     * Sends connected users list and ongoing games
     *
     * @param gameList Connected users list
     */
    void sendGamesList(List<GameLight> gameList);


    /**
     * Sends connected users list and ongoing games
     *
     * @param gameList Connected users list
     */
    void sendGamesInProgress(List<GameLight> gameList);

    /**
     * Update other's connected users list with a new one
     *
     * @param user User to add to lists
     */
    void updateUserList(UserLight user);

    /**
     * Sends information related to the user in param.
     * This is commonly used to update the model in the root controller,
     * in order to user info be accessible through the different views.
     *
     * @param userLight
     * @param userHeavy
     */
    void sendUserInfo(UserLight userLight, UserHeavy userHeavy);

    /**
     * @param s
     */
    void gameFinished(Stage s);

    /**
     * Update the local UserLight in the MainApplicationModel.
     * Called when stats of the user are updated after a game.
     *
     * @param userLightUpdated the new UserLight
     */
    public void updateLocalUser(UserLight userLightUpdated);
}
