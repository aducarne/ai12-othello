package common.interfaces.client;

public interface IMainService {
    /**
     * Toggles password display
     */
    void seeClear();

    /**
     * Imports a picture for user profile
     */
    void avatarImport();
}
