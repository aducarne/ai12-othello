package common.interfaces.client;

import common.exceptions.OthelloException;
import common.model.Board;
import javafx.stage.Stage;

import java.util.UUID;

public interface IGameToData {
    /**
     * Send a move data notification
     *
     * @param x:      x axis movement
     * @param y:      y axis movement
     * @param gameId: id of the game from which the move is made
     * @param board:  Board from which we have to count score
     */
    void playMove(int x, int y, UUID gameId, Board board);

    /**
     * Sending a request to the server to save the game
     * passed in parameter for the user passed in parameter
     *
     * @param gameId: id of the game you want to save
     * @param userId: id of the player requesting the save
     */
    void askGame(UUID gameId, UUID userId);

    /**
     * Constructing and sending a new chat message to the server
     *
     * @param m:      content of the message
     * @param gameId: id of the game where the message is sent
     */
    void newMessage(String m, UUID gameId);

    /**
     * Send confirmation notification
     *
     * @param uId:        User UUID of the user requesting to join the game
     * @param validation: boolean value of the validation
     */
    void acceptanceAcquired(UUID uId, Boolean validation) throws OthelloException;

    /**
     * Getting the UUID of the local player for IHM Game
     *
     * @return : the UUID of the local player
     */
    UUID getLocalPlayerUUID();

    /**
     * Returns an array containing player 1's score at index 0 and player 2's at index 1
     *
     * @param board: Board from which we have to count score
     */
    int[] getScore(Board board);

    /**
     * Remove a spectator from Game
     *
     * @param gameUUID: UUID of the game that the spectator is leaving
     */
    void removeSpectator(UUID gameUUID);

    /**
     * Once a game is finished the JavaFX stage is given back to main module
     *
     * @param s: JavaFX application Stage
     */
    void gameFinished(Stage s);
}
