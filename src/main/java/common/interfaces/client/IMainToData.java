package common.interfaces.client;

import common.exceptions.OthelloException;
import common.model.GameLight;
import common.model.UserHeavy;
import common.model.UserLight;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface IMainToData {

    /**
     * Select a game to join as the second player.
     *
     * @param game: the game to join
     * @param s:    stage to pass to IHM Game
     */
    void selectGame(GameLight game, Stage s);

    /**
     * Create a new game and send it to the server.
     *
     * @param ul:               creator of the game
     * @param spectatorsOk:     boolean allowing spectators or not
     * @param chatOk:           boolean allowing chat or not
     * @param creatorPlayWhite: boolean indicating if the creator is the white player or not
     * @param limitMove:        time limit for a move
     * @param stageFx:          stage to pass to IHM Game
     */
    void newGame(UserLight ul, boolean spectatorsOk, boolean chatOk, boolean creatorPlayWhite, int limitMove,
                 Stage stageFx);

    /**
     * Add user to the spectators of the game and sending the information to the server.
     *
     * @param game:    the game to spectate
     * @param user:    the user to add to spectators
     * @param stageFx: stage to pass to IHM Game
     */
    void requestToDataToSpecialGame(GameLight game, UserLight user, Stage stageFx);

    /**
     * Send information to create a user.
     *
     * @param pseudo:      pseudo of the user
     * @param lastName:    last name of the user
     * @param firstName:   first name of the user
     * @param dateOfBirth: date of birth of the user
     * @param avatarId:    avatar id of the user
     * @param password:    password of the user
     */
    void sendUserInfo(String pseudo, String lastName, String firstName, Date dateOfBirth, int avatarId, String password)
            throws OthelloException;

    /**
     * Update profile of a user.
     *
     * @param profileLight: new UserLight corresponding to user
     * @param profileHeavy: new UserHeavy corresponding to user
     */
    void updateProfile(UserLight profileLight, UserHeavy profileHeavy) throws OthelloException;

    /**
     * Identify the player trying to login.
     *
     * @param login:    login of the user
     * @param password: password of the user
     * @param address:  address of the server
     */
    void identify(String login, String password, String address) throws OthelloException, IOException, ParseException;

    /**
     * Disconnect the user.
     */
    void disconnect();

    /**
     * Get the list of saved games of the local user.
     */
    List<String> getSavedGames();

    /**
     * Replaying a saved game.
     *
     * @param stage: stage to pass to IHM Game
     * @param file:  file containing the saved game
     */
    void replayGame(Stage stage, String file) throws IOException, ParseException;
}
