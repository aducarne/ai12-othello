package common.interfaces.client;

import common.exceptions.OthelloException;
import common.model.*;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * This class is an interface which provides client methods to communicate from
 * Com to Data modules.
 */
public interface IComToDataClient {

    /**
     * Add a new user to the list.
     *
     * @param user : the user to add to the list
     */
    void addNewUser(UserLight user);

    /**
     * Notify the data module that a new game has been created.
     *
     * @param newGameCreated : the game created
     */
    void notifyNewGame(GameLight newGameCreated);

    /**
     * Send a saved game to the data module.
     *
     * @param savedGame : the game saved
     */
    void saveGame(GameHeavy savedGame) throws IOException;

    /**
     * Receive a chat message.
     *
     * @param message : the message to receive
     */
    void receiveChatMessage(Message message);

    /**
     * Notify the data module that spectators list of the game has been updated (spectator added).
     *
     * @param spectators : the actual list of spectators from server
     */
    void addSpectatorToGame(List<UserZero> spectators);

    /**
     * Notify the data module that spectators list of the game has been updated (spectator removed).
     *
     * @param spectatorList : the actual list of spectators from server
     */
    void removeSpectatorFromClient(List<UserZero> spectatorList);

    /**
     * Receive the answer to a game join request.
     *
     * @param isAccepted : the answer to the join request
     * @param gameUUID   : the UUID of the game to join
     */
    void receiveJoinRequestAnswer(Boolean isAccepted, UUID gameUUID);

    /**
     * Receive a join request.
     *
     * @param userProposingUUID : the UUID of the user wanting to join the game
     * @param gameUUID          : the UUID of the game to join
     */
    void receiveJoinRequest(UUID userProposingUUID, UUID gameUUID) throws OthelloException;

    /**
     * Notify that the game is ended.
     *
     * @param board : the game ended
     */
    void notifyEndGame(Board board);

    /**
     * Notify the data module that the board has been updated.
     *
     * @param board : the board updated
     */
    void updateBoard(Board board);

    /**
     * Receive both lists of connected users and available games
     * from server.
     *
     * @param players        : the list of connected user on the server
     * @param gamesAvailable : the list of available games on the server
     */
    void receivePlayersnGames(List<UserLight> players, List<GameLight> gamesAvailable);

    /**
     * Receive list of connected users from server.
     *
     * @param players : the list of connected users on the server
     */
    void forwardPlayers(List<UserLight> players);

    /**
     * Receive list of available games from server.
     *
     * @param games : the list of available games on the server
     */
    void forwardGames(List<GameLight> games);

    /**
     * Launch the game for both players
     *
     * @param gameId: game to be launched
     * @param board:  board of the game to be launched
     */
    void launchGame(UUID gameId, Board board);
}
