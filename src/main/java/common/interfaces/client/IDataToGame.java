package common.interfaces.client;

import common.model.*;
import javafx.stage.Stage;

import java.sql.Timestamp;
import java.util.List;

public interface IDataToGame {
    /**
     * Updates the game board using the given board information
     *
     * @param b : Board information to display
     */
    void updateInterface(Board b);

    /**
     * Shows a new message into the chat panel
     *
     * @param m : Message to display
     */
    void displayMessage(Message m);

    /**
     * Init method of the game IHM, <b>for the players</b>. Displays the game main
     * panel.
     *
     * @param gl : Game data
     * @param s  : JavaFX Stage (shared with IHM-MAIN module)
     */
    void getGameScreen(GameLight gl, Stage s);

    /**
     * Init method of the game IHM, <b>for the spectators</b>. Displays the game
     * main panel.
     *
     * @param gl : Game data
     * @param sc : JavaFX Stage (shared with IHM-MAIN module)
     */
    void beginGameDisplay(GameLight gl, Stage sc);

    /**
     * Updates the spectators list
     *
     * @param spectators : list of all spectators
     */
    void updateSpectatorList(List<UserZero> spectators);

    /**
     * Replays the specified game
     *
     * @param boards    : Game's boards list
     * @param movesTime : Time of each moves (the order of this list is supposed to
     *                  be coherent with boards list)
     * @param chat      : Game's chat
     * @param sc        : JavaFX Stage (shared with IHM-MAIN module)
     */
    void replayGame(List<Board> boards, List<Timestamp> movesTime, Chat chat, Stage sc);

    /**
     * Shows a popup message asking if the specified player can join the game
     *
     * @param userToJoin
     */
    void askHostValidation(UserLight userToJoin);

    /**
     * Informs the client that the game creator has rejected the proposal
     */
    void notifyPlayerRejection();
}
