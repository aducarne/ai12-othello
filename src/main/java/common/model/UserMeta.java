package common.model;

import java.util.UUID;

abstract class UserMeta {
    private UUID id;
    private String pseudo;

    public UserMeta(UUID id, String pseudo) {
        this.id = id;
        this.pseudo = pseudo;
    }

    public UserMeta() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    @Override
    public String toString() {
        return "UserMeta{" + "id=" + id + ", pseudo='" + pseudo + '\'' + '}';
    }
}
