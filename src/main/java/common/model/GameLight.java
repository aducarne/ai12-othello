package common.model;

import java.util.UUID;

public class GameLight extends GameMeta {

    public GameLight() {
        super();
    }

    public GameLight(UUID id, UserLight gameCreator, UserLight player2, GameStatus status, boolean isOkSpectators,
                     boolean isOkChat, boolean isWhiteCreator, int limitMove) {
        super(id, gameCreator, player2, status, isOkSpectators, isOkChat, isWhiteCreator, limitMove);
    }

}
