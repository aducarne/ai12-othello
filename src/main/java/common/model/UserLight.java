package common.model;

import java.util.Date;
import java.util.UUID;

public class UserLight extends UserMeta {
    private String lastName;
    private String firstName;
    private Date dateOfBirth;
    private int avatarId;
    private int playedGames;
    private int wonGames;

    public UserLight(UUID id, String pseudo, String lastName, String firstName, Date dateOfBirth, int avatarId,
                     int playedGames, int wonGames) {
        super(id, pseudo);
        this.lastName = lastName;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.avatarId = avatarId;
        this.playedGames = playedGames;
        this.wonGames = wonGames;
    }

    public UserLight(UUID id, String pseudo) {
        super(id, pseudo);
    }

    public UserLight() {
        super();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAvatarId() {
        return avatarId;
    }

    public void setAvatarId(int avatarId) {
        this.avatarId = avatarId;
    }

    public int getPlayedGames() {
        return playedGames;
    }

    public void setPlayedGames(int playedGames) {
        this.playedGames = playedGames;
    }

    public int getWonGames() {
        return wonGames;
    }

    public void setWonGames(int wonGames) {
        this.wonGames = wonGames;
    }

    public UserZero convertToUserZero() {
        return new UserZero(getId(), getPseudo());
    }

    @Override
    public String toString() {
        return "UserLight{" + "lastName='" + lastName + '\'' + ", firstName='" + firstName + '\'' + ", dateOfBirth="
                + dateOfBirth + ", avatarId=" + avatarId + ", playedGames=" + playedGames + ", wonGames=" + wonGames
                + '}';
    }
}
