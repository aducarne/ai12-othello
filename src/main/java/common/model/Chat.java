package common.model;

import java.util.ArrayList;
import java.util.List;

public class Chat {
    private List<Message> chatList;

    public Chat() {
        chatList = new ArrayList<>();
    }

    public Chat(List<Message> chatList) {
        this.chatList = chatList;
    }

    public List<Message> getChatList() {
        return chatList;
    }

    public void setChat(List<Message> chatList) {
        this.chatList = chatList;
    }

    @Override
    public String toString() {
        return "Chat{" + "chat=" + chatList + '}';
    }
}
