package common.model;

import java.util.List;
import java.util.UUID;

public class UserHeavy extends UserMeta {
    private String password;
    private String serverAddress;
    private String serverPort;
    private List<GameHeavy> savedGames;

    public UserHeavy() {
        super();
    }

    public UserHeavy(UUID id, String pseudo, String password, String serverAddress, String serverPort,
                     List<GameHeavy> savedGames) {
        super(id, pseudo);
        this.password = password;
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;
        this.savedGames = savedGames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }

    public String getServerPort() {
        return serverPort;
    }

    public void setServerPort(String serverPort) {
        this.serverPort = serverPort;
    }

    public List<GameHeavy> getSavedGames() {
        return savedGames;
    }

    public void setSavedGames(List<GameHeavy> savedGames) {
        this.savedGames = savedGames;
    }

    @Override
    public String toString() {
        return "UserHeavy{" + "password='" + password + '\'' + ", serverAddress='" + serverAddress + '\''
                + ", serverPort='" + serverPort + '\'' + ", savedGames=" + savedGames + '}';
    }
}
