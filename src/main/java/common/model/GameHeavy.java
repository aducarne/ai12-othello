package common.model;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public class GameHeavy extends GameMeta {
    private List<Move> moveList;
    private Timestamp gameStart;
    private Chat chat;
    private UUID winner;
    private List<UserZero> spectators;

    public GameHeavy() {
        super();
    }

    public GameHeavy(UUID id, UserLight gameCreator, UserLight player2, GameStatus status, boolean isOkSpectators,
                     boolean isOkChat, boolean isWhiteCreator, int limitMove, List<Move> moveList, Timestamp gameStart,
                     Chat chat, UUID winner, List<UserZero> spectators) {
        super(id, gameCreator, player2, status, isOkSpectators, isOkChat, isWhiteCreator, limitMove);
        this.moveList = moveList;
        this.gameStart = gameStart;
        this.chat = chat;
        this.winner = winner;
        this.spectators = spectators;
    }

    public List<Move> getMoveList() {
        return moveList;
    }

    public void setMoveList(List<Move> moveList) {
        this.moveList = moveList;
    }

    public Timestamp getGameStart() {
        return gameStart;
    }

    public void setGameStart(Timestamp gameStart) {
        this.gameStart = gameStart;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public UUID getWinner() {
        return winner;
    }

    public void setWinner(UUID winner) {
        this.winner = winner;
    }

    public List<UserZero> getSpectators() {
        return spectators;
    }

    public void setSpectators(List<UserZero> spectators) {
        this.spectators = spectators;
    }

    public GameLight convertToGameLight() {
        return new GameLight(getId(), getGameCreator(), getPlayer2(), getStatus(), isOkSpectators(), isOkChat(),
                isWhiteCreator(), getLimitMove());
    }

    @Override
    public String toString() {
        return "GameHeavy{" + "moveList=" + moveList + ", gameStart=" + gameStart + ", chat=" + chat + ", winner="
                + winner + ", spectators=" + spectators + '}';
    }
}
