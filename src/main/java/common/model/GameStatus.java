package common.model;

public enum GameStatus {
    PENDING,
    IN_PROGRESS,
    ENDED
}

