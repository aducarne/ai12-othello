package common.model;

import java.sql.Timestamp;

public class Message {
    private Timestamp hourMessage;
    private String messageContent;
    private UserZero author;

    public Message() {

    }

    public Message(Timestamp hourMessage, String message, UserZero author) {
        this.hourMessage = hourMessage;
        this.messageContent = message;
        this.author = author;
    }

    public Timestamp getHourMessage() {
        return hourMessage;
    }

    public void setHourMessage(Timestamp hourMessage) {
        this.hourMessage = hourMessage;
    }

    public String getMessage() {
        return messageContent;
    }

    public void setMessage(String message) {
        this.messageContent = message;
    }

    public UserZero getAuthor() {
        return author;
    }

    public void setAuthor(UserZero author) {
        this.author = author;
    }
}
