package common.model;

import java.util.UUID;

public class UserZero extends UserMeta {
    public UserZero() {
        super();
    }

    public UserZero(UUID id, String pseudo) {
        super(id, pseudo);
    }
}
