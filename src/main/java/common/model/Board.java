package common.model;

public class Board {
    private int[][] states;

    private UserLight player1;
    private UserLight player2;
    private int nextPlayer;

    public Board() {
        states = new int[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                states[i][j] = 0;
            }
        }
        states[3][3] = 1;
        states[4][4] = 1;
        states[3][4] = 2;
        states[4][3] = 2;
        nextPlayer = 1;
    }

    public Board(Board board) {
        this.states = new int[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                this.states[i][j] = board.states[i][j];
            }
        }
        this.player1 = board.getPlayer1();
        this.player2 = board.getPlayer2();
        this.nextPlayer = board.getNextPlayer();
    }

    public UserLight getPlayer1() {
        return player1;
    }

    public void setPlayer1(UserLight player1) {
        this.player1 = player1;
    }

    public UserLight getPlayer2() {
        return player2;
    }

    public void setPlayer2(UserLight player2) {
        this.player2 = player2;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(int nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public int[][] getStates() {
        return states;
    }

    public void setState(int x, int y, int moveColor) {
        states[x][y] = moveColor;
    }

    public int getColor(int posX, int posY) {
        if (onBoard(posX, posY)) {
            return states[posX][posY];
        }
        return -1;
    }

    public boolean onBoard(int posX, int posY) {
        return (posX < 8 && posY < 8 && posX > -1 && posY > -1);
    }

    public void display() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(states[j][i]);
            }
            System.out.println();
        }
    }
}
