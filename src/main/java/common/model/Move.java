package common.model;

import java.sql.Timestamp;

public class Move {
    private UserZero player;
    private Timestamp hourMove;
    private int x;
    private int y;

    public Move() {

    }

    public Move(UserZero player, Timestamp hourMove, int x, int y) {
        this.player = player;
        this.hourMove = hourMove;
        this.x = x;
        this.y = y;
    }

    public UserZero getPlayer() {
        return player;
    }

    public void setPlayer(UserZero player) {
        this.player = player;
    }

    public Timestamp getHourMove() {
        return hourMove;
    }

    public void setHourMove(Timestamp hourMove) {
        this.hourMove = hourMove;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Move{" + "player=" + player + ", hourMove=" + hourMove + ", x=" + x + ", y=" + y + '}';
    }
}
