package common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

abstract class GameMeta {
    private UUID id;
    private UserLight gameCreator;
    private UserLight player2;
    private GameStatus status;
    private boolean isOkSpectators;
    private boolean isOkChat;
    private boolean isWhiteCreator;
    private int limitMove;

    public GameMeta() {

    }

    public GameMeta(UUID id, UserLight gameCreator, UserLight player2, GameStatus status, boolean isOkSpectators,
                    boolean isOkChat, boolean isWhiteCreator, int limitMove) {
        this.id = id;
        this.gameCreator = gameCreator;
        this.player2 = player2;
        this.status = status;
        this.isOkSpectators = isOkSpectators;
        this.isOkChat = isOkChat;
        this.isWhiteCreator = isWhiteCreator;
        this.limitMove = limitMove;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UserLight getGameCreator() {
        return gameCreator;
    }

    public void setGameCreator(UserLight gameCreator) {
        this.gameCreator = gameCreator;
    }

    public UserLight getPlayer2() {
        return player2;
    }

    public void setPlayer2(UserLight player2) {
        this.player2 = player2;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    @JsonProperty(value = "isOkSpectators")
    public boolean isOkSpectators() {
        return isOkSpectators;
    }

    public void setOkSpectators(boolean okSpectators) {
        isOkSpectators = okSpectators;
    }

    @JsonProperty(value = "isOkChat")
    public boolean isOkChat() {
        return isOkChat;
    }

    public void setOkChat(boolean okChat) {
        isOkChat = okChat;
    }

    @JsonProperty(value = "isWhiteCreator")
    public boolean isWhiteCreator() {
        return isWhiteCreator;
    }

    public void setWhiteCreator(boolean whiteCreator) {
        isWhiteCreator = whiteCreator;
    }

    public int getLimitMove() {
        return limitMove;
    }

    public void setLimitMove(int limitMove) {
        this.limitMove = limitMove;
    }

    @Override
    public String toString() {
        return "GameMeta{" + "id=" + id + ", gameCreator=" + gameCreator + ", player2=" + player2 + ", status=" + status
                + ", isOkSpectators=" + isOkSpectators + ", isOkChat=" + isOkChat + ", isWhiteCreator=" + isWhiteCreator
                + ", limitMove=" + limitMove + '}';
    }
}
