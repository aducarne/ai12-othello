package common.network;

import common.network.messages.JoinRequestAnswerMessage;
import common.network.messages.JoinRequestMessage;

public abstract class ComCoreMeta {
    public abstract void joinRequestMessageProcess(JoinRequestMessage message);

    public abstract void joinRequestAnswerMessageProcess(JoinRequestAnswerMessage message);
}
