package common.network.messages;

import common.model.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.network.ComCoreServer;
import server.network.MessageServerController;

import java.util.UUID;

/**
 * Message containing a chat message to send to the server.
 */
public class ChatMessageToServerMessage extends MessageMeta {
    private static final Logger LOGGER = LogManager.getLogger(ChatMessageToServerMessage.class);
    private Message chatMessage;
    private UUID gameId;


    public ChatMessageToServerMessage(Message chatMessage, UUID gameId) {
        this.chatMessage = chatMessage;
        this.gameId = gameId;
    }

    private ChatMessageToServerMessage() {
    }

    public Message getChatMessage() {
        return chatMessage;
    }


    public void setChatMessage(Message chatMessage) {
        this.chatMessage = chatMessage;
    }


    public UUID getGameId() {
        return gameId;
    }


    public void setGameId(UUID gameId) {
        this.gameId = gameId;
    }

    @Override
    public void processData() {
        try {
            ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
            comCoreServ.getComToData().receiveNewChatMessage(this.getChatMessage(), this.getGameId());
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        MessageServerController.getInstance().sendChatMessageToClients(new ChatMessageToClientMessage(this.getChatMessage()),
                this.getGameId());
    }
}
