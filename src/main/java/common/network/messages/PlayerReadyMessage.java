package common.network.messages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.network.ComCoreServer;

import java.util.UUID;

public class PlayerReadyMessage extends MessageMeta {

    private static final Logger LOGGER = LogManager.getLogger(PlayerReadyMessage.class);

    private UUID playerAskingID;
    private UUID gameAskedID;
    private Boolean isAccepted;

    public PlayerReadyMessage() {
    }

    public PlayerReadyMessage(UUID playerAskingID, UUID gameAskedID, Boolean isAccepted) {
        super();
        this.playerAskingID = playerAskingID;
        this.gameAskedID = gameAskedID;
        this.isAccepted = isAccepted;
    }

    @Override
    public void processData() {
        try {
            ComCoreServer comCoreServer = (ComCoreServer) this.getComCoreMeta();
            comCoreServer.getComToData().receiveJoinRequestAnswer(playerAskingID, gameAskedID, isAccepted);
        } catch (Exception e) {
            LOGGER.error("PlayerReadyMessage error while processing data", e); //TODO Exception
        }
    }

    public UUID getPlayerAskingID() {
        return playerAskingID;
    }

    public void setPlayerAskingID(UUID playerAskingID) {
        this.playerAskingID = playerAskingID;
    }

    public UUID getGameAskedID() {
        return gameAskedID;
    }

    public void setGameAskedID(UUID gameAskedID) {
        this.gameAskedID = gameAskedID;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }
}
