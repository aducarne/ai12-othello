package common.network.messages;

import client.network.ComCoreClient;
import common.model.GameLight;

/**
 * Message notifying clients that a new game has been created.
 */
public class NewGameNotificationToClientMessage extends MessageMeta {

    private GameLight newGame;

    public NewGameNotificationToClientMessage(GameLight newGame) {
        this.newGame = newGame;
    }

    public NewGameNotificationToClientMessage() {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().notifyNewGame(this.getNewGame());
    }

    public GameLight getNewGame() {
        return newGame;
    }

    public void setNewGame(GameLight newGame) {
        this.newGame = newGame;
    }
}
