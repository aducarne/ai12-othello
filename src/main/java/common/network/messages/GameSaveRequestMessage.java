package common.network.messages;

import server.network.ComCoreServer;

import java.util.UUID;

/**
 * Message containing a game save request.
 */
public class GameSaveRequestMessage extends MessageMeta {
    private UUID gameID;
    private UUID playerID;

    public GameSaveRequestMessage(UUID gameID, UUID playerID) {
        this.gameID = gameID;
        this.playerID = playerID;
    }

    private GameSaveRequestMessage() {
        //NOP
    }

    @Override
    public void processData() {
        ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
        comCoreServ.getComToData().requestGameSave(playerID, gameID);
    }

    public UUID getGameID() {
        return gameID;
    }

    public void setGameID(UUID gameID) {
        this.gameID = gameID;
    }

    public UUID getPlayerID() {
        return playerID;
    }

    public void setPlayerID(UUID playerID) {
        this.playerID = playerID;
    }
}
