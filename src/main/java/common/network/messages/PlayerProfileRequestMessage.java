package common.network.messages;

import java.util.UUID;

/**
 * Message containing player profile request to send to the server.
 */
public class PlayerProfileRequestMessage extends MessageMeta {
    private UUID playerID;

    public PlayerProfileRequestMessage(UUID playerID) {
        this.playerID = playerID;
    }

    public PlayerProfileRequestMessage() {
    }

    @Override
    public void processData() {
        //TODO NOPE
    }

    public UUID getPlayerID() {
        return playerID;
    }

    public void setPlayerID(UUID playerID) {
        this.playerID = playerID;
    }

}
