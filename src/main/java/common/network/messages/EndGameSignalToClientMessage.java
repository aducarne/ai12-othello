package common.network.messages;

import client.network.ComCoreClient;
import common.model.Board;

/**
 * Message notifying clients that the game has ended.
 */
public class EndGameSignalToClientMessage extends MessageMeta {

    private Board endedGameInfo;

    public EndGameSignalToClientMessage() {
    }

    public EndGameSignalToClientMessage(Board endedGameInfo) {
        this.endedGameInfo = endedGameInfo;
    }

    public EndGameSignalToClientMessage(String message) {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreClient = (ComCoreClient) this.getComCoreMeta();
        comCoreClient.getComToDataClient().notifyEndGame(this.getEndedGameInfo());
    }

    public Board getEndedGameInfo() {
        return endedGameInfo;
    }

    public void setEndedGameInfo(Board endedGameInfo) {
        this.endedGameInfo = endedGameInfo;
    }
}
