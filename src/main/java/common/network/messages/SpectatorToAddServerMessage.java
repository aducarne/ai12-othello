package common.network.messages;

import common.model.GameLight;
import common.model.UserLight;
import server.network.ComCoreServer;

/**
 * Message containing a spectator to add to a game.
 */
public class SpectatorToAddServerMessage extends MessageMeta {
    private UserLight playerToAdd;
    private GameLight game;

    public SpectatorToAddServerMessage(GameLight game, UserLight playerToAdd) {
        this.game = game;
        this.playerToAdd = playerToAdd;
    }

    public SpectatorToAddServerMessage() {
    }

    @Override
    public void processData() {
        ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
        comCoreServ.getComToData().addSpectator(game, playerToAdd);
    }

    public UserLight getPlayerToAdd() {
        return playerToAdd;
    }

    public void setPlayerToAdd(UserLight playerToAdd) {
        this.playerToAdd = playerToAdd;
    }

    public GameLight getGame() {
        return game;
    }

    public void setGame(GameLight game) {
        this.game = game;
    }
}
