package common.network.messages;

import client.network.ComCoreClient;
import common.model.GameHeavy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Message containing a game save.
 */
public class GameSaveMessage extends MessageMeta {

    private static final Logger LOGGER = LogManager.getLogger(GameSaveMessage.class);
    private GameHeavy gameSave;

    public GameSaveMessage(GameHeavy gameSave) {
        this.gameSave = gameSave;
    }

    public GameSaveMessage() {
        //NOP
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        try {
            comCoreCli.getComToDataClient().saveGame(this.gameSave);
        } catch (IOException e) {
            LOGGER.error("Error while processing the GameSaveMessage : {}", e);
        }
    }

    public GameHeavy getGameSave() {
        return gameSave;
    }

    public void setGameSave(GameHeavy gameSave) {
        this.gameSave = gameSave;
    }
}
