package common.network.messages;

import common.model.UserLight;

/**
 * Message used to transfer a received player profile to the client.
 */
public class PlayerProfileReceivedMessage extends MessageMeta {

    private UserLight playerProfile;

    public PlayerProfileReceivedMessage(UserLight playerProfile) {
        this.playerProfile = playerProfile;
    }

    public PlayerProfileReceivedMessage() {
    }

    @Override
    public void processData() {
        //TODO NOPE
    }

    public UserLight getPlayerProfile() {
        return playerProfile;
    }

    public void setPlayerProfile(UserLight playerProfile) {
        this.playerProfile = playerProfile;
    }
}
