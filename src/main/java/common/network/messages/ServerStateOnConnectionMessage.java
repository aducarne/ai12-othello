package common.network.messages;

import client.network.ComCoreClient;
import common.model.GameLight;
import common.model.UserLight;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServerStateOnConnectionMessage extends MessageMeta {
    private UserLight receiver;
    private List<UserLight> usersConnected;
    private List<GameLight> gamesOnline;

    public ServerStateOnConnectionMessage(UserLight receiver, List<UserLight> usersConnected,
                                          List<GameLight> gamesOnline) {
        this.receiver = receiver;
        this.usersConnected = usersConnected;
        this.gamesOnline = gamesOnline;
    }

    public ServerStateOnConnectionMessage() {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().receivePlayersnGames(this.getUsersConnected(),
                this.getGamesOnline());
    }

    public UserLight getReceiver() {
        return receiver;
    }

    public void setReceiver(UserLight receiver) {
        this.receiver = receiver;
    }

    public List<UserLight> getUsersConnected() {
        return usersConnected;
    }

    public void setUsersConnected(List<UserLight> usersConnected) {
        this.usersConnected = usersConnected;
    }

    public List<GameLight> getGamesOnline() {
        return gamesOnline;
    }

    public void setGamesOnline(List<GameLight> gamesOnline) {
        this.gamesOnline = gamesOnline;
    }
}
