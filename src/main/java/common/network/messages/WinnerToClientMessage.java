package common.network.messages;

import common.model.Board;
import common.model.UserZero;

/**
 * Message notifying clients that the game has ended.
 */
public class WinnerToClientMessage extends MessageMeta {

    private Board updatedGameInfo;
    private UserZero winner;

    public WinnerToClientMessage() {
    }

    public WinnerToClientMessage(Board updatedGameInfo, UserZero winner) {
        this.updatedGameInfo = updatedGameInfo;
        this.winner = winner;
    }

    public WinnerToClientMessage(String message) {
    }

    public Board getUpdatedBoardInfo() {
        return updatedGameInfo;
    }

    public void setUpdatedBoardInfo(Board endedGameInfo) {
        this.updatedGameInfo = endedGameInfo;
    }

    public UserZero getWinner() {
        return winner;
    }

    public void setWinner(UserZero winner) {
        this.winner = winner;
    }

    public void processData() {
        // TODO
    }
}
