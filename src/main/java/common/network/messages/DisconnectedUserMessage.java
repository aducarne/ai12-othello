package common.network.messages;

import java.util.UUID;

/**
 * Message notifying clients that a user has been disconnected.
 */
public class DisconnectedUserMessage extends MessageMeta {

    private UUID disconnectedPlayer;

    public DisconnectedUserMessage(UUID disconnectedPlayer) {
        this.disconnectedPlayer = disconnectedPlayer;
    }

    public DisconnectedUserMessage() {
    }

    @Override
    public void processData() {
        //NOPE
    }

    public UUID getDisconnectedPlayer() {
        return disconnectedPlayer;
    }

    public void setDisconnectedPlayer(UUID disconnectedPlayer) {
        this.disconnectedPlayer = disconnectedPlayer;
    }
}
