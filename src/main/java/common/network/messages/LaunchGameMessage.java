package common.network.messages;

import client.network.ComCoreClient;
import common.model.Board;

import java.util.UUID;

public class LaunchGameMessage extends MessageMeta {
    private UUID gameId;
    private Board board;

    public LaunchGameMessage(UUID gameId, Board board) {
        this.board = board;
        this.gameId = gameId;
    }

    private LaunchGameMessage() {
        //NOP
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().launchGame(this.getGameId(), this.getBoard());
    }

    public UUID getGameId() {
        return gameId;
    }

    public void setGameId(UUID gameId) {
        this.gameId = gameId;
    }

    public Board getBoard() {
        return board;
    }

}
