package common.network.messages;

import client.network.ComCoreClient;
import common.model.Board;

import java.util.UUID;

/**
 * Message containing a move to forward to concerned clients.
 */
public class MoveToForwardToClientMessage extends MessageMeta {

    private UUID gameID;
    private Board boardToSend;

    public MoveToForwardToClientMessage(UUID gameID, Board boardToSend) {
        this.gameID = gameID;
        this.boardToSend = boardToSend;
    }

    public MoveToForwardToClientMessage() {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().updateBoard(this.getBoardToSend());
    }

    public UUID getGameID() {
        return gameID;
    }

    public void setGameID(UUID gameID) {
        this.gameID = gameID;
    }

    public Board getBoardToSend() {
        return boardToSend;
    }

    public void setBoardToSend(Board boardToSend) {
        this.boardToSend = boardToSend;
    }
}
