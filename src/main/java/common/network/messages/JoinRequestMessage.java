package common.network.messages;

import java.util.UUID;

/**
 * Message containing a game join request to transfer to the correct client.
 */
public class JoinRequestMessage extends MessageMeta {

    private UUID gameID;
    private UUID playerAskingID;

    public JoinRequestMessage(UUID gameID, UUID playerAskingID) {
        this.gameID = gameID;
        this.playerAskingID = playerAskingID;
    }

    public JoinRequestMessage() {
    }

    @Override
    public void processData() {
        this.getComCoreMeta().joinRequestMessageProcess(this);
    }

    public UUID getGameID() {
        return gameID;
    }

    public void setGameID(UUID gameID) {
        this.gameID = gameID;
    }

    public UUID getPlayerAskingID() {
        return playerAskingID;
    }

    public void setPlayerAskingID(UUID playerAskingID) {
        this.playerAskingID = playerAskingID;
    }
}
