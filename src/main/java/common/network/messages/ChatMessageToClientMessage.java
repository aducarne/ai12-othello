package common.network.messages;

import client.network.ComCoreClient;
import common.model.Message;


/**
 * Message containing a chat message to transfer to a client.
 */
public class ChatMessageToClientMessage extends MessageMeta {
    private Message message;

    public ChatMessageToClientMessage(Message message) {
        this.message = message;
    }

    public ChatMessageToClientMessage() {
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().receiveChatMessage(this.getMessage());
    }
}
