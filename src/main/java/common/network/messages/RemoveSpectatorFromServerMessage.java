package common.network.messages;

import common.model.UserZero;
import server.network.ComCoreServer;

import java.util.UUID;


/**
 * Message containing a chat message to transfer to a client.
 */
public class RemoveSpectatorFromServerMessage extends MessageMeta {

    private UserZero user;
    private UUID GameID;

    public RemoveSpectatorFromServerMessage(UserZero user, UUID GameID) {
        this.user = user;
        this.GameID = GameID;
    }

    public RemoveSpectatorFromServerMessage() {
    }

    public UserZero getUser() {
        return user;
    }

    public void setUser(UserZero user) {
        this.user = user;
    }

    public UUID getGameID() {
        return GameID;
    }

    public void setGameID(UUID GameID) {
        this.GameID = GameID;
    }

    @Override
    public void processData() {
        ComCoreServer comCoreServer = (ComCoreServer) this.getComCoreMeta();
        comCoreServer.getComToData().removeSpectatorFromServer(this.getUser(), this.GameID);
    }
}
