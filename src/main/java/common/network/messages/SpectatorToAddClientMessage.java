package common.network.messages;

import client.network.ComCoreClient;
import common.model.UserZero;

import java.util.List;

/**
 * Message notifying that a new spectator has been added to the game.
 */
public class SpectatorToAddClientMessage extends MessageMeta {

    private List<UserZero> spectators;

    public SpectatorToAddClientMessage(List<UserZero> spectators) {
        this.spectators = spectators;
    }

    public SpectatorToAddClientMessage() {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().addSpectatorToGame(spectators);
    }

    public List<UserZero> getSpectators() {
        return spectators;
    }

    public void setSpectators(List<UserZero> spectators) {
        this.spectators = spectators;
    }
}
