/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common.network.messages;

import client.network.ComCoreClient;
import common.model.UserZero;

import java.util.List;

/**
 * @author clementlebre
 */
public class RemoveSpectatorFromClientMessage extends MessageMeta {

    private List<UserZero> users;

    public RemoveSpectatorFromClientMessage() {
    }

    public RemoveSpectatorFromClientMessage(List<UserZero> users) {
        this.users = users;
    }

    public List<UserZero> getUsers() {
        return users;
    }

    public void setUsers(List<UserZero> users) {
        this.users = users;
    }


    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().removeSpectatorFromClient(this.users);
    }
}

