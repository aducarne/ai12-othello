package common.network.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

/**
 * Message containing the answer to a join request.
 */
public class JoinRequestAnswerMessage extends MessageMeta {

    private UUID playerAskingID;
    private UUID gameAskedID;
    private Boolean isAccepted;

    public JoinRequestAnswerMessage(UUID playerAskingID, UUID gameAskedID, Boolean isAccepted) {
        this.playerAskingID = playerAskingID;
        this.gameAskedID = gameAskedID;
        this.isAccepted = isAccepted;
    }

    public JoinRequestAnswerMessage() {
    }

    @Override
    public void processData() {
        this.getComCoreMeta().joinRequestAnswerMessageProcess(this);
    }

    public UUID getPlayerAskingID() {
        return playerAskingID;
    }

    public void setPlayerAskingID(UUID playerAskingID) {
        this.playerAskingID = playerAskingID;
    }

    public UUID getGameAskedID() {
        return gameAskedID;
    }

    public void setGameAskedID(UUID gameAskedID) {
        this.gameAskedID = gameAskedID;
    }

    @JsonProperty(value = "isAccepted")
    public Boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

}
