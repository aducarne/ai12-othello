package common.network.messages;

import server.network.ComCoreServer;
import server.network.MessageServerController;

/**
 * Message containing a login request to send to the server.
 */
public class RequestGameListMessage extends MessageMeta {

    public RequestGameListMessage() {
        // TODO
    }

    @Override
    public void processData() {

        ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();

        GameListMessage gameListMessage = new GameListMessage(comCoreServ.getComToData().getListGames());
        MessageServerController.getInstance().sendGameList(gameListMessage);
    }


}
