package common.network.messages;

import common.model.GameLight;
import server.network.ComCoreServer;

/**
 * Message containing new game information.
 */
public class NewGameToServerMessage extends MessageMeta {
    private GameLight newGame;

    public NewGameToServerMessage(GameLight newGame) {
        this.newGame = newGame;
    }

    public NewGameToServerMessage() {
    }

    @Override
    public void processData() {
        ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
        comCoreServ.getComToData().createGame(this.getNewGame());
    }

    public GameLight getNewGame() {
        return newGame;
    }

    public void setNewGame(GameLight newGame) {
        this.newGame = newGame;
    }
}
