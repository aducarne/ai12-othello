package common.network.messages;

import client.network.ComCoreClient;
import common.model.UserLight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UserListMessage extends MessageMeta {
    private static final Logger LOGGER = LogManager.getLogger(UserListMessage.class);
    private List<UserLight> userList;

    private UserListMessage() {
        //NOP
    }

    public UserListMessage(List<UserLight> userList) {
        this.userList = userList;
    }


    public List<UserLight> getUserList() {
        return userList;
    }

    public void setUserList(List<UserLight> userList) {
        this.userList = userList;
    }


    @Override
    public void processData() {
        try {
            ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
            comCoreCli.getComToDataClient().forwardPlayers(this.getUserList());
        } catch (Exception e) {
            LOGGER.error("processData", e);
        }
    }
}
