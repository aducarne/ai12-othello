package common.network.messages;

import common.model.GameLight;
import common.model.UserLight;
import server.network.ComCoreServer;
import server.network.MessageServerController;

import java.util.List;

/**
 * Message containing a login request to send to the server.
 */
public class LoginUserToServerMessage extends MessageMeta {
    private UserLight userToAdd;
    private String userAddress;

    public LoginUserToServerMessage(UserLight userToAdd) {
        this.userToAdd = userToAdd;
    }

    public LoginUserToServerMessage(UserLight userToConnect, String address) {
        this.userToAdd = userToConnect;
        this.userAddress = address;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public UserLight getUserToAdd() {
        return userToAdd;
    }

    public void setUserToAdd(UserLight userToAdd) {
        this.userToAdd = userToAdd;
    }

    @Override
    public void processData() {
        // notify the data module of the new user
        ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
        comCoreServ.getComToData().addAuthenticatedPlayer(this.getUserToAdd());

        // send lists to the new user
        List<GameLight> gamesOnline = comCoreServ.getComToData().getListGames();
        List<UserLight> usersConnected = comCoreServ.getComToData().getListUsers();
        ServerStateOnConnectionMessage msg = new ServerStateOnConnectionMessage(this.getUserToAdd(),
                usersConnected, gamesOnline);
        MessageServerController.getInstance().sendListsToNewUser(msg);

        //notify others of the fact a new user is available
        NewUserAddedMessage newUserToAdd = new NewUserAddedMessage(this.getUserToAdd());
        MessageServerController.getInstance().notifyNewUserAdded(newUserToAdd);
    }
}
