package common.network.messages;

import client.network.ComCoreClient;
import common.model.GameLight;

import java.util.List;

public class GameListMessage extends MessageMeta {

    private List<GameLight> gameLights;

    public GameListMessage(List<GameLight> gameLights) {
        this.gameLights = gameLights;
    }

    public GameListMessage() {
    }

    public List<GameLight> getGameLights() {
        return gameLights;
    }

    public void setGameLights(List<GameLight> gameLights) {
        this.gameLights = gameLights;
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().forwardGames(this.getGameLights());
    }
}
