package common.network.messages;

import common.exceptions.OthelloException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.network.ComCoreServer;

import java.util.UUID;

/**
 * Message containing a disconnection request to send to the server.
 */
public class DisconnectionRequestMessage extends MessageMeta {
    private static final Logger LOGGER = LogManager.getLogger(DisconnectionRequestMessage.class);

    private UUID playerAskingID;

    public DisconnectionRequestMessage(UUID playerAskingID) {
        this.playerAskingID = playerAskingID;
    }

    public DisconnectionRequestMessage() {
    }

    public UUID getPlayerAskingID() {
        return playerAskingID;
    }

    public void setPlayerAskingID(UUID playerAskingID) {
        this.playerAskingID = playerAskingID;
    }

    @Override
    public void processData() {
        ComCoreServer comCoreServer = (ComCoreServer) this.getComCoreMeta();
        try {
            comCoreServer.getComToData().disconnectUser(playerAskingID);
        } catch (OthelloException e) {
            LOGGER.debug(e);
        }
    }
}
