package common.network.messages;

import client.network.ComCoreClient;
import common.model.UserLight;

/**
 * Message notifying clients that a user has been added.
 */
public class NewUserAddedMessage extends MessageMeta {

    private UserLight userToAdd;

    public NewUserAddedMessage(UserLight userToAdd) {
        this.userToAdd = userToAdd;
    }

    public NewUserAddedMessage() {
    }

    @Override
    public void processData() {
        ComCoreClient comCoreCli = (ComCoreClient) this.getComCoreMeta();
        comCoreCli.getComToDataClient().addNewUser(this.getUserToAdd());
    }

    public UserLight getUserToAdd() {
        return userToAdd;
    }

    public void setUserToAdd(UserLight userToAdd) {
        this.userToAdd = userToAdd;
    }


}
