package common.network.messages;

import common.model.Board;
import common.model.Move;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.network.ComCoreServer;

import java.util.UUID;

/**
 * Message containing a move to send to the server.
 */
public class MoveToSendToServerMessage extends MessageMeta {
    private static final Logger LOGGER = LogManager.getLogger(MoveToSendToServerMessage.class);
    private Move moveToSend;
    private Board boardToSend;
    private UUID gameID;

    public MoveToSendToServerMessage(UUID gameID, Move moveToSend, Board boardToSend) {
        this.gameID = gameID;
        this.boardToSend = boardToSend;
        this.moveToSend = moveToSend;
    }

    public MoveToSendToServerMessage() {

    }

    @Override
    public void processData() {
        try {
            ComCoreServer comCoreServ = (ComCoreServer) this.getComCoreMeta();
            comCoreServ.getComToData().sendMove(this.getGameID(), this.getMoveToSend(), this.getBoardToSend());
        } catch (Exception e) {
            LOGGER.error("processData", e); // TODO Exception
        }
    }

    public Move getMoveToSend() {
        return moveToSend;
    }

    public void setMoveToSend(Move moveToSend) {
        this.moveToSend = moveToSend;
    }

    public UUID getGameID() {
        return gameID;
    }

    public Board getBoardToSend() {
        return boardToSend;
    }
}
