package common.network.messages;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import common.network.ComCoreMeta;

/**
 * class inherited by all types of messages exchanged in the Communication module.
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "type")
public abstract class MessageMeta {

    @JsonIgnore
    private ComCoreMeta comCoreMeta;

    public MessageMeta() {
        //NOP
    }

    public ComCoreMeta getComCoreMeta() {
        return comCoreMeta;
    }

    public void setComCoreMeta(ComCoreMeta comCoreMeta) {
        this.comCoreMeta = comCoreMeta;
    }

    /**
     * Method to treat message depending on its type
     * (uses polymorphism).
     */
    public abstract void processData();

}
