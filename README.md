
# AI12 Projet othello :whale2: :whale:

## Composition du projet

||**Communication**|**Données**|**IHM-Main**|**IHM-Game**|
|---|---|---|---|---|
|**Directeur**|Cynthia :alarm_clock:|Alexandre S. :toilet: :shit:|Louis BDS. :love_hotel:|Aurélien :black_joker:|
|**Manager**|Clément :rocket:|Quentin :hamster:|Louis F. :books:|Mohamed :muscle:|
|**Resp. Conception**|Thibault :mortar_board:|Yaya :sailboat:|Mame :beetle:|Lucas :moneybag:|
|**Resp. Qualité**|Alexandre D. :red_circle:|Tancrède :pig:|Sarah :cookie:|Amaury :airplane:|
|**Resp. Développement**|Corentin :woman:|Alexandre M. :recycle:|William :bike:|Thomas :trumpet:|

## Exécution

### Compilation

Placez vous à la racine du projet et lancez la commande `mvn install`. L'archive *othello-<numéro_de_version>.jar* est générée dans le dossier *target/*.

### Lancement du serveur de l'application

Depuis la racine du projet, lancez la commande `java -jar target/othello-0.0.1-SNAPSHOT.jar -server`.

### Lancement d'une application client

Depuis la racine du projet, lancez la commande `java -jar target/othello-0.0.1-SNAPSHOT.jar -client` **ou** `java -jar target/othello-0.0.1-SNAPSHOT.jar`.
